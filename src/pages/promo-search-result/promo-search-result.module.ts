import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PromoSearchResultPage } from './promo-search-result';

@NgModule({
  declarations: [
    PromoSearchResultPage,
  ],
  imports: [
    IonicPageModule.forChild(PromoSearchResultPage),
  ],
})
export class PromoSearchResultPageModule {}
