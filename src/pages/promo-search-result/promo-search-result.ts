import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';
import { PromoSearchProvider } from '../../providers/promo-search/promo-search';

/**
 * Generated class for the PromoSearchResultPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-promo-search-result',
  templateUrl: 'promo-search-result.html',
})
export class PromoSearchResultPage {
newLocation: any;
promos_search: any = [];
x: any;

  constructor(
    public navCtrl: NavController, 
    public appCtrl: App, 
    public promoSearchProvider: PromoSearchProvider,
    public navParams: NavParams) {
      this.newLocation = localStorage.getItem('key');
      console.log('item', this.newLocation);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PromoSearchResultPage');
    //this.loadPromoCategory();
  }

  goBack() {
    this.navCtrl.pop();
  }


  // loadPromoCategory(){
  //   this.promoSearchProvider.getPromoFilter(localStorage.getItem('key')).subscribe(
  //     res => {
  //       let resultset: any = res;

  //       //display all the category with product inside
  //       this.promos_search = resultset.data;

  //       this.x = localStorage.getItem('key');

       

  //       console.log('prom', this.promos_search);

  //     }, error => {
  //       console.log(error);
  //   });
  // }

  goTo(){
    this.appCtrl.getRootNav().push('FilterPage');
    console.log("sana");
  }

}
