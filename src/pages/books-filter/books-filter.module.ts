import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BooksFilterPage } from './books-filter';

@NgModule({
  declarations: [
    BooksFilterPage,
  ],
  imports: [
    IonicPageModule.forChild(BooksFilterPage),
  ],
})
export class BooksFilterPageModule {}
