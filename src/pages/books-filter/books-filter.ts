import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App, LoadingController, UrlSerializer } from 'ionic-angular';
import { FormGroup, FormControl, Validators, FormBuilder, NgControl } from "@angular/forms";
import { Storage } from '@ionic/storage';

import { VoucherProvider }  from '../../providers/voucher/voucher';
import { ProductProvider }  from '../../providers/product/product';

/**
 * Generated class for the BooksFilterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-books-filter',
  templateUrl: 'books-filter.html',
})
export class BooksFilterPage {

  public saveUsername:boolean;
  formGroup: any;
  categories: any [];
  newmessage: any;
  productCategories: any = [];
  cat: any;
  selectedArray: any = [];
  merchantID: number;
  locationPass: any;
  newCate: any;

  constructor(
    public navCtrl: NavController,
    public appCtrl: App,  
    private fb: FormBuilder,
    private storage: Storage,
    private loadingCtrl: LoadingController,
    public voucherProvider: VoucherProvider,
    public productProvider: ProductProvider,
    public navParams: NavParams) {

      this.formGroup = this.fb.group({
        location:[''],
        tc:['false']
      });

      this.locationPass = this.navParams.get("location");
      this.newCate = localStorage.getItem("categorize");

      console.log('form-val', this.formGroup.value);
      localStorage.setItem('categorize', "");
  }


    public onSaveUsernameChanged(value:boolean){
        this.cat = value;
    }

    selectMember(data){

      console.log('dat', data);
     
      if (data.checked == true) {
         this.selectedArray.push(data.cat_id);
      } 
      if (data.checked == false) 
      {
        this.selectedArray = this.selectedArray.filter(item => item !== data.cat_id);
      }
  
      let newLL = []
      newLL.push(this.selectedArray)
      console.log("category return", newLL)
      localStorage.setItem('categorize', this.selectedArray);
    }

    filterForm(event: any, category, data) {

      console.log('loca', this.formGroup.value.location);

    localStorage.getItem('cat');
    
    console.log("select array", this.selectedArray)
    let newLL2 = []
    newLL2.push(this.selectedArray)
    console.log("newLL", newLL2)

    if(this.formGroup.value.location == undefined){
      this.formGroup.value.location = "";
    }else{
      localStorage.setItem('key', "");
      localStorage.setItem('key', this.formGroup.value.location);
    }

    this.navCtrl.push('SearchResultPage', 
      {categoryPush: newLL2,
      location: localStorage.getItem('key')
      });

    this.newmessage = '';
    this.formGroup.value.location  = '';
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FilterPage');
    this.loadProductCategory();
  }

  onInput(ev: any) {
    // Reset items back to all of the items
    //this.initializeItems();

    // set val to the value of the searchbar
    let val = ev.target.value;

    console.log('val: ', val);

    // if the value is an empty string don't filter the items
    // if (val && val.trim() != '') {
    //   this.items = this.items.filter((item) => {
    //     return (item.storeName.toLowerCase().indexOf(val.toLowerCase()) > -1);
    //   })
    // }

  }

  loadProductCategory(){

    this.storage.get('merchantID').then((value)=>{
      this.merchantID = value;

      let data = {
        "merchant_id":this.merchantID
      };
  
      this.productProvider.getProductCategories(data).subscribe(
        res => {
          let resultset: any = res;



          let loader = this.loadingCtrl.create({
            content: 'Loading Page ... ',
          });
      
          loader.present();
      
          setTimeout(() => {
      
          //display all the category with product inside
          this.productCategories = resultset.data;
  
          console.log('product categories', this.productCategories);
      
          }, 1000);
      
          setTimeout(() => {
            loader.dismiss();
          }, 3000);

  
        }, error => {
          console.log(error);
      });
    
    });
    
  }

  goBack() {
    this.navCtrl.pop();
  }

  navigateTo(page:string){
    this.appCtrl.getRootNav().push(page);
  }

  openModal(){
    this.navCtrl.push('PromoPage');
  }

}
