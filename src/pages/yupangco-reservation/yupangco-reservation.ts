import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';

/**
 * Generated class for the YupangcoReservationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-yupangco-reservation',
  templateUrl: 'yupangco-reservation.html',
})
export class YupangcoReservationPage {

  constructor(
    public navCtrl: NavController,
    public appCtrl: App,
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad YupangcoReservationPage');
  }

  goBack() {
    this.navCtrl.pop();
  }

}
