import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { YupangcoReservationPage } from './yupangco-reservation';

@NgModule({
  declarations: [
    YupangcoReservationPage,
  ],
  imports: [
    IonicPageModule.forChild(YupangcoReservationPage),
  ],
})
export class YupangcoReservationPageModule {}
