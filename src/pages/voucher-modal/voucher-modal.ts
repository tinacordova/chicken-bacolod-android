import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App, AlertController } from 'ionic-angular';
import { VoucherProvider } from '../../providers/voucher/voucher';

import { WalletProvider } from '../../providers/wallet/wallet';
import { TransactionProvider } from '../../providers/transaction/transaction';

@IonicPage()
@Component({
  selector: 'page-voucher-modal',
  templateUrl: 'voucher-modal.html',
})
export class VoucherModalPage {
  voucherParamId: number;
  vouchers: any [];
  vouchers_new: any [];
  voucher_stats: any[];
  wallet_voucher: any [];
  transaction: any[];
  new_vouchers: any[];
  stat: any;
  transac: any [];
  checker: any [];

  disableButton: boolean = true
  show: boolean
  show_dis: boolean

  localStorageUserId: string;
  localStorageFirstName: string;
  localStorageLastName: string;
  
  x: any = [];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public appCtrl: App, 
    public voucherProvider: VoucherProvider,
    public walletProvider: WalletProvider,
    public alertCtrl: AlertController,
    public transactionProvider: TransactionProvider) {
      this.voucherParamId = this.navParams.get('voucher_id');
  }

  ionViewDidLoad() {
    this.loadVoucherDetail();
    // this.voucherStatus();
    this.newVoucherDetail();
    // this.getNumber();
    // this.checkVoucher();

    this.localStorageUserId = localStorage.getItem('user_id');
    this.localStorageFirstName = localStorage.getItem('user_firstname');
    this.localStorageLastName = localStorage.getItem('user_lastname');
  }

  newVoucherDetail(){
    console.log("load detail")

    this.walletProvider.checkVoucher(this.voucherParamId).subscribe(
      res => {
        let resultset: any = res;
        this.checker = resultset.data;
        console.log("checker", this.checker)
        
        if (this.checker[0].checker == 0){
          this.show = true;
        }else{
          this.transactionProvider.voucherDetail(this.voucherParamId).subscribe(
            res => {
              let resultset: any = res;
              this.new_vouchers = resultset.data;
              console.log("new vouchers", this.new_vouchers)
              console.log("stat", this.new_vouchers[0].status)
      
              if (this.new_vouchers[0].status != 0){
                // this.show_dis = true
                this.show = false
              }else{
                // this.show = true
                null
              }
      
            }, error => {
              console.log(error);
          });
        }

      }, error => {
        console.log(error);
      });
  }

  // checkVoucher(){
  //   this.walletProvider.checkVoucher(this.voucherParamId).subscribe(
  //     res => {
  //       let resultset: any = res;
  //       this.checker = resultset.data;
  //       console.log("checker", this.checker)
  //       // console.log("stat", this.vouchers[0].status)
  //     }, error => {
  //       console.log(error);
  //     });
  // }

  loadVoucherDetail(){
    console.log("load detail")
    this.voucherProvider.getVoucherDetail(this.voucherParamId).subscribe(
      res => {
        let resultset: any = res;
        this.vouchers = resultset.data;
        console.log("vouchers", this.vouchers)
        // console.log("stat", this.vouchers[0].status)
      }, error => {
        console.log(error);
    });
  }


  // voucherStatus(){
  //   var id = localStorage.getItem('user_id');

  //   this.walletProvider.getVoucherStatus(parseInt(id), this.voucherParamId).subscribe(
  //     res => {
  //       let resultset: any = res;
  //       this.voucher_stats = resultset.data;

  //       this.stat = this.voucher_stats.map(t=>t.status);

  //       if(this.stat[0] == 1){
  //         //Hide the Get Voucher Button in the voucher module
  //         this.show = false;
  //       }

  //     }, error => {
  //       console.log(error);
  //   });
  // }

  // loadVoucherList(){
  //   console.log("id", this.voucherParamId)
  //   this.walletProvider.getWalletVoucher().subscribe(
  //     res => {
  //       let resultset: any = res;
  //       this.vouchers_new = resultset.data;

  //       let result = this.vouchers_new.map(a => a.voucher_id);
  //       console.log("result", result);

  //       let obj = this.vouchers_new.find(o => o.voucher_id === this.voucherParamId);

  //       console.log("obj", obj);

  //       if (obj == this.voucherParamId){
  //         console.log("done")
  //       }else{
  //         console.log("not done")
  //       }

  //       console.log("list", this.vouchers_new);
  //     }, error => {
  //       console.log(error);
  //   });
  // }

  goBack() {
    this.navCtrl.pop();
  }

  navigateTo(page:string){
    this.appCtrl.getRootNav().push(page);
  }

  getVoucher(){
    var id = localStorage.getItem('user_id');
    var f_name = localStorage.getItem('user_firstname');
    var l_name = localStorage.getItem('user_lastname');

    console.log(this.voucherParamId, id, f_name, l_name)

    this.walletProvider.postWalletVoucher(this.voucherParamId, id, f_name, l_name).subscribe(
      res => {
        let resultset: any = res;
        this.wallet_voucher = resultset.data;

        this.transactionProvider.voucherDetail(this.voucherParamId).subscribe(
          res => {
            console.log('in')
            let resultset: any = res;
            this.new_vouchers = resultset.data;
            console.log("new vouchers", this.new_vouchers)
  
            let vid = this.new_vouchers[0].id
            let mid = this.new_vouchers[0].merchant_id
            let uid = this.new_vouchers[0].ipayu_user_id
            let amt = this.new_vouchers[0].value
            let type = "voucher"
            let bal = this.new_vouchers[0].balance_qty
            let vui = this.new_vouchers[0].voucher_user_id
  
            this.transactionProvider.postTransactionGet(vid, mid, uid, amt, type, bal,
                                                          "123", "123", vui, "1").subscribe(
              res => {
                console.log('in')
                let resultset: any = res;
                this.transac = resultset.data;
                console.log("transaction", this.transac)
              }, error => {
                console.log(error);
            });
  
          }, error => {
            console.log(error);
        });

        const alert = this.alertCtrl.create({
          title: "Congratulations!",
          subTitle: "Voucher is saved in your wallet.",
          buttons: ['OK'],
          cssClass: 'alertCustomCss'
        });
        alert.present()
        this.navCtrl.pop()
      }), error => {
        console.log(error);
      }

      
  }

}
