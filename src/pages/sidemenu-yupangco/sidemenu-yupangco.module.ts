import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SidemenuYupangcoPage } from './sidemenu-yupangco';

@NgModule({
  declarations: [
    SidemenuYupangcoPage,
  ],
  imports: [
    IonicPageModule.forChild(SidemenuYupangcoPage),
  ],
})
export class SidemenuYupangcoPageModule {}
