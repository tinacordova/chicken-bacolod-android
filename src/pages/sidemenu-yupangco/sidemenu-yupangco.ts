import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Nav, Platform, MenuController } from 'ionic-angular';

/**
 * Generated class for the SidemenuYupangcoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sidemenu-yupangco',
  templateUrl: 'sidemenu-yupangco.html',
})
export class SidemenuYupangcoPage {
  @ViewChild(Nav) nav: Nav;

  resto: boolean = true ;
  music: boolean = false;
  pages: Array<{title: string, component: any}>;


  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController, 
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SidemenuYupangcoPage');
  }

  openPage(page) {
    this.nav.setRoot(page);
    // this.activeMenuIndex = i;
    this.menuCtrl.open();
    }

    toggleResto(){
      this.resto = !this.resto;
      this.music = !this.music;
    }
  
    toggleMusic(){
      this.resto = !this.resto;
      this.music = !this.music;
    }

}
