import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppModalPage } from './app-modal';

@NgModule({
  declarations: [
    AppModalPage,
  ],
  imports: [
    IonicPageModule.forChild(AppModalPage),
  ],
})
export class AppModalPageModule {}
