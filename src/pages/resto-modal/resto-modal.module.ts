import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RestoModalPage } from './resto-modal';

@NgModule({
  declarations: [
    RestoModalPage,
  ],
  imports: [
    IonicPageModule.forChild(RestoModalPage),
  ],
})
export class RestoModalPageModule {}
