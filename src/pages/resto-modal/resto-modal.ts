import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ProductProvider } from '../../providers/product/product';

/**
 * Generated class for the RestoModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-resto-modal',
  templateUrl: 'resto-modal.html',
})
export class RestoModalPage {
  prodParamId: number;
  products: any = [];
  productSubImages: any = [];

  constructor(
    private navCtrl: NavController, 
    public navParams: NavParams,
    public productProvider: ProductProvider) {

      this.prodParamId = this.navParams.get('prod_id');

  }

  ionViewDidLoad() {
    this.loadProductDetail();
    this.loadProductSubImages();
  }

  loadProductDetail(){
    this.productProvider.getProductDetailV8(this.prodParamId).subscribe(
      res => {
        let resultset: any = res;
        this.products = resultset.data;
        console.log("product detail", this.products);

        //let color = this.products.map(x=>x.background_color);

      //  console.log("test", color);

      }, error => {
        console.log(error);
    });
  }

  loadProductSubImages(){
    this.productProvider.getProductSubImages(this.prodParamId).subscribe(
      res => {
        let resultset: any = res;
        this.productSubImages = resultset.data;
        console.log("product sub images", this.productSubImages);
      }, error => {
        console.log(error);
    });
  }

  goBack() {
    this.navCtrl.pop();
  }

}
