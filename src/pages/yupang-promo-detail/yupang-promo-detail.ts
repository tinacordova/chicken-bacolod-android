import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';

/**
 * Generated class for the YupangPromoDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-yupang-promo-detail',
  templateUrl: 'yupang-promo-detail.html',
})
export class YupangPromoDetailPage {

  constructor(
    public navCtrl: NavController,
    public appCtrl: App, 
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad YupangPromoDetailPage');
  }

  goBack() {
    this.navCtrl.pop();
  }


}
