import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { YupangPromoDetailPage } from './yupang-promo-detail';

@NgModule({
  declarations: [
    YupangPromoDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(YupangPromoDetailPage),
  ],
})
export class YupangPromoDetailPageModule {}
