import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, App, LoadingController } from 'ionic-angular';
import { Nav, MenuController, ModalController, ToastController, ViewController, Content } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';

import { ProductProvider } from '../../providers/product/product';
import { MerchantProvider } from '../../providers/merchant/merchant';
import { CartProvider } from '../../providers/cart/cart';
import { RestoCategoryProvider } from '../../providers/resto-category/resto-category';

@IonicPage()
@Component({
  selector: 'page-menu-category',
  templateUrl: 'menu-category.html',
  
})
export class MenuCategoryPage {

  @ViewChild(Nav) nav: Nav;
  @ViewChild(Content) content: Content;

  pages: Array<{title: string, component: any}>;

  isAndroid: boolean = false;

  specialOffers: any [];
  public counter : number = 1;
  merchants: any [];
  merchantParamId: any;
  cartcounter: any [];
  counterFront: number;
  my_id: number;
  my_amt: number;
  count_order: any [];
  products: any [];
  updateorder: any [];
  orders: any [];
  catID: number;
  category: any [];
  new_catID: any;
  merchantID: number;
  cartID: number;
  table_num: any;
  cart_id: any;
  main: any;
  tableNumber: any;
  userID: number;

  closeToggle: boolean = true;
  openToggle: boolean = false;
  openopen: boolean = false;

  localStorageUserId: string;
  localStorageFirstName: string;
  localStorageLastName: string;
  tableNum: string;

  firstName: string;
  lastName: string;

  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController, 
    public appCtrl: App, 
    public navParams: NavParams,
    public http: Http,
    public productProvider: ProductProvider,
    public merchantProvider: MerchantProvider,
    public loadingCtrl: LoadingController,
    public cartProvider: CartProvider,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    public restoCat: RestoCategoryProvider,
    private storage: Storage,
    public viewCtrl: ViewController) {
      this.firstName = localStorage.getItem('user_firstname');
      this.lastName = localStorage.getItem('user_lastname');

      this.merchantParamId = this.navParams.get('merchant_id');
      this.catID = this.navParams.get('cit');

      this.new_catID = localStorage.getItem('cat_id')

  }

  ionViewDidEnter(){
    this.loadCartDetailCounter();
    console.log('did enter')
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MenuCategoryPage');
    this.loadCartDetailCounter();
    this.getCartID();
    this.loadProdSpecialOffer()
    this.loadShopInfo()
    this.loadCatName()
    console.log('cat_id', this.new_catID)

    this.localStorageUserId = localStorage.getItem('user_id');
    this.localStorageFirstName = localStorage.getItem('user_firstname');
    this.localStorageLastName = localStorage.getItem('user_lastname');
    this.tableNum = localStorage.getItem('table_number')
  }

  navigateTo(page:string){
    this.appCtrl.getRootNav().push(page);
  }

  goBack() {
    this.navCtrl.push('RestaurantPage');
  }

  loadProdSpecialOffer(){
    let bg = '';
    var mer_id =  localStorage.getItem('new_merchant_id')

    this.restoCat.getProducts(parseInt(mer_id), this.new_catID).subscribe(
      res => {
        let resultset: any = res;
        this.specialOffers = resultset.data;

        console.log("Special Offer", this.specialOffers);     
      }, error => {
        console.log(error);
    });
  }

  getCartID(){

    this.storage.get('userID').then((value)=>{
      this.userID = value;

      console.log('user_id', this.userID);
  
      this.storage.get('merchantID').then((value)=>{
        this.merchantID = value;

        this.storage.get('tableNumber').then((value)=>{
          this.tableNumber = value;

          this.cartProvider.cartMain(this.userID, this.merchantID, this.tableNumber).subscribe(
            res => {
              let resultset: any = res;
              this.main = resultset.data;
              console.log("main", this.main);
              console.log("cartid", this.main[0].cart_id);
              console.log("mainid", this.main[0].ipayu_id);
      
              this.cart_id = this.main[0].cart_id
              //this.ipayu_id = this.main[0].ipayu_id
      
              this.storage.set('cartID', this.cart_id);
      
            }, error => {
              console.log(error);
          });
        
        });
      
      });
    
    });
  }

  loadCatName(){
    this.restoCat.getCategory(this.new_catID).subscribe(
      res => {
        let resultset: any = res;
        this.category = resultset.data;

        console.log("category", this.category);     
      }, error => {
        console.log(error);
    });
  }

  loadShopInfo(){
    var mer_id =  localStorage.getItem('new_merchant_id')
    this.merchantProvider.merchantInfo(parseInt(mer_id)).subscribe(
      res => {
        let resultset: any = res;
        this.merchants = resultset.data;

        console.log("merchants", this.merchants);
      }, error => {
        console.log(error);
    });
  }

  add(pid: number){
    this.counter += 1;
  }

  remove(pid: number){
    if(this.counter > 1){
      this.counter -= 1;
    }
  }

  openCart(){
    this.appCtrl.getRootNav().push('MenuOrderingPage');
    // console.log("shop")
  }

  openProductDetail(prod_id){
    this.merchantParamId
    localStorage.setItem('merch_id', this.merchantParamId)

    let loader = this.loadingCtrl.create({
      content: 'Loading Page ... ',
    });

    loader.present();

    setTimeout(() => {

      this.navCtrl.push('RestoFoodDetailPage', 
      {prod_id: prod_id});

    }, 1000);

    setTimeout(() => {
      loader.dismiss();
    }, 3000);

  }

  loadCartDetailCounter(){

    this.storage.get('cartID').then((value)=>{
      this.cartID = value;

      console.log('cartID', this.cartID);

    this.storage.get('merchantID').then((value)=>{
      this.merchantID = value;

      console.log('merchantID', this.merchantID);


        this.cartProvider.getCartDetailCounter(this.cartID, this.merchantID).subscribe(
          res => {
            let resultset: any = res;
            this.cartcounter = resultset.data;
            
            console.log("counter", this.cartcounter[0].detail_counter);
            this.counterFront = this.cartcounter[0].detail_counter
    
            if (this.cartcounter[0].detail_counter == 0)
            this.counterFront + 1
            
          }, error => {
            console.log(error);
        });

    });

  });

  }


  addtoOrder(id: number, amt: number, mid: number, elementId: string){
    var mer_id =  localStorage.getItem('new_merchant_id')
    this.my_amt = amt;
    this.my_id = id;
    this.table_num = localStorage.getItem('table_number');

    this.cartProvider.cartCounter(parseInt(mer_id)).subscribe(
      res => {
        let resultset: any = res;
        this.count_order = resultset.data;
        console.log("first", this.count_order);

        if (this.count_order[0].total_count == 0){
          console.log("insert")

          let modalTable = this.modalCtrl.create('Table2Page', {
            pid: id, amt: amt, mid: mer_id, count: this.counter
          })
          modalTable.present()

        }else{
          var mer_id =  localStorage.getItem('new_merchant_id')
          console.log("insert/update")

          this.cartProvider.postCart(parseInt(this.localStorageUserId), 1, parseInt(this.table_num), id, this.counter, amt, parseInt(mer_id)).subscribe(
            res => {
              let resultset: any = res;
              this.orders = resultset.data;
              console.log("orders", this.orders);
              
            }, error => {
              console.log(error);
          });

          this.cartProvider.cartMain(parseInt(this.localStorageUserId), parseInt(mer_id), this.table_num).subscribe(
            res => {
              let resultset: any = res;
              this.main = resultset.data;
              console.log("main", this.main);
              console.log("cartid", this.main[0].cart_id);
              console.log("mainid", this.main[0].ipayu_id);
      
              let cartStorageVal = this.merchantID + 'cartID';

              console.log('merVal', cartStorageVal);

              this.storage.set(cartStorageVal, this.main[0].cart_id);
      
            }, error => {
              console.log(error);
          });

          const toast = this.toastCtrl.create({
            message: 'Order saved in the cart.',
            duration: 2000
          });

          toast.present();

          this.navCtrl.setRoot(this.navCtrl.getActive().component);
        }

      }, error => {
        console.log(error);
    });
  }

  goToReservation(){
    this.navCtrl.push('BlankPage').then(() => this.appCtrl.getRootNav().setRoot('RestaurantPage'))
  }

  open_Toggle(){
    this.openopen = true;
    this.closeToggle = false;
    this.openToggle = true;
  }

  close_Toggle(){
    this.openopen = false;
    this.closeToggle = true;
    this.openToggle = false;
  }

  openMenu(cit: any) {
    this.navCtrl.push('BlankPage').then(() => this.appCtrl.getRootNav().setRoot('MenuCategoryPage', {
      cit: cit
    }))

    localStorage.setItem('cat_id', cit)
  }

  menu(){
    console.log('hey')
  }

}
