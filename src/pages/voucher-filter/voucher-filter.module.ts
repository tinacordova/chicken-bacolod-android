import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VoucherFilterPage } from './voucher-filter';

@NgModule({
  declarations: [
    VoucherFilterPage,
  ],
  imports: [
    IonicPageModule.forChild(VoucherFilterPage),
  ],
})
export class VoucherFilterPageModule {}
