import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, App, LoadingController } from 'ionic-angular';
import { Nav, Platform, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { MerchantProvider } from '../../providers/merchant/merchant';
import { PromoProvider } from '../../providers/promo/promo';
import { VoucherProvider } from '../../providers/voucher/voucher';
import { ProductProvider } from '../../providers/product/product';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';

/**
 * Generated class for the YupangcoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-yupangco',
  templateUrl: 'yupangco.html',
})
export class YupangcoPage {
  @ViewChild(Nav) nav: Nav;

  pages: Array<{title: string, component: any}>;

  isAndroid: boolean = false;
  merchantParamId: number;
  merchants: any [];
  promos: any [];
  vouchers: any [];

  constructor(
    public navCtrl: NavController, 
    public menuCtrl: MenuController, 
    public navParams: NavParams,
    public appCtrl: App,
    public merchantProvider: MerchantProvider,
    public promoProvider: PromoProvider,
    public voucherProvider: VoucherProvider,
    private loadingCtrl: LoadingController,
    public productProvider: ProductProvider,
    public http: Http,
    platform: Platform, 
    statusBar: StatusBar,
    splashScreen: SplashScreen) {
      this.merchantParamId = this.navParams.get('merchant_id');
  

  this.productProvider.getProductMerchantCategory(this.merchantParamId).subscribe(
    res => {
      let resultset: any = res;
      // this.prodCategories = resultset.data;

      // this.category_ids = this.prodCategories.map(x=>x.cat_id);
      // this.categories = this.prodCategories.map(t=>t.cat_name);
      // this.category = "";

      // console.log('cats id', this.category_ids); 
      // console.log('cats', this.categories); 

    }, error => {
      console.log(error);
  });
}

ionViewDidLoad() {
  this.loadShopInfo();
  this.loadMerchantPromo();
  this.loadMerchantVoucher();
  this.loadProductMerchantCategory();
}

segmentChanged(){
  console.log("test");
}

doRefresh(refresher) {
  console.log('Begin async operation', refresher);

  this.loadShopInfo();
  this.loadMerchantPromo();
  this.loadMerchantVoucher();
  this.loadProductMerchantCategory();

  setTimeout(() => {
    console.log('Async operation has ended');
    refresher.complete();
  }, 2000);
}

loadShopInfo(){
  this.merchantProvider.merchantInfo(this.merchantParamId).subscribe(
    res => {
      let resultset: any = res;
      this.merchants = resultset.data;

      console.log("merchants", this.merchants);
    }, error => {
      console.log(error);
  });
}

loadMerchantPromo(){
  let bg = '';
  this.promoProvider.getMerchantPromo(this.merchantParamId).subscribe(
    res => {
      let resultset: any = res;
      this.promos = resultset.data;

      console.log("merchant promos", this.promos);     
    }, error => {
      console.log(error);
  });
}

loadMerchantVoucher(){
  let bg = '';
  this.voucherProvider.getMerchantVoucher(this.merchantParamId).subscribe(
    res => {
      let resultset: any = res;
      this.vouchers = resultset.data;
      
      console.log("merchant vouchers", this.vouchers);     
    }, error => {
      console.log(error);
  });
}

loadProductMerchantCategory(){
  let bg = '';
  this.productProvider.getProductMerchantCategory(this.merchantParamId).subscribe(
    res => {
      let resultset: any = res;
      // this.prodCategories = resultset.data;
      
      // console.log("product merchant cat", this.prodCategories);     
    }, error => {
      console.log(error);
  });
}

goBack() {
  this.navCtrl.pop();
}

goNext(){
  this.appCtrl.getRootNav().push('YupangVoucDetailPage');
}

goPromo(){
  this.appCtrl.getRootNav().push('YupangPromoDetailPage');
}

openMenuDetail(){
  this.appCtrl.getRootNav().push('YupangFoodDetailPage');
}

openSpecialDetail(){
  this.appCtrl.getRootNav().push('YupangSpecialDetailPage');
}

openPage() {
  this.appCtrl.getRootNav().push('YupangcoBranchPage');
  // this.nav.setRoot('YupangcoBranchPage');
  // this.activeMenuIndex = i;
  this.menuCtrl.open();
  }

}
