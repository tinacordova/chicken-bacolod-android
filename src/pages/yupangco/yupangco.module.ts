import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { YupangcoPage } from './yupangco';

@NgModule({
  declarations: [
    YupangcoPage,
  ],
  imports: [
    IonicPageModule.forChild(YupangcoPage),
  ],
})
export class YupangcoPageModule {}
