import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VoucherSearchResultPage } from './voucher-search-result';

@NgModule({
  declarations: [
    VoucherSearchResultPage,
  ],
  imports: [
    IonicPageModule.forChild(VoucherSearchResultPage),
  ],
})
export class VoucherSearchResultPageModule {}
