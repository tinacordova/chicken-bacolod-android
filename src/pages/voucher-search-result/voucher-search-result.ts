import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';

/**
 * Generated class for the VoucherSearchResultPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-voucher-search-result',
  templateUrl: 'voucher-search-result.html',
})
export class VoucherSearchResultPage {

  constructor(
    public navCtrl: NavController, 
    public appCtrl: App, 
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VoucherSearchResultPage');
  }

  goTo(){
    this.appCtrl.getRootNav().push('VoucherFilterPage');
    console.log("sana");
  }

  goBack() {
    this.navCtrl.pop();
  }


}
