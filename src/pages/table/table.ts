import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, App, ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { CartProvider } from '../../providers/cart/cart';

@IonicPage()
@Component({
  selector: 'page-table',
  templateUrl: 'table.html',
})
export class TablePage {

  table_num: number;
  location: any;
  prod_id: number;
  amount: number;
  merch_id: number;
  counter: number;
  orders: any [];
  main: any [];
  merchantID: number;
  userID: number;

  cartid: any;
  cart_id: any;
  ipayu_id: any;
  cartData: any = [];
  arrayData: any = [];

  outID: any;

  localStorageUserId: string;
  localStorageFirstName: string;
  localStorageLastName: string;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public viewCtrl: ViewController,
              public appCtrl: App,
              public cartProvider: CartProvider,
              private storage: Storage,
              public toastCtrl: ToastController) {

      this.prod_id = this.navParams.get('pid');
      this.amount = this.navParams.get('amt');
      this.merch_id = this.navParams.get('mid');
      this.counter = this.navParams.get('count');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TablePage');

    this.localStorageUserId = localStorage.getItem('user_id');
    this.localStorageFirstName = localStorage.getItem('user_firstname');
    this.localStorageLastName = localStorage.getItem('user_lastname');
  }

  submit(){

    console.log('sample123', this.location);

    console.log(this.merch_id, "table: ", this.location);

    localStorage.setItem('table_number', this.location);

    this.storage.set('tableNumber', parseInt(this.location));

    //var mer_id =  localStorage.getItem('new_merchant_id')

    this.storage.get('merchantID').then((value)=>{
      this.merchantID = value;

      // let items = [];
      // this.storage.forEach((v,k,i) => {
      //     items.push(v);
      // }).then(() => {
      //     items;
  
      //Items Array Value
      //0-cartID
      //1-ipayuID
      //2-merchantID
      //3-userID
  
      //     console.log('arr', items);
      //     console.log('cart_id', items['0']);
      //     console.log('ipayu_id', items['1']);
      //     console.log('merchant_id', items['2']);
      //     console.log('user_id', items['3']);
          
      // console.log('items', items);


      this.storage.get('userID').then((value)=>{
        this.userID = value;

        this.cartProvider.postCart(this.userID, 1, this.location, this.prod_id, this.counter, this.amount, this.merchantID).subscribe(
          res => {
            let resultset: any = res;
            this.orders = resultset.data;
            console.log("orders", this.orders);
    
          }, error => {
            console.log(error);
        });
    
        this.cartProvider.cartMain(this.userID, this.merchantID, parseInt(this.location)).subscribe(
          res => {
            let resultset: any = res;
            this.main = resultset.data;
            console.log("main", this.main);
            console.log("cartid", this.main[0].cart_id);
            console.log("mainid", this.main[0].ipayu_id);
    
            this.cart_id = this.main[0].cart_id
            this.ipayu_id = this.main[0].ipayu_id
    
            this.storage.set('cartID', this.cart_id);
    
          }, error => {
            console.log(error);
        });
    
            
          this.appCtrl.getRootNav().push('RestaurantPage');
          this.viewCtrl.dismiss()  
          
          const toast = this.toastCtrl.create({
            message: 'Order saved in the cart.',
            duration: 2000
        });
    
        toast.present();
      
      });  
  
  

  
  
      //});



    });


   


   


  }

  get(value: number){
    this.table_num = value;
    console.log("value", value)
  }

  dismiss(){
    this.viewCtrl.dismiss()
  }

  close(){
    this.viewCtrl.dismiss()
  }

}
