import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App, AlertController, ToastController } from 'ionic-angular';

import { PromoProvider } from '../../providers/promo/promo';
import { PromoRatingProvider } from '../../providers/promo-rating/promo-rating';

import { SocialSharing } from '@ionic-native/social-sharing';
import { Calendar } from '@ionic-native/calendar';

import { ModalController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-promo-modal',
  templateUrl: 'promo-modal.html',
})
export class PromoModalPage {
  promoParamId: number;
  promos: any [];
  promos_rating: any [];

  // Social Sharing
  message: string = null;
  file: string = null;
  link: string = null;
  subject: string = null;

  // Calendar
  event;

  ave: string;
  noZeroes: string;
  rate: number;

  // Stars
  starOneHalf: boolean = false
  starOneFull: boolean = false
  starTwoHalf: boolean = false
  starTwoFull: boolean = false
  starThreeHalf: boolean = false
  starThreeFull: boolean = false
  starFourHalf: boolean = false
  starFourFull: boolean = false
  starFiveHalf: boolean = false
  starFiveFull: boolean = false


  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public appCtrl: App,
    public promoProvider: PromoProvider,
    public socialSharing: SocialSharing,
    public alertCtrl: AlertController,
    public calendar: Calendar,
    public promoRating: PromoRatingProvider,
    public toastCtrl: ToastController,
    public modalCtrl: ModalController) {
    this.promoParamId = this.navParams.get('promo_id');
  }

  ionViewDidLoad() {
    this.loadPromoDetail();
    this.loadPromoRating();
  }

  loadPromoDetail(){
    this.promoProvider.getPromoDetail(this.promoParamId).subscribe(
      res => {
        let resultset: any = res;
        this.promos = resultset.data;
        console.log("promo detail", this.promos);
      }, error => {
        console.log(error);
    });
  }

  share(promo_name, promo_description, promo_url, promo_start, promo_end, promo_add){
    console.log(promo_description, promo_name, promo_url)
    // const toast = this.toastCtrl.create({
    //   message: 'Sharing...',
    //   duration: 2000
    // });
    // toast.present();
    let newMesage = (promo_description + ' @ ' + promo_add + " that will starts on " + promo_start + " and ends at " + promo_end)
    console.log(newMesage)

    this.socialSharing.share(newMesage, promo_name, promo_url)
              .then(()=>{
              }).catch( e => {
              });
  }

  openCalendar(promoName, valEnd, valStart, promoLoc, promoDesc){
    let modalRating = this.modalCtrl.create('CalendarPage', {
      promoName: promoName,
      valEnd: valEnd,
      valStart: valStart,
      promoLoc: promoLoc,
      promoDesc: promoDesc
    })
    modalRating.present()
  }

  loadPromoRating(){
    this.promoRating.getPromoRating(this.promoParamId).subscribe(
      res => {
        let resultset: any = res;
        this.promos_rating = resultset.data;
        console.log("promo rating ave", this.promos_rating);
        console.log(this.promos_rating[0].rating_average);
        this.ave = this.promos_rating[0].rating_average
        var twoPlacedFloat = parseFloat(this.ave).toFixed(1)
        console.log(twoPlacedFloat)
        this.ave = twoPlacedFloat

        // let wakoko = .7

        if(this.promos_rating[0].rating_average >= 0.1 &&  this.promos_rating[0].rating_average <=  0.9){
          console.log("half")
          this.starOneHalf = true;
        }
        if(this.promos_rating[0].rating_average == 1){
          console.log("one")
          this.starOneFull = true;
        }
        if(this.promos_rating[0].rating_average >= 1.1 &&  this.promos_rating[0].rating_average <=  1.9){
          console.log("onehalf")
          this.starTwoHalf = true;
        }
        if(this.promos_rating[0].rating_average == 2){
          console.log("two")
          this.starTwoFull = true;
        }
        if(this.promos_rating[0].rating_average >= 2.1 &&  this.promos_rating[0].rating_average <=  2.9){
          console.log("twohalf")
          this.starThreeHalf = true;
        }
        if(this.promos_rating[0].rating_average == 3){
          console.log("three")
          this.starThreeFull = true;
        }
        if(this.promos_rating[0].rating_average >= 3.1 &&  this.promos_rating[0].rating_average <=  3.9){
          console.log("threehalf")
          this.starFourHalf = true;
        }
        if(this.promos_rating[0].rating_average == 4){
          console.log("four")
          this.starFourFull = true;
        }
        if(this.promos_rating[0].rating_average >= 4.1 &&  this.promos_rating[0].rating_average <=  4.9){
          console.log("fourhalf")
          this.starFiveHalf = true;
        }
        if(this.promos_rating[0].rating_average == 5){
          console.log("five")
          this.starFiveFull = true;
        }

      }, error => {
        console.log(error);
    });
  }

  onModelChange($event){
    console.log($event)
    // this.rate = 5
  }

  rating(promo_id: number){
    console.log("rate us")
    let modalRating = this.modalCtrl.create('PromoRatingPage', {
      promo_id: promo_id
    })
    modalRating.present()
  }

  goBack() {
    this.navCtrl.pop();
  }

  navigateTo(page:string){
    this.appCtrl.getRootNav().push(page);
  }

  openModal(){
    this.navCtrl.push('PromoModalPage');
  }

  openSearchTo(){
    this.navCtrl.push('PromoSearchResultPage');
  }
}
