import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PromoModalPage } from './promo-modal';

import { IonicRatingModule } from "ionic-rating";

@NgModule({
  declarations: [
    PromoModalPage,
  ],
  imports: [
    IonicPageModule.forChild(PromoModalPage),
    IonicRatingModule
  ],
})
export class PromoModalPageModule {}
