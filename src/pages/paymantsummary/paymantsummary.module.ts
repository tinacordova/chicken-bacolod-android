import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaymantsummaryPage } from './paymantsummary';

@NgModule({
  declarations: [
    PaymantsummaryPage,
  ],
  imports: [
    IonicPageModule.forChild(PaymantsummaryPage),
  ],
})
export class PaymantsummaryPageModule {}
