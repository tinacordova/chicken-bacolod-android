import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, App, ToastController } from 'ionic-angular';

import { CartProvider } from '../../providers/cart/cart';

@IonicPage()
@Component({
  selector: 'page-paymantsummary',
  templateUrl: 'paymantsummary.html',
})
export class PaymantsummaryPage {

  carts: any [];
  update: any [];
  transaction: any [];

  f_vat: any;
  f_grand: any;
  cash: number;
  change: any;

  ipayu_id: number;
  cart_id: number;
  discount: number;
  p_cash: number;
  p_card: number;

  merc_id: any

  show: boolean = false;
  total: number;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public cartProvider: CartProvider,
              public viewCtrl: ViewController,
              public appCtrl: App,
              public toastCtrl: ToastController) {

      this.cash = this.navParams.get('cash')
      console.log("merchant id", localStorage.getItem('new_merchant_id'))
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PaymantsummaryPage');
    this.loadCartMain()
    
  }

  loadCartMain(){
    // let newItems:any = localStorage.getItem('items')
    console.log('cash', this.cash)

    this.cartProvider.getCart(1, this.merc_id).subscribe(
      res => {
        let resultset: any = res;
        this.carts = resultset.data;
        console.log("cart", this.carts);

        this.ipayu_id = this.carts[0].ipayu_id;
        this.cart_id = this.carts[0].cart_id;
        this.discount = 0.00;
        // this.p_cash
        this.p_card = 0.00

        var sub = this.carts[0].grand_total
        let vat = sub * .12

        let grand_total = (parseInt(sub) + vat + 50)

        this.f_grand = grand_total.toFixed(2)
        this.f_vat = vat.toFixed(2)

        if (this.cash < +this.f_grand || this.cash == undefined){
          this.show = true;
          console.log('disabled')

          this.change = "0.00"
          console.log(this.change)
        }

        if (this.cash >= +this.f_grand){
          this.show = false;
          console.log('enabled')

          let n_change = this.cash - this.f_grand
          this.change = n_change.toFixed(2)
          console.log ("change", n_change)
        }

      }, error => {
        console.log(error);
    });
  }

  pay(){
    this.merc_id =  localStorage.getItem('new_merchant_id')

    this.cartProvider.getCartPayment(this.ipayu_id, this.cart_id, 
              this.discount, this.cash, this.p_card, this.change).subscribe(
      res => {
        let resultset: any = res;
        this.update = resultset.data;
        console.log("update", this.update);

      }, error => {
        console.log(error);
    });

    this.cartProvider.saveTransaction(this.ipayu_id, this.f_grand, this.merc_id, 123, 1, this.cart_id,
                                      this.cash, this.p_card, this.change).subscribe(
        res => {
        let resultset: any = res;
        this.transaction = resultset.data;
        console.log("transaction", this.transaction);

        }, error => {
        console.log(error);
        });

    const toast = this.toastCtrl.create({
      message: 'Payment successfull.',
      duration: 5000
    });
    toast.present();

    this.viewCtrl.dismiss().then(() => this.appCtrl.getRootNav().setRoot('RestaurantPage'))

  }

  dismiss(){
    this.viewCtrl.dismiss()
  }

}
