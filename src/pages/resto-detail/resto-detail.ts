import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the RestoDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-resto-detail',
  templateUrl: 'resto-detail.html',
})
export class RestoDetailPage {

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RestoDetailPage');
  }

  openProductDetail(product_id){
    this.navCtrl.push('RestoModalPage', 
    {product_id: product_id
    });
  }

}
