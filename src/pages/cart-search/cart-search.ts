import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-cart-search',
  templateUrl: 'cart-search.html',
})
export class CartSearchPage {

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CartSearchPage');
  }

  goBack(){
    this.navCtrl.pop()
  }

}
