import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CartSearchPage } from './cart-search';

@NgModule({
  declarations: [
    CartSearchPage,
  ],
  imports: [
    IonicPageModule.forChild(CartSearchPage),
  ],
})
export class CartSearchPageModule {}
