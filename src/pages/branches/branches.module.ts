import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BranchesPage } from './branches';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    BranchesPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(BranchesPage),
  ],
})
export class BranchesPageModule {}
