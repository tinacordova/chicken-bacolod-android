import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';

import { MerchantProvider } from '../../providers/merchant/merchant';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the BranchesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-branches',
  templateUrl: 'branches.html',
})
export class BranchesPage {

  merchantID: number;
  merchants: any = [];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public appCtrl: App,
    public merchantProvider: MerchantProvider,
    private storage: Storage) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BranchesPage');
    this.loadShopInfo();
  }

  loadShopInfo(){

    this.storage.get('merchantID').then((val)=>{
      this.merchantID = val;

      this.merchantProvider.merchantInfo(this.merchantID).subscribe(
        res => {
          let resultset: any = res;
          this.merchants = resultset.data;
  
          console.log("merchants", this.merchants);
        }, error => {
          console.log(error);
      });
    
    });
  }

  toWallet() {
    this.appCtrl.getRootNav().push("WalletFabPage");
  }
  
  toProduct() {
    this.appCtrl.getRootNav().push("SpecialOfferPage");
  }

  toPromo() {
    this.appCtrl.getRootNav().push("PromoPage");
  }

  toResto() {
    this.appCtrl.getRootNav().push("VoucherFabPage");
  }

  navScreen(page:string) {
    this.appCtrl.getRootNav().push(page);
  }

  goBack() {
    this.navCtrl.pop();
  }

}
