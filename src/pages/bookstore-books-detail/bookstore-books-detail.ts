import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the BookstoreBooksDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-bookstore-books-detail',
  templateUrl: 'bookstore-books-detail.html',
})
export class BookstoreBooksDetailPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BookstoreBooksDetailPage');
  }

  goBack() {
    this.navCtrl.pop();
  }

}
