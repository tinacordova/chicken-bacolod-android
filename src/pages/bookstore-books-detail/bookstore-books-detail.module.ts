import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BookstoreBooksDetailPage } from './bookstore-books-detail';

@NgModule({
  declarations: [
    BookstoreBooksDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(BookstoreBooksDetailPage),
  ],
})
export class BookstoreBooksDetailPageModule {}
