import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  App,
  LoadingController,
  Platform,
  ModalController
} from "ionic-angular";
import { Storage } from '@ionic/storage';

import { BannerProvider } from "../../providers/banner/banner";
import { ProductProvider } from "../../providers/product/product";
import { MerchantProvider } from "../../providers/merchant/merchant";
import { VoucherProvider } from "../../providers/voucher/voucher";
import { PromoProvider } from "../../providers/promo/promo";
import { WalletProvider } from "../../providers/wallet/wallet";

import { Observable } from "rxjs";
import { debounceTime, distinctUntilChanged } from "rxjs/operators";

@IonicPage()
@Component({
  selector: "page-home",
  templateUrl: "home.html"
})
export class HomePage {
  banners: any[];
  products: any[];
  merchants: any[];
  vouchers: any[];
  promos: any[];
  merchant10: any[];
  merchant20: any[];
  promoCategories: any = [];
  wallet_voucher: any[];
  apps: any[];
  resCount: number;
  clearStatus: Boolean = false;

  promoCurrentPage: number = 1;
  promoTotalPage: number = 1;
  promoLoad: boolean = true;

  new: any;

  searchTerm: string;
  modelChanged: any;
  localStorageUserId: string;
  localStorageFirstName: string;
  localStorageLastName: string;

  searchChangeObserver;

  constructor(
    public navCtrl: NavController,
    public bannerProvider: BannerProvider,
    public productProvider: ProductProvider,
    public merchantProvider: MerchantProvider,
    public voucherProvider: VoucherProvider,
    public promoProvider: PromoProvider,
    public appCtrl: App,
    public modalCtrl: ModalController,
    private loadingCtrl: LoadingController,
    public platform: Platform,
    private storage: Storage,
    public walletProvider: WalletProvider
  ) {}

  ionViewDidEnter() {
    this.myApps();
    this.resetStorage();

  }

  ionViewDidLoad() {
    this.localStorageUserId = localStorage.getItem('user_id');
    this.localStorageFirstName = localStorage.getItem('user_firstname');
    this.localStorageLastName = localStorage.getItem('user_lastname');

    console.log('user id: ', this.localStorageUserId)

    this.new = localStorage.getItem('cartid');

    let loader = this.loadingCtrl.create({
      content: "Loading Page ... "
    });

    loader.present();

    setTimeout(() => {
      this.resetStorage();
      this.loadBanner();
      this.loadProduct();
      this.loadMerchant();
      this.loadVoucher();
      this.loadPromoCategory();
      this.loadPromoCategory();
      this.loadmerchant10();
      this.loadmerchant20();
    }, 1000);

    setTimeout(() => {
      loader.dismiss();
    }, 3000);
  }

  resetStorage(){
    this.storage.remove('cartID');
    this.storage.remove('merchantID');
    //this.storage.remove('tableNumber');
  }

  doInfinite(infiniteScroll) {
    console.log("Begin async operation");
    this.promoCurrentPage = this.promoCurrentPage + 1;

    this.promoProvider.getPromoHomeLimit(this.promoCurrentPage).subscribe(
      res => {
        let resultset: any = res;
        this.promos = resultset.data;
        console.log("promo list limit", this.promos);

        infiniteScroll.complete();
      },
      error => {
        console.log(error);
      }
    );
  }

  doRefresh(refresher) {
    console.log("Begin async operation", refresher);

    this.banners = [];
    this.products = [];
    this.merchants = [];
    this.merchant10 = [];
    this.merchant20 = [];
    this.vouchers = [];

    this.loadBanner();
    this.loadProduct();
    this.loadMerchant();
    this.loadVoucher();
    this.loadPromo();
    this.loadmerchant10();
    this.loadmerchant20();
    this.myApps();

    setTimeout(() => {
      console.log("Async operation has ended");
      refresher.complete();
    }, 2000);
  }

  loadBanner() {
    console.log("Banner Loads!");
    this.bannerProvider.getBanner().subscribe(
      res => {
        let resultset: any = res;
        this.banners = [];

        this.banners = resultset.data;
        console.log("banner", this.banners);
      },
      error => {
        console.log(error);
      }
    );
  }

  loadProduct() {
    console.log("Product Loads!");
    this.productProvider.getProductsHome().subscribe(
      res => {
        let resultset: any = res;
        let viewNum = 0;
        this.products = [];

        this.products = resultset.data;
        viewNum = this.products.length;

        console.log("num", this.products.length);

        console.log("prod", this.products);
      },
      error => {
        console.log(error);
      }
    );
  }

  loadMerchant() {
    console.log("Merchant Loads!");
    this.merchantProvider.getMerchant().subscribe(
      res => {
        let resultset: any = res;
        this.merchants = [];

        this.merchants = resultset.data;
        console.log("merc", this.merchants);
      },
      error => {
        console.log(error);
      }
    );
  }

  loadVoucher() {
    console.log("Voucher Loads!");
    this.voucherProvider.getVoucherHome().subscribe(
      res => {
        let resultset: any = res;
        this.vouchers = [];

        this.vouchers = resultset.data;
        console.log("voucher home", this.vouchers);
      },
      error => {
        console.log(error);
      }
    );
  }

  loadPromo() {
    this.promoLoad = true;

    console.log("Promo Loads!");
    this.promoProvider.getPromoCategoryItems().subscribe(
      res => {
        let resultset: any = res;
        this.promoCategories = resultset.data;
        console.log("promo list category", this.promoCategories);
      },
      error => {
        console.log(error);
      }
    );
  }

  loadPromoCategory() {
    this.promoLoad = false;

    this.promoProvider.getPromoCategoryItems().subscribe(
      res => {
        let resultset: any = res;
        this.promoCategories = resultset.data;
        console.log("promo list category", this.promoCategories);
      },
      error => {
        console.log(error);
      }
    );
  }

  myApps() {
    let data = {
      user_id: localStorage.getItem("user_id")
    };

    this.merchantProvider.getAppList(data).subscribe(
      res => {
        let resultset: any = res;
        this.apps = resultset.data;

        console.log("my apps: ", this.apps);
      },
      error => {
        console.log(error);
      }
    );
  }

  loadmerchant10() {
    console.log("Merchant 10");
    this.merchantProvider.merchant10().subscribe(
      res => {
        let resultset: any = res;
        this.merchant10 = [];

        this.merchant10 = resultset.data;

        console.log("merchant 10", this.merchant10);
      },
      error => {
        console.log(error);
      }
    );
  }

  loadmerchant20() {
    console.log("Merchant 20");
    this.merchantProvider.merchant20().subscribe(
      res => {
        let resultset: any = res;
        this.merchant20 = [];

        this.merchant20 = resultset.data;
        console.log("merchant 20", this.merchant20);
      },
      error => {
        console.log(error);
      }
    );
  }

  goToAppSearch(page: string) {
    let loader = this.loadingCtrl.create({
      content: "Loading Page ... "
    });

    loader.present();

    setTimeout(() => {
      this.navCtrl.push(page);
    }, 1000);

    setTimeout(() => {
      loader.dismiss();
    }, 3000);
  }

  goToApp(page: string, merchant_id: number) {
    localStorage.setItem("new_merchant_id", merchant_id.toString());
    this.storage.set('merchantID', merchant_id);
    

    let loader = this.loadingCtrl.create({
      content: "Loading Page ... "
    });

    loader.present();

    setTimeout(() => {
      this.navCtrl.push(page, { merchant_id: merchant_id });
    }, 1000);

    setTimeout(() => {
      loader.dismiss();
    }, 3000);
  }

  navigateTo(page: string) {
    this.appCtrl.getRootNav().push(page);
  }

  toProduct() {
    this.appCtrl.getRootNav().push("SpecialOfferPage");
  }

  toPromo() {
    this.appCtrl.getRootNav().push("PromoPage");
  }

  toResto() {
    this.appCtrl.getRootNav().push("VoucherFabPage");
  }

  // toFood() {
  //   this.appCtrl.getRootNav().push("RestaurantNewPage");
  // }

  toWallet() {
    this.appCtrl.getRootNav().push("WalletFabPage");
  }

  openVoucherDetail(voucher_id) {
    let loader = this.loadingCtrl.create({
      content: "Loading Page ... "
    });

    loader.present();

    setTimeout(() => {
      this.navCtrl.push("VoucherModalPage", { voucher_id: voucher_id });
    }, 1000);

    setTimeout(() => {
      loader.dismiss();
    }, 3000);
  }

  openPromoDetail(promo_id) {
    let loader = this.loadingCtrl.create({
      content: "Loading Page ... "
    });

    loader.present();

    setTimeout(() => {
      this.navCtrl.push("PromoModalPage", { promo_id: promo_id });
    }, 1000);

    setTimeout(() => {
      loader.dismiss();
    }, 3000);
  }

  openProductDetail(prod_id) {
    let loader = this.loadingCtrl.create({
      content: "Loading Page ... "
    });

    loader.present();

    setTimeout(() => {
      this.navCtrl.push("HomeSpecialDetailPage", { prod_id: prod_id });
    }, 1000);

    setTimeout(() => {
      loader.dismiss();
    }, 3000);
  }

  universalSearch(ev: any) {
    setTimeout(() => {
      const val = ev.target.value;

      console.log("Searching...", val);

      this.navCtrl.push("UniversalSearchPage", {
        keyword: val
      });
    }, 3000);
  }

  universalSearchEnter(ev: any) {
    const val = ev.target.value;

    console.log("Search on enter", val);

    this.navCtrl.push("UniversalSearchPage", {
      keyword: val
    });
  }

  onSearchChange(searchValue: string) {
    if (!this.searchChangeObserver) {
      Observable.create(observer => {
        this.searchChangeObserver = observer;
      })
        .pipe(debounceTime(1500)) // wait 300ms after the last event before emitting last event
        .pipe(distinctUntilChanged()) // only emit if value is different from previous value
        .subscribe(res => {
          this.resCount = res.length;

          if (
            this.resCount > 0 &&
            res.length > 0 &&
            this.clearStatus === false
          ) {
            this.navCtrl.push("UniversalSearchPage", {
              origin: "uni-search",
              keyword: res
            });
          }

          this.clearStatus = false;
        });
    }

    this.searchChangeObserver.next(searchValue);
  }

  onClear(event: any) {
    //This is a marker that the clear search was clicked (the x button)
    this.clearStatus = true;
  }

  onChange(event: any) {
    //Get the string length of the search bar and assign to resCount
    this.resCount = event.target.value.length;
  }

  openUniCart(){
    this.navCtrl.push('CartSearchPage')
  }
}
