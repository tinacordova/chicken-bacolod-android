import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App, LoadingController, AlertController, MenuController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { WalletProvider } from '../../providers/wallet/wallet';
import { MerchantProvider } from '../../providers/merchant/merchant';
import { ReservationProvider } from '../../providers/reservation/reservation';
import { CartProvider } from '../../providers/cart/cart';
import { TransactionProvider } from '../../providers/transaction/transaction';

import { HttpClient } from '@angular/common/http/';
import { HttpHeaders } from '@angular/common/http';

@IonicPage()
@Component({
  selector: 'page-wallet-fab',
  templateUrl: 'wallet-fab.html',
})
export class WalletFabPage {

  pet: string = "reserve";
  //category: string = "appetizers";
  isAndroid: boolean = false;
  wallet_voucher: any = [];
  merchant_voucher: any = [];
  reservations: any = [];
  merchants: any = [];
  reservationStatus: any;
  fcm_num: any [];
  merch_id: number;
  orders: any [];
  voucher_use: boolean = true;
  voucher_used: boolean;
  userID: number;
  merchantID: number;
  tableNumber: number;
  main: any;
  cart_id: number;
  cartcounter: any [];
  counterFront: number;
  transactions: any;

  localStorageUserId: string;
  localStorageFirstName: string;
  localStorageLastName: string;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public appCtrl: App,
    private loadingCtrl: LoadingController,
    public walletProvider: WalletProvider,
    public merchantProvider: MerchantProvider,
    public alertCtrl: AlertController,
    public reservationProvider: ReservationProvider,
    private transactionProvider: TransactionProvider,
    private http: HttpClient,
    private storage: Storage,
    public menuCtrl: MenuController,
    public cartProvider: CartProvider) {
      
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WalletFabPage');
    this.getCartID();
    this.loadShopInfo();
    this.displayWalletVoucher()
    this.loadAppointmentList();
    this.loadOrderList()

    this.localStorageUserId = localStorage.getItem('user_id');
    this.localStorageFirstName = localStorage.getItem('user_firstname');
    this.localStorageLastName = localStorage.getItem('user_lastname');
  }

  getCartID(){

    this.storage.get('userID').then((value)=>{
      this.userID = value;

      console.log('user_id', this.userID);
  
      this.storage.get('merchantID').then((value)=>{
        this.merchantID = value;

        console.log('merchant_id', this.merchantID);

        this.storage.get('tableNumber').then((value)=>{
          this.tableNumber = value;

          this.cartProvider.cartMain(this.userID, this.merchantID, 0).subscribe(
            res => {
              let resultset: any = res;
              this.main = resultset.data;
              console.log("main", this.main);
              console.log("cartid", this.main[0].cart_id);
              console.log("mainid", this.main[0].ipayu_id);

              this.loadCartDetailCounter(this.main[0].cart_id);
      
              this.cart_id = this.main[0].cart_id      
              this.storage.set('cartID', this.cart_id);
      
            }, error => {
              console.log(error);
          });
        
        });
      
      });
    
    });
  }

  loadCartDetailCounter(cartID){

    this.storage.get('merchantID').then((value)=>{
      this.merchantID = value;

        this.cartProvider.getCartDetailCounter(cartID, this.merchantID).subscribe(
          res => {
            let resultset: any = res;
            this.cartcounter = resultset.data;
            
            console.log("counter 123", this.cartcounter[0].detail_counter);
            this.counterFront = this.cartcounter[0].detail_counter
    
            if (this.cartcounter[0].detail_counter == 0)
            this.counterFront + 1
            
          }, error => {
            console.log(error);
        });

    });
}


  segmentChanged(){
    console.log("test");
  }

  openCart(){
    this.appCtrl.getRootNav().push('MenuOrderingPage');
  }

  transactionAppointment(txn_type:string){

    this.storage.get('merchantID').then((value)=>{
      this.merchantID = value;
    
        let data = {
          "amount": "0",
          "merchant_id": this.merchantID,
          "user_id": 2,
          "txn_type":txn_type
        };

        this.transactionProvider.addTransactionAppointment(data).subscribe(
          res => {
            let resultset: any = res;  
            this.transactions = resultset.status;
        });

    });

  }

  cancelReservation(applicationID:any, merchantID: any){
    this.cancelConfirm(applicationID, merchantID);
  }

  cancelConfirm(applicationID:any, merchantID: any) {
    let alert = this.alertCtrl.create({
      title: 'Confirm cancellation',
      message: 'Do you want to cancel this reservation?',
      buttons: [
        {
          text: 'YES',
          role: 'cancel',
          handler: () => {
            console.log('Yes clicked');

            let data = {
              "appointment_id":applicationID,
              "merchant_id":merchantID
            };

                  this.reservationProvider.cancelAppointment(data).subscribe(
                    res => {
                      let resultset: any = res;
                      this.reservationStatus = resultset.data;
                      
                      if(this.reservationStatus){
                        this.transactionAppointment("cancelled");
                        console.log("Successfull updated");
                        this.loadAppointmentList();
                      }else{
                        console.log("ERR");
                      }
              
                    }), error => {
                      console.log(error);
                    }
          }
        },
        {
          text: 'NO',
          handler: () => {
            console.log('No clicked');
          }
        }
      ]
    });
    alert.present();
  }


  goBack() {
    this.navCtrl.pop();
  }

  displayWalletVoucher(){
   // var id = localStorage.getItem('user_id');


    this.storage.get('userID').then((value)=>{
      this.userID = value;

      this.storage.get('merchantID').then((value)=>{
        this.merchantID = value;

        let data = {
          "user_id":2,
          "merchant_id":this.merchantID
        };
    
        this.walletProvider.getMerchantWalletVoucher(data).subscribe(
          res => {
            let resultset: any = res;
            this.wallet_voucher = resultset.data;
            console.log("WALLET",this.wallet_voucher);
          }), error => {
            console.log(error);
          }  
    });
   });

  }

  loadAppointmentList(){

    this.storage.get('merchantID').then((value)=>{
      this.merchantID = value;

    let data = {
      "merchant_id":this.merchantID
    };

    this.reservationProvider.getMerchantAppointmentList(data).subscribe(
      res => {
        let resultset: any = res;
        this.reservations = resultset.data;
        console.log("RES",this.reservations);
      }), error => {
        console.log(error);
      }

    });

  }

  useVoucher(vid: number, mid: number){
    
    this.storage.get('userID').then((value) => {
    this.userID = value;

    this.walletProvider.getWalletUse(this.userID, vid).subscribe(
      res => {
        let resultset: any = res;
        this.merchant_voucher = resultset.data;
        console.log("WALLET2",this.merchant_voucher);

        this.walletProvider.sendNotif(mid).subscribe(
          res => {
            let resultset: any = res;
            this.fcm_num = resultset.data;
            console.log("fcm", this.fcm_num[0].fcm_token);
            console.log("merchant id", mid)

            let body = {
              "notification":{
                "title":"IPAYU Philippines",
                "body":"A customer just redeemed their voucher. Please check it out.",
                "sound":"default",
                "click_action":"FCM_PLUGIN_ACTIVITY",
                "icon":"icon"
              },
              "to": this.fcm_num[0].fcm_token,
              "priority":"high",
              "restricted_package_name":""
            }

            let options = new HttpHeaders().set('Content-Type','application/json');
            this.http.post("https://fcm.googleapis.com/fcm/send",body,{
              headers: options.set('Authorization', 'key=AAAAPi0wxFY:APA91bHZhmnNW0Axfc5JaEr72jjDu80IFtrOozXao-uUp_qHKs5OnMxo_BIE2z84lL0MShow1aC8XYA-w6XRytTEAWHk-KOFNI-VE3JNmBDktA4IsRQ82VEERMGz6FpR7dfPEB4-Uqga'),
            }).subscribe();
            
            const alert = this.alertCtrl.create({
              title: "Congratulations!",
              subTitle: "Voucher is ready to use.",
              buttons: [
                {
                  text: 'OK',
                  handler: () => {
                    this.displayWalletVoucher()
                  }
                }
              ],
              cssClass: 'alertCustomCss'
            });
            alert.present()
          
          }), error => {
            console.log(error);
          }
          }), error => {
            console.log(error);
          }

        });

  }

  loadOrderList(){
    var id = localStorage.getItem('user_id');

    this.storage.get('userID').then((value) => {
      this.userID = value;

      this.storage.get('merchantID').then((value) => {
        this.merchantID = value;
     
        let data = {
          "ipayu_id":this.userID,
          "merchant_id":this.merchantID
        };
    
        this.cartProvider.orderMerchantWalletList(data).subscribe(
          res => {
            let resultset: any = res;
            this.orders = resultset.data;
            console.log("orders",this.orders);
          }), error => {
            console.log(error);
          }
     
      });

    });

  }

  loadShopInfo(){

    this.storage.get('merchantID').then((val)=>{
      this.merchantID = val;

      this.merchantProvider.merchantInfo(this.merchantID).subscribe(
        res => {
          let resultset: any = res;
          this.merchants = resultset.data;
  
          console.log("merchants", this.merchants);
        }, error => {
          console.log(error);
      });
    
    });
  }

  
  toWallet() {
    this.appCtrl.getRootNav().push("WalletFabPage");
  }
  
  toProduct() {
    this.appCtrl.getRootNav().push("SpecialOfferPage");
  }

  toPromo() {
    this.appCtrl.getRootNav().push("PromoPage");
  }

  toResto() {
    this.appCtrl.getRootNav().push("VoucherFabPage");
  }
  
  navigateTo(page: string) {
    this.appCtrl.getRootNav().push(page);
  }

  openSideMenu() {
    this.menuCtrl.enable(true)
     this.menuCtrl.toggle();    
 }

}
