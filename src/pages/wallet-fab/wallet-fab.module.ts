import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WalletFabPage } from './wallet-fab';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    WalletFabPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(WalletFabPage),
  ],
})
export class WalletFabPageModule {}
