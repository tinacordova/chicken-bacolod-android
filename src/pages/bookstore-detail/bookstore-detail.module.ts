import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BookstoreDetailPage } from './bookstore-detail';

@NgModule({
  declarations: [
    BookstoreDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(BookstoreDetailPage),
  ],
})
export class BookstoreDetailPageModule {}
