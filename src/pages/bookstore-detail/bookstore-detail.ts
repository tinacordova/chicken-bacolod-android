import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App, ModalController, ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { ProductProvider } from '../../providers/product/product';
import { CartProvider } from '../../providers/cart/cart';

/**
 * Generated class for the BookstoreDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-bookstore-detail',
  templateUrl: 'bookstore-detail.html',
})
export class BookstoreDetailPage {

  prodParamId: number;
  products: any = [];
  productSubImages: any = [];
  prod_id: any;
  cart: any [];
  merchantParamId: any;
  orders: any[];
  cartcounter: any [];
  counterFront: number;
  updateorder: any [];
  merc_id: any;
  table_num: any;
  userID: number;
  cartID: number;
  merchantID: number;
  main: number;
  cartQty: number;

  count_order: any[];

  public counter : number = 1;
  // counter2: number;

  localStorageUserId: string;
  localStorageFirstName: string;
  localStorageLastName: string;

  constructor(
    public navCtrl: NavController,
    public appCtrl: App, 
    public navParams: NavParams,
    public productProvider: ProductProvider,
    public cartProvider: CartProvider,
    public modalCtrl: ModalController,
    private storage: Storage,
    public toastCtrl: ToastController) {

      this.prodParamId = this.navParams.get('prod_id');
      this.merc_id = localStorage.getItem('merch_id');


      this.storage.get('itemQuantity').then((val) => {
      
        if(val != 0){
          this.cartQty = val;
        }else{
          this.cartQty = 0;
        }
      });  

  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad RestoFoodDetailPage');

    this.loadProductDetail();
    this.loadProductSubImages();
    this.loadCartDetailCounter()

    this.localStorageUserId = localStorage.getItem('user_id');
    this.localStorageFirstName = localStorage.getItem('user_firstname');
    this.localStorageLastName = localStorage.getItem('user_lastname');

    // console.log('count', this.counter2)
  }

  loadProductDetail(){
    this.productProvider.getProductBookDetailV8(this.prodParamId).subscribe(
      res => {
        let resultset: any = res;
        this.products = resultset.data;
        console.log("product detail", this.products);

      }, error => {
        console.log(error);
    });
  }

  loadProductSubImages(){
    this.productProvider.getProductSubImages(this.prodParamId).subscribe(
      res => {
        let resultset: any = res;
        this.productSubImages = resultset.data;
        console.log("product sub images", this.productSubImages);
      }, error => {
        console.log(error);
    });
  }

  goBack() {
    // this.navCtrl.push('RestaurantPage');
    this.navCtrl.pop();
  }

  getMenu(id: number, amt: number, mid: number){
    // localStorage.setItem('items', this.prod_id);
    // this.table_num = localStorage.getItem('table_number')

    this.storage.get('merchantID').then((value) => {
      this.merchantID = value;
    
      this.cartProvider.cartCounter(this.merchantID).subscribe(
        res => {
          let resultset: any = res;
          this.count_order = resultset.data;
          console.log("first", this.count_order);
  
          this.storage.get('userID').then((val) => {
            this.userID = val;
  
            this.storage.get('merchantID').then((value) => {
              this.merchantID = value;
  
              this.cartProvider.postCart(this.userID, 1, 0, id, this.counter, amt, this.merchantID).subscribe(
                res => {
                  let resultset: any = res;
                  this.orders = resultset.data;
                  console.log("orders", this.orders);
      
                }, error => {
                  console.log(error);
      
                  const toast = this.toastCtrl.create({
                    message: 'Order saved in the cart.',
                    duration: 2000
                  });
                  toast.present();
                  this.navCtrl.setRoot(this.navCtrl.getActive().component);
              });
  
              this.cartProvider.cartMain(this.userID, this.merchantID, 0).subscribe(
                res => {
                  let resultset: any = res;
                  this.main = resultset.data;
                  console.log("main", this.main);
                  console.log("cartid", this.main[0].cart_id);
                  console.log("mainid", this.main[0].ipayu_id);
          
                  this.storage.set('cartID', this.main[0].cart_id);
          
                }, error => {
                  console.log(error);
              });
              
            });
  
          });
  
        }, error => {
          console.log(error);
      });

    });
    
    this.navCtrl.push('BookstorePage'),{
        merchant_id: mid
    }

  }

  add(){
    this.counter += 1;
  }

  remove(){
    if(this.counter > 1){
      this.counter -= 1;
    }
  }
  
  toHome(){
    this.navCtrl.push('BookstorePage')
  }

  toCart(){
    this.navCtrl.push('MenuOrderingPage')
  }

  loadCartDetailCounter(){
    this.storage.get('cartID').then((value)=>{
      this.cartID = value;

      this.storage.get('merchantID').then((value)=>{
        this.merchantID = value;

        this.cartProvider.getCartDetailCounter(this.cartID, this.merchantID).subscribe(
          res => {
            let resultset: any = res;
            this.cartcounter = resultset.data;
            
            console.log("counter", this.cartcounter[0].detail_counter);
            this.counterFront = this.cartcounter[0].detail_counter
    
            if (this.cartcounter[0].detail_counter == 0)
            this.counterFront + 1
            
          }, error => {
            console.log(error);
        });
  
      });

    });
  }

  // openCart(){
  //   this.appCtrl.getRootNav().push('MenuOrderingPage');
  // }

}
