import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { PromoSearchProvider } from '../../providers/promo-search/promo-search';
import { MerchantProvider } from '../../providers/merchant/merchant';
import { PromoProvider } from '../../providers/promo/promo';
import { CartProvider } from '../../providers/cart/cart';

import { Observable } from "rxjs";
import { debounceTime, distinctUntilChanged } from "rxjs/operators";

@IonicPage()
@Component({
  selector: 'page-promo',
  templateUrl: 'promo.html',
})
export class PromoPage {

  promos_search: any = [];
  promos: any = [];
  promosLoad: any = [];
  promoCategories: any = [];
  merchants: any = [];

  searchItems: string;
  newLocation: any;
  x: any;
  locationPass: any;

  searchPromo: boolean = false;
  searchFilter: boolean = true;
  promoLoad: boolean = true;
  closeFilter: boolean = true;
  newFilter: boolean = false;
  userID: number;
  merchantID: number;
  tableNumber: number;
  main: any;
  cart_id: number;
  cartcounter: any [];
  counterFront: number;

  /* Sir Genil */
  vouchers: any = [];
  voucherCounters: any = [];
  search_results: any = [];
  voucherCategories: any = [];
  show: boolean = false;
  voucherBoolean: boolean = false;

  searchQuery: string = '';
  items: string[];
  /* Ends Here */

  noResults: boolean = false;
  promoCounters: any = [];
  newCate: any;

  resCount: number;
  clearStatus: Boolean = false;

  searchChangeObserver;

  constructor(
    public navCtrl: NavController, 
    public appCtrl: App, 
    public navParams: NavParams,
    private storage: Storage,
    public cartProvider: CartProvider,
    public promoSearchProvider: PromoSearchProvider,
    public promoProvider: PromoProvider,
    public merchantProvider: MerchantProvider,
    public loadingCtrl: LoadingController) {

      this.newLocation = localStorage.getItem('key');
      this.locationPass = this.navParams.get('location');
      // this.newCate = this.navParams.get('categoryPush');
      // this.newCate = localStorage.getItem('cate');
      this.newCate = localStorage.getItem('categorize')
      this.initializeItems();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PromoPage');
    this.getCartID();
    this.loadShopInfo();

    if (this.locationPass == null){
      this.loadPromo();
      // this.newPromoCategory();
      // this.loadPromoList();
      console.log("if", this.newCate);
    }else{
      this.loadPromoCategory()
      console.log("else", this.newCate);
    }
  }

  initializeItems() {
    this.items = [
      'Amsterdam',
      'Bogota'
    ];
  }

  getCartID(){

    this.storage.get('userID').then((value)=>{
      this.userID = value;

      console.log('user_id', this.userID);
  
      this.storage.get('merchantID').then((value)=>{
        this.merchantID = value;

        console.log('merchant_id', this.merchantID);

        this.storage.get('tableNumber').then((value)=>{
          this.tableNumber = value;

          this.cartProvider.cartMain(this.userID, this.merchantID, 0).subscribe(
            res => {
              let resultset: any = res;
              this.main = resultset.data;
              console.log("main", this.main);
              console.log("cartid", this.main[0].cart_id);
              console.log("mainid", this.main[0].ipayu_id);

              this.loadCartDetailCounter(this.main[0].cart_id);
      
              this.cart_id = this.main[0].cart_id      
              this.storage.set('cartID', this.cart_id);
      
            }, error => {
              console.log(error);
          });
        
        });
      
      });
    
    });
  }

  loadCartDetailCounter(cartID){

    this.storage.get('merchantID').then((value)=>{
      this.merchantID = value;

        this.cartProvider.getCartDetailCounter(cartID, this.merchantID).subscribe(
          res => {
            let resultset: any = res;
            this.cartcounter = resultset.data;
            
            console.log("counter 123", this.cartcounter[0].detail_counter);
            this.counterFront = this.cartcounter[0].detail_counter
    
            if (this.cartcounter[0].detail_counter == 0)
            this.counterFront + 1
            
          }, error => {
            console.log(error);
        });

    });
}

loadShopInfo(){

  this.storage.get('merchantID').then((val)=>{
    this.merchantID = val;

    this.merchantProvider.merchantInfo(this.merchantID).subscribe(
      res => {
        let resultset: any = res;
        this.merchants = resultset.data;

        console.log("merchants", this.merchants);
      }, error => {
        console.log(error);
    });
  
  });
}

  loadPromo(){
    this.promoLoad = true;
    this.searchFilter = false;
    this.searchPromo = false;

    console.log("Promo Loads!")

    this.storage.get('merchantID').then((value) => {
      this.merchantID = value;

      let data = {
        "merchant_id":this.merchantID
      };
  
      this.promoProvider.getMerchantPromoCategoryItems(data).subscribe(
        res => {
          let resultset: any = res;
          this.promoCategories = resultset.data;
          console.log("promo list category", this.promoCategories);
        }, error => {
          console.log(error);
      });    
    
    });

  }

  goBack() {
    this.navCtrl.pop();
  }

  goTo(){
    this.appCtrl.getRootNav().push('FilterPage');
  }

  nextTo(){
    this.navCtrl.pop();
    // this.appCtrl.getRootNav().push('FilterPage');
  } 

  //-------- NEW PROMO SEARCH
  // getItems(ev: any){
  //     // this.promoList = false;
  //     this.promoLoad = false;
  //     this.searchFilter = false;
  //     this.searchPromo = true;
  
  //     // this.searchItems = event.target.value
  //     console.log(this.searchItems)
  
  //     this.promoSearchProvider.getPromoSearchCat(this.searchItems).subscribe(
  //       res => {
  //         let resultset: any = res;
  //         this.promos = resultset.data;
  //         console.log("promo search list", this.promos); 
  //         // this.searchFilter = true;
  //       }, error => {
  //         console.log(error);
  //     });
  //   }

  getItems(ev: any) {
    let sample = 0;
    //Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the searchbar
    const val = ev.target.value;

    if(val.length >= 3){

      this.search_results = [];

      this.promoLoad = false;
      this.searchFilter = false;
      this.searchPromo = true;

      console.log("text search", val)
      //Fetched the result from api
      this.promoSearchProvider.getPromoSearchCat(val).subscribe(
        res => {
          let resultset: any = res;
          console.log(resultset)
          let resultset2: any = res;
          // this.search_results = resultset.data;

            let loader = this.loadingCtrl.create({
              content: 'Loading Page ... ',
            });
        
            loader.present();
        
            setTimeout(() => {
        
              this.promoCounters = resultset2.promo_counter;

              if(this.promoCounters.length > 0){
                this.voucherBoolean = true;
                console.log("1st if")
              }
              this.search_results = resultset.data

            }, 1000);
        
            setTimeout(() => {
              loader.dismiss();
            }, 1000);
      
          console.log('promo counter', resultset2.promo_counter);
          console.log("promo search result", this.voucherCounters);
          console.log("promo search result2", this.search_results);

        }, error => {
          console.log(error);
      });

      console.log('Thank you');
    }else{
      this.show = false;
      console.log('Bleeh');
    }


    console.log('value', val);
    console.log('value lenth', val.length);

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  //--------- New PROMO FILTER

  loadPromoCategory(){
    this.promoLoad = false;
    this.searchFilter = true;
    this.searchPromo = false;
    this.closeFilter = false;
    this.newFilter = true;

    console.log("trade category id", localStorage.getItem('categorize'))

    this.promoSearchProvider.getPromoFilterCat(localStorage.getItem('key'), localStorage.getItem('categorize')).subscribe(
      res => {
        let resultset: any = res;

        //display all the category with product inside
        this.promos_search = resultset.data;

        this.x = localStorage.getItem('key');

        console.log('prom', this.promos_search);

      }, error => {
        console.log(error);
    });
  }

  openPromoDetail(p_id: number){
    console.log('open promo detail')

    this.navCtrl.push('PromoModalPage',
    {promo_id: p_id})
  }

  openCart(){
    this.appCtrl.getRootNav().push('MenuOrderingPage');
  }

  toWallet() {
    this.appCtrl.getRootNav().push("WalletFabPage");
  }
  
  toProduct() {
    this.appCtrl.getRootNav().push("SpecialOfferPage");
  }

  toPromo() {
    this.appCtrl.getRootNav().push("PromoPage");
  }

  toResto() {
    this.appCtrl.getRootNav().push("VoucherFabPage");
  }

  navScreen(page:string) {
    this.appCtrl.getRootNav().push(page);
  }

  navigateTo(page: string) {
    this.appCtrl.getRootNav().push(page);
  }

  onSearchChange(searchValue: string) {
    if (!this.searchChangeObserver) {
      Observable.create(observer => {
        this.searchChangeObserver = observer;
      })
        .pipe(debounceTime(1500)) // wait 300ms after the last event before emitting last event
        .pipe(distinctUntilChanged()) // only emit if value is different from previous value
        .subscribe(res => {
          this.resCount = res.length;

          if (
            this.resCount > 0 &&
            res.length > 0 &&
            this.clearStatus === false
          ) {

            this.storage.get('merchantID').then((value)=>{
              this.merchantID = value;

              let data = {
                "merchant_id":this.merchantID,
                "keyword":res
              };
  
                this.promoProvider.getMerchantPromoSearch(data).subscribe(
                  res => {
                    let resultset: any = res;
                    this.promoCategories = [];
            
                    this.promoCategories = resultset.data;
                    console.log("Promo merchant Search", this.promoCategories);
                  },
                  error => {
                    console.log(error);
                  }
                );
            });
          }

          this.clearStatus = false;
        });
    }

    this.searchChangeObserver.next(searchValue);
  }

  onClear(event: any) {
    //This is a marker that the clear search was clicked (the x button)
    this.clearStatus = true;
  }

  onChange(event: any) {
    //Get the string length of the search bar and assign to resCount
    this.resCount = event.target.value.length;
  }


}