import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { 
  IonicPage, 
  NavController, 
  NavParams, 
  App, 
  LoadingController, 
  ViewController, 
  ModalController, 
  ToastController, 
  Refresher } from 'ionic-angular';
import { Storage } from '@ionic/storage';
// import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { Nav, Platform, MenuController, Content } from 'ionic-angular';   

import { MerchantProvider } from '../../providers/merchant/merchant';
import { PromoProvider } from '../../providers/promo/promo';
import { VoucherProvider } from '../../providers/voucher/voucher';
import { ProductProvider } from '../../providers/product/product';
import { CartProvider } from '../../providers/cart/cart';

import {Http} from '@angular/http';
import 'rxjs/add/operator/map';

import { MenuCategoryPage } from '../../pages/menu-category/menu-category';

interface StorageItem {
  merchantID: number
 }
 

@IonicPage()
@Component({
  selector: 'page-restaurant',
  templateUrl: 'restaurant.html',
})
export class RestaurantPage {

  @ViewChild(Nav) nav: Nav;
  @ViewChild(Content) content: Content;

  pages: Array<{title: string, component: any}>;

  pet: string = "kittens";
  //category: string = "appetizers";
  isAndroid: boolean = false;

  categories: any [];
  category_ids: any [];
  category: string;

  merchantParamId: any;
  merchants: any [];
  merchant_ads: any [];
  promos: any [];
  vouchers: any [];
  prodCategories: any [];
  prodCatItems: any [];
  catProds: any [];
  catProdResult: any [];
  specialOffers: any [];
  cartcounter: any [];
  counterFront: number;
  open: boolean=false;
  menuClose: boolean=false;
  orders: any[];
  count_order: any [];
  products: any [];
  updateorder: any [];
  table: any;
  firstName: string;
  lastName: string;
  cart_detail: any[];
  usersData: any[];
  cartData: any[];
  merchantID: number;
  storageItem: number;
  sampleTable: number;
  cartID: number;
  userID: number;
  main: any;

  my_id: number;
  my_amt: number;
  table_num: any;
  cart_id: number;
  tableNumber: number;
  tableDetails: any;
  // menuOpen: boolean=true;

  slideTwoForm: FormGroup;
  openopen: boolean = false;
  closeToggle: boolean = true;
  openToggle: boolean = false;

  public counter : number = 1;
  public counter2 : number = 1;
  newcount: number

  localStorageUserId: string;
  localStorageFirstName: string;
  localStorageLastName: string;

  sampleName: string;

  new: any;

  constructor(
    public menuCtrl: MenuController, 
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public appCtrl: App,
    public merchantProvider: MerchantProvider,
    public promoProvider: PromoProvider,
    public voucherProvider: VoucherProvider,
    private loadingCtrl: LoadingController,
    public productProvider: ProductProvider,
    public formBuilder: FormBuilder,
    public viewCtrl: ViewController,
    public cartProvider: CartProvider,
    public http: Http,
    public toastCtrl: ToastController, 
    public modalCtrl: ModalController,
    private storage: Storage,
    // private sqlite: SQLite,
    public nav2: Nav) {
      this.firstName = localStorage.getItem('user_firstname');
      this.lastName = localStorage.getItem('user_lastname');
      
      this.merchantParamId = this.navParams.get('merchant_id');
  
            this.slideTwoForm = formBuilder.group({
      
        privacy: ['']
 
    });

    var table = 0;

    this.storage.get('sample_table').then((val) => {
      table = val;

      this.sampleTable = val;
      console.log('Sample Table', val);
      this.storageItem = val;
      
      console.log('table', table);

    });

  }


  ionViewDidEnter(){
    this.getTableNumber();
    this.getCartID();
    
  }

  ionViewDidLoad() {

    //this.resetLocalStorage();
    //this.loadCartDetailCounter();
    this.loadShopInfo();
    this.loadMerchantAds();
    this.loadProdSpecialOffer();
    this.loadMerchantPromo();
    this.loadMerchantVoucher();
    this.loadProductMerchantCategory();
    

    this.testing();
  
    this.retrieveCartData();
    console.log('hi', this.sample());

    console.log('hay', this.getName());


    console.log('hello name:', this.sampleName);

    this.localStorageUserId = localStorage.getItem('user_id');
    this.localStorageFirstName = localStorage.getItem('user_firstname');
    this.localStorageLastName = localStorage.getItem('user_lastname');
    // this.doRefresh(Refresher);

    this.table = localStorage.getItem('table')
    console.log("table", this.table)

    console.log("merchant id", localStorage.getItem('new_merchant_id'))

    //this.new = localStorage.getItem('cartid');
  }

  public getSeasonList() {
    console.log('Season sampleName', this.sampleName);
  }


  getTableNumber(){

    this.storage.get('userID').then((value)=>{
      this.userID = value;

      this.storage.get('merchantID').then((value)=>{
        this.merchantID = value;

        this.cartProvider.getTableNumber(this.userID, this.merchantID).subscribe(
          res => {
            let resultset: any = res;
            this.tableDetails = resultset.data;
            this.tableNumber = parseInt(this.tableDetails[0].table_number);

            console.log('table_details', this.tableDetails);
            console.log('table_number', this.tableNumber);


          this.storage.set('tableNumber', this.tableNumber);

          }, error => {
            console.log(error);
        });

      });

    });

  }

  getCartID(){

    console.log('get cart id');

    this.storage.get('userID').then((value)=>{
      this.userID = value;

      console.log('user_id', this.userID);
  
      this.storage.get('merchantID').then((value)=>{
        this.merchantID = value;

        this.storage.get('tableNumber').then((value)=>{
          this.tableNumber = value;

          this.cartProvider.cartMain(this.userID, this.merchantID, this.tableNumber).subscribe(
            res => {
              let resultset: any = res;
              this.main = resultset.data;
              console.log("main", this.main);
              console.log("cartid", this.main[0].cart_id);
              console.log("mainid", this.main[0].ipayu_id);

              this.loadCartDetailCounter(this.main[0].cart_id);
      
              this.cart_id = this.main[0].cart_id
              //this.ipayu_id = this.main[0].ipayu_id
      
              this.storage.set('cartID', this.cart_id);
      
            }, error => {
              console.log(error);
          });
        
        });
      
      });
    
    });
  }


  testing(){
    return this.storage.get('cartID').then((value)=>{
      this.cartID = value;
      return this.sendReq(this.cartID)
    });
}

resetLocalStorage(){
  this.storage.clear();
}

sendReq(token){
  console.log('token', token);
}

  getName(){
    this.storage.get('name').then((val) => {
      this.sampleName = val;
      console.log('Your name is', val);
      return val;
    });
  }


  sample(){
    this.storage.get('name');
  }

  retrieveCartData() {
    this.storage.get("cartData").then( cart => {
      this.cartData = JSON.parse(cart);
      console.log('get cartData', this.cartData);
    })
  }

  segmentChanged(){
    console.log("test");
  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    this.loadShopInfo();
    this.loadMerchantAds();
    this.loadProdSpecialOffer();
    this.loadMerchantPromo();
    this.loadMerchantVoucher();
    this.loadProductMerchantCategory();

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

  loadShopInfo(){
    var mer_id =  localStorage.getItem('new_merchant_id')
    this.merchantProvider.merchantInfo(parseInt(mer_id)).subscribe(
      res => {
        let resultset: any = res;
        this.merchants = resultset.data;

        console.log("merchants", this.merchants);
      }, error => {
        console.log(error);
    });
  }

  loadMerchantPromo(){
    let bg = '';
    var mer_id =  localStorage.getItem('new_merchant_id')
    this.promoProvider.getMerchantPromo(parseInt(mer_id)).subscribe(
      res => {
        let resultset: any = res;
        this.promos = resultset.data;

        console.log("merchant promos", this.promos);     
      }, error => {
        console.log(error);
    });
  }

  loadMerchantAds(){
    let mer_id =  localStorage.getItem('new_merchant_id')
    
    let data = {
      "merchant_id":mer_id
    };

    this.merchantProvider.getMerchantAds(data).subscribe(
      res => {
        let resultset: any = res;
        this.merchant_ads = resultset.data;

        console.log("merchant ads", this.merchant_ads);     
      }, error => {
        console.log(error);
    });
  }

  loadProdSpecialOffer(){
    let bg = '';
    var mer_id =  localStorage.getItem('new_merchant_id')
    this.productProvider.getSpecialOffer(parseInt(mer_id)).subscribe(
      res => {
        let resultset: any = res;
        this.specialOffers = resultset.data;

        console.log("Special Offer", this.specialOffers);     
      }, error => {
        console.log(error);
    });
  }

  loadMerchantVoucher(){
    let bg = '';
    var mer_id =  localStorage.getItem('new_merchant_id')
    this.voucherProvider.getMerchantVoucher(parseInt(mer_id)).subscribe(
      res => {
        let resultset: any = res;
        this.vouchers = resultset.data;
        
        console.log("merchant vouchers", this.vouchers);     
      }, error => {
        console.log(error);
    });
  }

  loadProductMerchantCategory(){
    var mer_id =  localStorage.getItem('new_merchant_id')
    this.productProvider.getProductMerchantCategory(parseInt(mer_id)).subscribe(
      res => {
        let resultset: any = res;
        //display all the category with product inside
        this.prodCategories = resultset.data;

      }, error => {
        console.log(error);
    });
  }

  navigateTo(page:string){
    this.appCtrl.getRootNav().push(page);
  }

  openProductDetail(prod_id, count){
    this.merchantParamId
    console.log('count', count)
    localStorage.setItem('merch_id', this.merchantParamId)

    let loader = this.loadingCtrl.create({
      content: 'Loading Page ... ',
    });

    loader.present();

    setTimeout(() => {

      this.navCtrl.push('RestoFoodDetailPage', 
      {prod_id: prod_id,
      count: count});

    }, 1000);

    setTimeout(() => {
      loader.dismiss();
    }, 3000);

  }

  openPromoDetail(promo_id){

    let loader = this.loadingCtrl.create({
      content: 'Loading Page ... ',
    });

    loader.present();

    setTimeout(() => {
      this.navCtrl.push('PromoModalPage', 
      {promo_id: promo_id
      });

    }, 1000);

    setTimeout(() => {
      loader.dismiss();
    }, 3000);
    
  }

  openVoucherDetail(voucher_id){


    let loader = this.loadingCtrl.create({
      content: 'Loading Page ... ',
    });

    loader.present();

    setTimeout(() => {

      this.navCtrl.push('VoucherModalPage', 
      {voucher_id: voucher_id
      });

    }, 1000);

    setTimeout(() => {
      loader.dismiss();
    }, 3000);
    
  }

  goBack() {
    this.navCtrl.pop();
  }

  goNext(){
    this.appCtrl.getRootNav().push('RestoVoucDetailPage');
    console.log("sana");
  }

  goPromo(){
    this.appCtrl.getRootNav().push('RestoPromoDetailPage');
    console.log("sana");
  }

  openMenuDetail(){
    this.appCtrl.getRootNav().push('RestoFoodDetailPage');
  }

  openSpecialDetail(prod_id, count){
    console.log('count', count)
    let loader = this.loadingCtrl.create({
      content: 'Loading Page ... ',
    });

    loader.present();

    setTimeout(() => {

      this.navCtrl.push('RestoSpecialDetailPage', 
      {prod_id: prod_id,
        count: count});

    }, 1000);

    setTimeout(() => {
      loader.dismiss();
    }, 3000);

  }

  openPage() {
    this.appCtrl.getRootNav().push('ReservationPage');
    // this.viewCtrl.dismiss().then(() => this.appCtrl.getRootNav().setRoot('RestaurantPage'))
    this.menuCtrl.open();
    }

  openMenu(page: string, cit: any) {
    this.navCtrl.push('BlankPage').then(() => this.appCtrl.getRootNav().setRoot(page, {
      cit: cit
    }))

    localStorage.setItem('cat_id', cit)
  }

  open_Toggle(){
    this.openopen = true;
    this.closeToggle = false;
    this.openToggle = true;
  }

  close_Toggle(){
    this.openopen = false;
    this.closeToggle = true;
    this.openToggle = false;
  }

  openFoodDetail(){
    this.appCtrl.getRootNav().push('RestoFoodDetailPage');
  }
  
  openToWallet(){
    this.appCtrl.getRootNav().push('WalletFabPage');
  }
    
  openCart(){
    this.appCtrl.getRootNav().push('MenuOrderingPage');
  }

  add(pid: number, i){
    this.newcount = i
    console.log('add', pid, this.newcount)
    this.counter += 1;
    
  }

  remove(pid: number, i){
    console.log('remove', pid, i)
    if(this.counter > 1){
      this.counter -= 1;
    }
  }

  addtoOrder(id: number, amt: number, mid: number, elementId: string){
    var mer_id =  localStorage.getItem('new_merchant_id')
    this.my_amt = amt;
    this.my_id = id;
    this.table_num = localStorage.getItem('table_number');

    this.cartProvider.cartCounter(parseInt(mer_id)).subscribe(
      res => {
        let resultset: any = res;
        this.count_order = resultset.data;
        console.log("first", this.count_order);

        if (this.count_order[0].total_count == 0){
          console.log("insert")

          let modalTable = this.modalCtrl.create('TablePage', {
            pid: id, amt: amt, mid: mer_id, count: this.counter
          })
          modalTable.present()

        }else{
          var mer_id =  localStorage.getItem('new_merchant_id')
          console.log("insert/update")

          this.cartProvider.postCart(parseInt(this.localStorageUserId), 1, parseInt(this.table_num), id, this.counter, amt, parseInt(mer_id)).subscribe(
            res => {
              let resultset: any = res;
              this.orders = resultset.data;
              console.log("orders", this.orders);
              
            }, error => {
              console.log(error);
          });

          this.cartProvider.cartMain(parseInt(this.localStorageUserId), parseInt(mer_id), this.table_num).subscribe(
            res => {
              let resultset: any = res;
              this.main = resultset.data;
              console.log("main", this.main);
              console.log("cartid", this.main[0].cart_id);
              console.log("mainid", this.main[0].ipayu_id);
      
              this.storage.set('cartID', this.main[0].cart_id);
      
            }, error => {
              console.log(error);
          });

          const toast = this.toastCtrl.create({
            message: 'Order saved in the cart.',
            duration: 2000
          });

          toast.present();

          this.navCtrl.setRoot(this.navCtrl.getActive().component);

          let y = document.getElementById(elementId).offsetTop;
          this.content.scrollTo(0, y);
        }

      }, error => {
        console.log(error);
    });
  }

  loadCartDetailCounter(cartID){

    console.log('loadDetailCounter');


    // this.storage.get('cartID').then((val)=>{
    //   this.cartID = val;
    //   console.log('bam', val);
    // });


    // this.storage.get('cartID').then((val)=>{
    //   this.cartID = val;

    //   console.log('cartID', val);

    this.storage.get('merchantID').then((value)=>{
      this.merchantID = value;

      console.log('merchantID', this.merchantID);


        this.cartProvider.getCartDetailCounter(cartID, this.merchantID).subscribe(
          res => {
            let resultset: any = res;
            this.cartcounter = resultset.data;
            
            console.log("counter", this.cartcounter[0].detail_counter);
            this.counterFront = this.cartcounter[0].detail_counter
    
            if (this.cartcounter[0].detail_counter == 0)
            this.counterFront + 1
            
          }, error => {
            console.log(error);
        });

    });

  //});




  }

  goToReservation(){
    this.navCtrl.push('ReservationPage')
  }

  // scrollTo(elementId: string) {
  //   let y = document.getElementById(elementId).offsetTop;
  //   this.content.scrollTo(0, y);
  // }
}
