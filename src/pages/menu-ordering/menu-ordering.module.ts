import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MenuOrderingPage } from './menu-ordering';

@NgModule({
  declarations: [
    MenuOrderingPage,
  ],
  imports: [
    IonicPageModule.forChild(MenuOrderingPage),
  ],
})
export class MenuOrderingPageModule {}
