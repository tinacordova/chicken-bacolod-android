import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController, ToastController } from 'ionic-angular';

import { ProductProvider } from '../../providers/product/product';
import { CartProvider } from '../../providers/cart/cart';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-menu-ordering',
  templateUrl: 'menu-ordering.html',
})
export class MenuOrderingPage {

  prods: any [];
  products: any [];
  carts: any [];
  orders: {};
  item2: number;
  payment: any [];
  itms: [{}];
  cash: number;
  allCarts: any [];
  remove_item: any [];
  update_qty: any [];
  cartID2: number;
  userID: number;
  merchantID: number;
  cartID: number;
  cartQty: number;

  f_vat: any;
  f_grand: any;

  localStorageUserId: string;
  localStorageFirstName: string;
  localStorageLastName: string;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private productProvider: ProductProvider,
              private cartProvider: CartProvider,
              private modalCtrl: ModalController,
              private alertCtrl: AlertController,
              private storage: Storage,
              private toastCtrl: ToastController) {

      this.item2 = this.navParams.get('id')
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MenuOrderingPage');
    console.log('items', localStorage.getItem('items'))

  
    this.storage.get('cartID').then((value)=>{
      this.cartID = value;

      this.storage.get('merchantID').then((value)=>{
        this.merchantID = value;

        this.loadCartMain(this.cartID, this.merchantID);

        });
    
    });

    this.loadOrderList();
    
    this.localStorageUserId = localStorage.getItem('user_id');
    this.localStorageFirstName = localStorage.getItem('user_firstname');
    this.localStorageLastName = localStorage.getItem('user_lastname');
  }

  goBack(){
    //this.navCtrl.pop();
    this.navCtrl.push('BookstorePage');
  }

  loadOrderList(){
    // var mer_id =  localStorage.getItem('new_merchant_id')
    // var id = localStorage.getItem('cartid');
    // console.log("cart id ", id, "mer id ", mer_id)


    //Items Array Value
    //0-cartID
    //1-ipayuID
    //2-merchantID
    //3-userID

    
    // let items = [];
    // this.storage.forEach((v,k,i) => {
    //     items.push(v);
    // }).then(() => {
    //     items;
    //     let ipayuID = items['0'];
    //     let merchantID = items['2'];

        //console.log('merchant_id', items['0']);

        this.storage.get('cartID').then((value)=>{
          this.cartID = value;

          this.cartProvider.postCartDetail(this.cartID).subscribe(
            res => {
              let resultset: any = res;
              this.products = resultset.data;
              this.cartID2 = value;
              console.log("orders", this.products);
      
            }, error => {
              console.log(error);
          });

          // let cartStorageVal = this.merchantID + 'cartID';

          // console.log('menu ordering storage', cartStorageVal);

          // this.storage.get(cartStorageVal).then((value)=>{

          //   console.log('cartStorageVal', value);

          // });

          // this.storage.get(cartStorageVal).then((value)=>{

          //     this.cartProvider.postCartDetail(value).subscribe(
          //       res => {
          //         let resultset: any = res;
          //         this.products = resultset.data;
          //         this.cartID2 = value;
          //         console.log("orders", this.products);
          
          //       }, error => {
          //         console.log(error);
          //     });
        
          //   });


        });



        


   // });

    //Items Array Value
    //0-cartID
    //1-ipayuID
    //2-merchantID
    //3-userID

    // console.log('items', items);
    // console.log('ipayuid', items['1']);
    // console.log('merchant_id', items['2']);


  }

  loadCartMain(cartID, merchantID){

    console.log('idLoadCartMAin', cartID);
    console.log('merchantIDCartMAin', merchantID);


    this.cartProvider.getCart(cartID, merchantID).subscribe(
      res => {
        let resultset: any = res;
        this.carts = resultset.data;

        if(this.carts.length != 0){
          console.log("cart", this.carts);

          var sub = this.carts[0].grand_total;
          console.log("sub", sub);
          let vat = sub * .12
  
          let grand_total = (parseInt(sub) + vat + 50);
  
          this.f_grand = grand_total.toFixed(2);
          this.f_vat = vat.toFixed(2);
        }else{

          this.storage.set('itemQuantity', 0);

        }

      }, error => {
        console.log(error);
    });
    
  }

  goHome(){
    this.navCtrl.push('BookstorePage');
  }

  openPlaceOrder(){
    let modalRating = this.modalCtrl.create('PaymantsummaryPage',{
      cash: this.cash
    })
    modalRating.present()
  } 

  cancelOrder(pid: number, cdid: number, cit: number){
    console.log('remove', pid, cdid, cit)

    this.cartProvider.cancelOrder(pid, cdid, cit).subscribe(
      res => {
        let resultset: any = res;
        this.remove_item = resultset.data;

        this.ionViewDidLoad();

        console.log('item', this.remove_item);

        this.storage.get('itemQuantity').then((val) => {
          this.cartQty = val;
        });

        this.navCtrl.setRoot(this.navCtrl.getActive().component);   
        
      }, error => {
        console.log(error);
    });

    const toast = this.toastCtrl.create({
      message: 'Item removed.',
      duration: 2000
    });
    toast.present();          
  }

  add(pid: number, qty: number, cdid: number){
    console.log(pid, qty, cdid)

    let new_qty = Math.round(qty)

    new_qty += 1;

    console.log("qty", new_qty)

    this.cartProvider.updateQty(new_qty, pid, cdid).subscribe(
      res => {
        let resultset: any = res;
        this.update_qty = resultset.data;

        this.ionViewDidLoad()
        
      }, error => {
        console.log(error);
    });

  }

  remove(pid: number, qty: number, cdid: number){
    console.log(pid, qty)

    if(qty > 1){
      let new_qty = Math.round(qty)

      new_qty -= 1;

      console.log("qty", new_qty)

      this.cartProvider.updateQty(new_qty, pid, cdid).subscribe(
        res => {
          let resultset: any = res;
          this.update_qty = resultset.data;
  
          this.ionViewDidLoad()
          
        }, error => {
          console.log(error);
      });
    }
  }
}
