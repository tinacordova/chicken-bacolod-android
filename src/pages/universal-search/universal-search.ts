import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, App } from 'ionic-angular';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

import { UniversalSearchProvider } from '../../providers/universal-search/universal-search';


/**
 * Generated class for the UniversalSearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-universal-search',
  templateUrl: 'universal-search.html',
})
export class UniversalSearchPage {
keyword: any;
merchants: any = [];
products: any = [];
specialOffers: any = [];
vouchers: any = [];
events: any = [];
searchChangeObserver;
origin: string;
closeFilter: Boolean = true;
newFilter: Boolean = false;
clearStatus: Boolean = false;
resCount: number;
noResult: Boolean = true;

  constructor(
    public navCtrl: NavController, 
    public appCtrl: App,
    public http: Http,
    private universalSearchProvider: UniversalSearchProvider,
    public loadingCtrl: LoadingController,
    public navParams: NavParams) {  

      this.keyword = this.navParams.get('keyword');
      console.log('keyword', this.keyword);
      console.log('loc', this.navParams.get('test'));
      console.log('loc2', localStorage.getItem('key'));
  }

  ionViewDidLoad() {
    this.keyword = this.navParams.get('keyword');
    let paramOrigin = this.navParams.get('origin');
    

    console.log('param-origin', this.navParams.get('origin'));
    


    if(paramOrigin === "location"){
      
      this.origin = paramOrigin;
      console.log('I am from location');
      this.loadSearchResultLocation();

    }else if(paramOrigin === "uni-search"){
      
      this.origin = "uni-search";
      console.log('I am from uni-search');
      this.loadSearchResult(this.keyword);
    
    }
    
    // else{
    //   console.log('else');
    //   this.loadSearchResult(this.keyword);
    
    // }

    // console.log('origin', this.origin);

    // console.log('this is the keyword', this.keyword);
    // this.loadSearchResult(this.keyword);

  }

  loadSearchResultLocation(){
    this.closeFilter = false;
    this.newFilter = true;

    let data = {
      "keyword": localStorage.getItem('key')
    };

    this.universalSearchProvider.universalSearchLocation(data).subscribe(
      res => {
        // let loader = this.loadingCtrl.create({
        //   content: 'Loading Page ... ',
        // });
    
        // loader.present();
        let resultset: any = res;

        this.merchants = resultset.data.merchants;
        this.products = resultset.data.products;
        this.specialOffers = resultset.data.special_offers;
        this.vouchers = resultset.data.vouchers;
        this.events = resultset.data.events;

        //loader.dismiss();

        if(this.merchants.length == 0 && this.products.length == 0 && this.specialOffers.length == 0 && this.vouchers.length == 0 && this.events.length == 0){
          //Display no result found
          this.noResult = false;
        } 


      }, error => {
        console.log(error);
    });
  }

  loadSearchResult(keyword: any){
    let data = {
      "keyword": keyword
    };

    this.universalSearchProvider.universalSearch(data).subscribe(
      res => {

        // let loader = this.loadingCtrl.create({
        //   content: 'Loading Page ... ',
        // });
    
        // loader.present();

        let resultset: any = res;

        this.merchants = resultset.data.merchants;
        this.products = resultset.data.products;
        this.specialOffers = resultset.data.special_offers;
        this.vouchers = resultset.data.vouchers;
        this.events = resultset.data.events;

        if(this.merchants.length == 0 && this.products.length == 0 && this.specialOffers.length == 0 && this.vouchers.length == 0 && this.events.length == 0){
          //Display no result found
          this.noResult = false;
        } 

        console.log('merchant', this.merchants.length);
        console.log('products', this.products.length);
        console.log('sp', this.specialOffers.length);
        console.log('vouchers', this.vouchers.length);
        console.log('events', this.events.length);

        //loader.dismiss();

      }, error => {
        console.log(error);
    });
  
  }

  goToApp(page:string, merchant_id:number){

    localStorage.setItem("new_merchant_id", merchant_id.toString())
  
    let loader = this.loadingCtrl.create({
      content: 'Loading Page ... ',
    });

    loader.present();

    setTimeout(() => {

      this.navCtrl.push(page, 
      {merchant_id: merchant_id});

    }, 1000);

    setTimeout(() => {
      loader.dismiss();
    }, 3000);

  }

  goBack() {
    //this.navCtrl.pop();
    this.appCtrl.getRootNav().push('HomePage');
  }

  navigateTo(page:string){
    this.appCtrl.getRootNav().push(page);
  }

  newNavigate(page:string){
    this.appCtrl.getRootNav().push(page);
  }

  goTo(){
    this.appCtrl.getRootNav().push('UniversalSearchFilterPage');
  }

  nextTo(){
    this.navCtrl.pop();
    // this.appCtrl.getRootNav().push('FilterPage');
  } 

  specialOfferDetail(prod_id){
    //Use the 1st endpoint for product detail: url/product/id
    localStorage.setItem('uni_search', 'false');

    let loader = this.loadingCtrl.create({
      content: 'Loading Page ... ',
    });

    loader.present();

    setTimeout(() => {

      this.navCtrl.push('RestoSpecialDetailPage', 
      {prod_id: prod_id});

    }, 1000);

    setTimeout(() => {
      loader.dismiss();
    }, 3000);

  }

  productDetail(prod_id){

    //Use the 2nd endpoint for product detail: url/product_uni/id
    localStorage.setItem('uni_search', 'true');

    let loader = this.loadingCtrl.create({
      content: 'Loading Page ... ',
    });

    loader.present();

    setTimeout(() => {

      this.navCtrl.push('RestoModalPage', 
      {prod_id: prod_id});

    }, 1000);

    setTimeout(() => {
      loader.dismiss();
    }, 3000);

  }

  openVoucherDetail(voucher_id){

    let loader = this.loadingCtrl.create({
      content: 'Loading Page ... ',
    });

    loader.present();

    setTimeout(() => {

      this.navCtrl.push('VoucherModalPage', 
      {voucher_id: voucher_id});

    }, 1000);

    setTimeout(() => {
      loader.dismiss();
    }, 3000);

  }

  openPromoDetail(promo_id){

    let loader = this.loadingCtrl.create({
      content: 'Loading Page ... ',
    });

    loader.present();

    setTimeout(() => {

      this.navCtrl.push('PromoModalPage', 
      {promo_id: promo_id});

    }, 1000);

    setTimeout(() => {
      loader.dismiss();
    }, 3000);

  }

  universalSearchClick(keyword: any){

    console.log('keyword', keyword);

    let data = {
      "keyword": keyword
    };

    this.universalSearchProvider.universalSearch(data).subscribe(
      res => {
        let resultset: any = res;

        this.merchants = resultset.data.merchants;
        this.products = resultset.data.products;
        this.specialOffers = resultset.data.special_offers;
        this.vouchers = resultset.data.vouchers;
        this.events = resultset.data.events;

        console.log('merchant-result', this.merchants);
        console.log('products-result', this.products);
        console.log('special-result', this.specialOffers);
        console.log('vouchers-result', this.vouchers);
        console.log('events-result', this.events);

      }, error => {
        console.log(error);
    });

  }

  universalSearch(ev: any) {
    // set val to the value of the searchbar
    const val = ev.target.value;

    console.log('Searching...', val);

      setTimeout(() => {
        this.loadSearchResult(val);
        console.log('test');
        
      }, 2000);

  }

  onSearchChange(searchValue: string) {
    
      if (!this.searchChangeObserver) {
        Observable.create(observer => {
          this.searchChangeObserver = observer;
        }).pipe(debounceTime(1500)) // wait 300ms after the last event before emitting last event
          .pipe(distinctUntilChanged()) // only emit if value is different from previous value
          .subscribe(res =>{

            this.resCount = res.length;

            if(this.resCount > 0 && res.length > 0 && this.clearStatus === false){

              let loader = this.loadingCtrl.create({
                content: 'Loading Page ... ',
              });
          
              loader.present();

              this.loadSearchResult(res);

              loader.dismiss();
            }
            
            this.clearStatus = false;
  
          });
      }
  
      this.searchChangeObserver.next(searchValue)

  } 


  universalSearchEnter(ev: any) {
    const val = ev.target.value;

    console.log('Universal search enter', val);

      setTimeout(() => {
        this.loadSearchResult(val);
        console.log('test');
        
      }, 2000);

  }

  onClear(event: any){
    //This is a marker that the clear search was clicked (the x button)
    this.clearStatus = true;
  }

  onChange(event: any){
    //Get the string length of the search bar and assign to resCount
    this.resCount = event.target.value.length;
  }

}
