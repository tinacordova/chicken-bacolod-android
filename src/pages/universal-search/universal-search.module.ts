import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UniversalSearchPage } from './universal-search';

@NgModule({
  declarations: [
    UniversalSearchPage,
  ],
  imports: [
    IonicPageModule.forChild(UniversalSearchPage),
  ],
})
export class UniversalSearchPageModule {}
