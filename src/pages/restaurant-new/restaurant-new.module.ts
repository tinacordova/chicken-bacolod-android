import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RestaurantNewPage } from './restaurant-new';

@NgModule({
  declarations: [
    RestaurantNewPage,
  ],
  imports: [
    IonicPageModule.forChild(RestaurantNewPage),
  ],
})
export class RestaurantNewPageModule {}
