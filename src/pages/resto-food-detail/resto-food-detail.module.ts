import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RestoFoodDetailPage } from './resto-food-detail';

@NgModule({
  declarations: [
    RestoFoodDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(RestoFoodDetailPage),
  ],
})
export class RestoFoodDetailPageModule {}
