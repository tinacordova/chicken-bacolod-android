import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RestoSpecialDetailPage } from './resto-special-detail';

@NgModule({
  declarations: [
    RestoSpecialDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(RestoSpecialDetailPage),
  ],
})
export class RestoSpecialDetailPageModule {}
