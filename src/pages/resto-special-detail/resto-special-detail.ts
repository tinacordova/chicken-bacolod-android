import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App, ModalController, ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { ProductProvider } from '../../providers/product/product';
import { CartProvider } from '../../providers/cart/cart';

@IonicPage()
@Component({
  selector: 'page-resto-special-detail',
  templateUrl: 'resto-special-detail.html',
})
export class RestoSpecialDetailPage {

  prodParamId: number;
  products: any = [];
  productSubImages: any = [];
  prod_id: any;
  cart: any [];
  merchantParamId: any;
  orders: any[];
  cartcounter: any [];
  counterFront: number;
  updateorder: any [];
  merc_id: any;
  table_num: any;
  new_mer_id: any;
  merchantID: number;
  cartID: number;

  count_order: any[];

  public counter : number = 1;

  localStorageUserId: string;
  localStorageFirstName: string;
  localStorageLastName: string;

  constructor(
    public navCtrl: NavController, 
    public appCtrl: App, 
    public navParams: NavParams,
    public productProvider: ProductProvider,
    public cartProvider: CartProvider,
    public modalCtrl: ModalController,
    private storage: Storage,
    public toastCtrl: ToastController) {

      this.prodParamId = this.navParams.get('prod_id');
      this.merc_id = localStorage.getItem('merch_id')

      this.new_mer_id = this.navParams.get('mer_id')
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RestoSpecialDetailPage');

    this.loadProductDetail();
    this.loadProductSubImages();
    this.loadCartDetailCounter();

    this.localStorageUserId = localStorage.getItem('user_id');
    this.localStorageFirstName = localStorage.getItem('user_firstname');
    this.localStorageLastName = localStorage.getItem('user_lastname');
  }

  loadProductDetail(){
    this.productProvider.getProductDetailV8(this.prodParamId).subscribe(
      res => {
        let resultset: any = res;
        this.products = resultset.data;
        console.log("product detail", this.products);

      }, error => {
        console.log(error);
    });
  }

  loadProductSubImages(){
    this.productProvider.getProductSubImages(this.prodParamId).subscribe(
      res => {
        let resultset: any = res;
        this.productSubImages = resultset.data;
        console.log("product sub images", this.productSubImages);
      }, error => {
        console.log(error);
    });
  }

  goBack() {
    this.navCtrl.pop();
  }

  getMenu(id: number, amt: number, mid: number){
    localStorage.setItem('items', this.prod_id);
    this.table_num = 0;


    this.storage.get('merchantID').then((value)=>{
      this.merchantID = value;

      console.log('merchant_id', this.merchantID);


      this.cartProvider.cartCounter(this.merchantID).subscribe(
        res => {
          let resultset: any = res;
          this.count_order = resultset.data;
          console.log("first", this.count_order);
  
          if (this.count_order[0].total_count == 0){
            console.log("insert")
  
            let modalTable = this.modalCtrl.create('TablePage', {
              pid: id, amt: amt, mid: mer_id, count: this.counter
            })
            modalTable.present()
  
          }else{
            var mer_id =  localStorage.getItem('new_merchant_id')
            console.log("insert/update")
  
            this.cartProvider.postCart(parseInt(this.localStorageUserId), 1, this.table_num, id, this.counter, amt, this.merchantID).subscribe(
              res => {
                let resultset: any = res;
                this.orders = resultset.data;
                console.log("orders", this.orders);
  
              }, error => {
                console.log(error);
  
                const toast = this.toastCtrl.create({
                  message: 'Order saved in the cart.', 
                  duration: 2000
                });
                toast.present();
            });
          }
  
        }, error => {
          console.log(error);
      });

    
    });

  }

  add(){
    this.counter += 1;
  }

  remove(){
    if(this.counter > 1){
      this.counter -= 1;
    }
  }
  
  toHome(){
    this.navCtrl.push('BookstorePage');
  }

  toCart(){
    this.navCtrl.push('MenuOrderingPage')
  }

  loadCartDetailCounter(){
    this.storage.get('cartID').then((value)=>{
      this.cartID = value;

      this.storage.get('merchantID').then((value)=>{
        this.merchantID = value;

        
        let cartStorageVal = this.merchantID + 'cartID';
        console.log('cartStorageVal Bookstore', cartStorageVal);

          this.cartProvider.getCartDetailCounter(this.cartID, this.merchantID).subscribe(
            res => {
              let resultset: any = res;
              this.cartcounter = resultset.data;
              
              console.log("counter", this.cartcounter[0].detail_counter);
              this.counterFront = this.cartcounter[0].detail_counter;
  
              if (this.cartcounter[0].detail_counter == 0)
              this.counterFront + 1
              
            }, error => {
              console.log(error);
          });
        
      });

    });

  }

}
