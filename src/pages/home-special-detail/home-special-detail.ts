import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App, ModalController, ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { ProductProvider } from '../../providers/product/product';
import { CartProvider } from '../../providers/cart/cart';
import { ProductDetailProvider } from '../../providers/product-detail/product-detail';

@IonicPage()
@Component({
  selector: 'page-home-special-detail',
  templateUrl: 'home-special-detail.html',
})
export class HomeSpecialDetailPage {

  prodParamId: number;
  products: any = [];
  productSubImages: any = [];
  prod_id: any;
  cart: any [];
  merchantParamId: any;
  orders: any[];
  cartcounter: any [];
  counterFront: number;
  updateorder: any [];
  merc_id: any;
  table_num: any;
  new_mer_id: any;
  new_merID: any;
  merchantID: number;
  cartID: number;

  count_order: any[];

  public counter : number = 1;

  constructor(public navCtrl: NavController, 
    public appCtrl: App, 
    public navParams: NavParams,
    public productProvider: ProductProvider,
    public cartProvider: CartProvider,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    private storage: Storage,
    public detailProvider: ProductDetailProvider) {

      this.prodParamId = this.navParams.get('prod_id');
      this.merc_id = localStorage.getItem('merch_id')

      this.new_mer_id = this.navParams.get('mer_id')
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomeSpecialDetailPage');
    this.loadProductDetail();
    this.loadProductSubImages();
    this.loadCartDetailCounter()
  }

  loadProductDetail(){
    this.productProvider.getProductDetailV8(this.prodParamId).subscribe(
      res => {
        let resultset: any = res;
        this.products = resultset.data;
        console.log("product detail", this.products);

        this.new_merID = this.products[0].merchant_id

      }, error => {
        console.log(error);
    });
  }

  loadProductSubImages(){
    this.productProvider.getProductSubImages(this.prodParamId).subscribe(
      res => {
        let resultset: any = res;
        this.productSubImages = resultset.data;
        console.log("product sub images", this.productSubImages);
      }, error => {
        console.log(error);
    });
  }

  goBack() {
    this.navCtrl.pop();
  }

  getMenu(id: number, amt: number, mid: number){
    localStorage.setItem('items', this.prod_id);
    this.table_num = localStorage.getItem('table_number')
    
      var mer_id =  localStorage.getItem('new_merchant_id')
      console.log("insert")

      this.cartProvider.postCart(1, 1, 0, id, this.counter, amt, this.new_merID).subscribe(
        res => {
          let resultset: any = res;
          this.orders = resultset.data;
          console.log("orders", this.orders);

        }, error => {
          console.log(error);

          const toast = this.toastCtrl.create({
            message: 'Order saved in the cart.',
            duration: 2000
          });
          toast.present();
      });
        

    // this.navCtrl.push('RestaurantPage'),{
    //   merchant_id: mid}
  }

  add(){
    this.counter += 1;
  }

  remove(){
    if(this.counter > 1){
      this.counter -= 1;
    }
  }
  
  toHome(){
    this.navCtrl.push('HomePage')
  }

  toCart(){
    this.navCtrl.push('MenuOrderingPage')
  }

  loadCartDetailCounter(){
    this.storage.get('merchantID').then((value)=>{
      this.merchantID = value;

      this.storage.get('cartID').then((value)=>{
        this.cartID = value;

        this.cartProvider.getCartDetailCounter(this.merchantID, this.cartID).subscribe(
          res => {
            let resultset: any = res;
            this.cartcounter = resultset.data;
            
            console.log("counter", this.cartcounter[0].detail_counter);
            this.counterFront = this.cartcounter[0].detail_counter
    
            if (this.cartcounter[0].detail_counter == 0)
            this.counterFront + 1
            
          }, error => {
            console.log(error);
        });
  
      });

    });
  }
  

}
