import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomeSpecialDetailPage } from './home-special-detail';

@NgModule({
  declarations: [
    HomeSpecialDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(HomeSpecialDetailPage),
  ],
})
export class HomeSpecialDetailPageModule {}
