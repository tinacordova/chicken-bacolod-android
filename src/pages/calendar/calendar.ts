import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController } from 'ionic-angular';

import { Calendar } from '@ionic-native/calendar';

@IonicPage()
@Component({
  selector: 'page-calendar',
  templateUrl: 'calendar.html',
})
export class CalendarPage {

  promo_name: any;
  val_start: any;
  val_end: any;
  promo_loc: any;
  promo_desc: any;

  event;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public viewCtrl: ViewController,
              public calendar: Calendar,
              public toastCtrl: ToastController) {

      this.promo_name = this.navParams.get('promoName');
      this.val_start = this.navParams.get('valStart');
      this.val_end = this.navParams.get('valEnd');
      this.promo_loc = this.navParams.get('promoLoc');
      this.promo_desc = this.navParams.get('promoDesc');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CalendarPage');
    console.log(this.promo_name, this.val_start, this.val_end, this.promo_loc, this.promo_desc)
  }

  dismiss(){
    this.viewCtrl.dismiss()
  }

  submit(){
    this.event = { 
    title: this.promo_name, 
    location: this.promo_loc, 
    message: this.promo_desc, 
    startDate: this.val_start,
    endDate: this.val_end
                };
      this.calendar.createEvent(
        this.event.title, 
        this.event.location, 
        this.event.message, 
        new Date(this.event.startDate), 
        new Date(this.event.endDate))

    this.viewCtrl.dismiss()
    const toast = this.toastCtrl.create({
      message: 'Your event has been saved to your calendar.',
      duration: 2000
    });
    toast.present();
  }

}
