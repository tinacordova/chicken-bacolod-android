import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, App, LoadingController, ModalController, ToastController, MenuController, Content } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { MerchantProvider } from '../../providers/merchant/merchant';
import { PromoProvider } from '../../providers/promo/promo';
import { VoucherProvider } from '../../providers/voucher/voucher';
import { ProductProvider } from '../../providers/product/product';
import { CartProvider } from '../../providers/cart/cart';
import { c } from '@angular/core/src/render3';

/**
 * Generated class for the SearchResultPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search-result',
  templateUrl: 'search-result.html',
})
export class SearchResultPage {

  @ViewChild(Content) content: Content;

  merchants: any = [];
  promos: any = [];
  vouchers: any = [];
  products: any = [];
  merchant_ads: any = [];
  specialOffers: any = [];
  prodCategories: any = [];
  prodVouchers: any = [];
  prodSpecialOffers: any = [];
  prodEvents: any = [];
  orders: any[];
  cartcounter: any [];
  counterFront: number;
  newcount: number;
  counter : number = 1;
  count_order: any [];
  userID: number;
  cartID: number;
  merchantID: number;
  main: any;
  cart_id: number;
  tableNumber: number;
  itemCount: number;
  prodCount: number;
  detectCount: number;

  show: boolean = false;
  localStorageUserId: string;
  firstName: string;
  lastName: string;
  textSearchTemplate: boolean = false;
  filterSearchTemplate: boolean = false; 

  firstFilter: boolean = true;
  secondFilter: boolean = false;
  productCounters: number;

  my_id: number;
  my_amt: number;
  table_num: any;
  keyword: any;
  items: string[];
  search_results: any;
  noResultFound: Boolean = false;
  cartQty: number;

  constructor(
    public navCtrl: NavController, 
    public appCtrl: App,
    public menuCtrl: MenuController,
    public merchantProvider: MerchantProvider,
    public promoProvider: PromoProvider,
    public voucherProvider: VoucherProvider,
    public productProvider: ProductProvider,
    private loadingCtrl: LoadingController,
    public cartProvider: CartProvider,
    public toastCtrl: ToastController, 
    public modalCtrl: ModalController,
    private storage: Storage,
    public navParams: NavParams) {
      this.keyword = this.navParams.get('keyword');
      let paramOrigin = this.navParams.get('origin');

      if(paramOrigin == "bookstore"){
        this.getCartID();
        this.loadShopInfo();   
        this.loadSearchResult(this.keyword);
      }else{

        console.log('location');

        this.getCartID();
        this.loadShopInfo();    
        this.firstFilter = false;
        this.secondFilter = true;
    
        this.loadBookstoreMerchantCategory();

      }

      this.storage.get('firstName').then((val)=>{
        this.firstName = val;
      });

      this.storage.get('lastName').then((val)=>{
        this.lastName = val;
      });

  }

  ionViewDidEnter(){
    this.getCartID();
  }

  ionViewDidLoad() {
  this.getCartID();
  //   this.loadShopInfo();
  //   //this.loadMerchantAds();
  //   //this.loadProdSpecialOffer();
  //   //this.loadMerchantPromo();
  //   //this.loadMerchantVoucher();
  //   //this.loadProductMerchantCategory();

  //   this.firstFilter = false;
  //   this.secondFilter = true;

  //   this.loadBookstoreMerchantCategory();
  }

  loadSearchResult(keyword){
    this.storage.get('merchantID').then((value)=>{
      this.merchantID = value;

      console.log('text search loaded');

      let data = {
        "merchant_id":this.merchantID,
        "keyword":keyword
      };

      this.productProvider.getBookstoreTextSearch(data).subscribe(
        res => {
          let resultset: any = res;

          this.prodCount = resultset.product_counter;
          this.prodCategories = resultset.products;
          this.prodVouchers = resultset.vouchers;
          this.prodSpecialOffers = resultset.special_offers;
          this.prodEvents = resultset.promos;

          this.detectCount = this.prodCount + this.prodVouchers.length + this.prodSpecialOffers.length + this.prodEvents.length;

          if(this.detectCount < 1){
            this.noResultFound = true;
          }else{
            this.noResultFound = false;
          }
          
          console.log('voucher', this.prodVouchers.length);
          console.log('sp', this.prodSpecialOffers.length);
          console.log('events', this.prodEvents.length);
          console.log('itemCount', this.itemCount);
          console.log('prod len', this.prodCategories);

          this.textSearchTemplate = true;
        }, error => {
          console.log(error);
      });
    });
  }

  getCartID(){

    this.storage.get('userID').then((value)=>{
      this.userID = value;

      console.log('user_id', this.userID);
  
      this.storage.get('merchantID').then((value)=>{
        this.merchantID = value;

        console.log('merchant_id', this.merchantID);

        this.storage.get('tableNumber').then((value)=>{
          this.tableNumber = value;

          this.cartProvider.cartMain(this.userID, this.merchantID, 0).subscribe(
            res => {
              let resultset: any = res;
              this.main = resultset.data;
              console.log("main", this.main);
              console.log("cartid", this.main[0].cart_id);
              console.log("mainid", this.main[0].ipayu_id);

              this.loadCartDetailCounter(this.main[0].cart_id);
      
              this.cart_id = this.main[0].cart_id      
              this.storage.set('cartID', this.cart_id);
      
            }, error => {
              console.log(error);
          });
        
        });
      
      });
    
    });
  }

  loadShopInfo(){

    this.storage.get('merchantID').then((val)=>{
      this.merchantID = val;

      this.merchantProvider.merchantInfo(this.merchantID).subscribe(
        res => {
          let resultset: any = res;
          this.merchants = resultset.data;
  
          console.log("merchants", this.merchants);
        }, error => {
          console.log(error);
      });
    
    });
  }

  loadMerchantPromo(){
    this.storage.get('merchantID').then((val)=>{
      this.merchantID = val;

      this.promoProvider.getMerchantPromo(this.merchantID).subscribe(
        res => {
          let resultset: any = res;
          this.promos = resultset.data;
  
          console.log("merchant promos", this.promos);     
        }, error => {
          console.log(error);
      });
  
    });

  }

  loadMerchantVoucher(){

    this.storage.get('merchantID').then((value)=>{
      this.merchantID = value;

      this.voucherProvider.getMerchantVoucher(this.merchantID).subscribe(
        res => {
          let resultset: any = res;
          this.vouchers = resultset.data;
          
          console.log("merchant vouchers", this.vouchers);     
        }, error => {
          console.log(error);
      });
    
    });

  }

  loadProductMerchantCategory(){
    this.storage.get('merchantID').then((value)=>{
      this.merchantID = value;

      this.productProvider.getProductMerchantCategory(this.merchantID).subscribe(
        res => {
          let resultset: any = res;
          this.prodCategories = resultset.data;
  
        }, error => {
          console.log(error);
      });
    });
  }

  loadBookstoreMerchantCategory(){
    this.storage.get('merchantID').then((value)=>{
      this.merchantID = value;

      let data = {
        "merchant_id":this.merchantID,
        "location":localStorage.getItem('key'),
        "category":localStorage.getItem('categorize')
      };

      this.productProvider.getBookstoreMerchantCategory(data).subscribe(
        res => {
          let resultset: any = res;
          this.prodCategories = resultset.data;
          this.filterSearchTemplate = true;
  
        }, error => {
          console.log(error);
      });
    });
  }

  loadCartDetailCounter(cartID){

        this.storage.get('merchantID').then((value)=>{
          this.merchantID = value;

            this.cartProvider.getCartDetailCounter(cartID, this.merchantID).subscribe(
              res => {
                let resultset: any = res;
                this.cartcounter = resultset.data;
                
                console.log("counter", this.cartcounter[0].detail_counter);
                this.counterFront = this.cartcounter[0].detail_counter
        
                if (this.cartcounter[0].detail_counter == 0)
                this.counterFront + 1

                this.storage.set('itemQuantity', this.counterFront);

                this.storage.get('itemQuantity').then((val) => {

                  if(val != 0){
                    this.cartQty = val;
                  }else{
                    this.cartQty = 0;
                  }
                
                });
                
              }, error => {
                console.log(error);
            });

        });
    //});


  }


  openCart(){
    this.appCtrl.getRootNav().push('MenuOrderingPage');
  }
  
  loadProdSpecialOffer(){
    this.storage.get('merchantID').then((val) => {
      this.merchantID = val;

        this.productProvider.getSpecialOffer(this.merchantID).subscribe(
          res => {
            let resultset: any = res;
            this.specialOffers = resultset.data;
    
            console.log("Special Offer", this.specialOffers);     
          }, error => {
            console.log(error);
        });

    });

  }

  loadMerchantAds(){

    this.storage.get('merchantID').then((val) => {
      this.merchantID = val;

      let data = {
        "merchant_id": this.merchantID
      };
  
      this.merchantProvider.getMerchantAds(data).subscribe(
        res => {
          let resultset: any = res;
          this.merchant_ads = resultset.data;
  
          console.log("merchant ads", this.merchant_ads);     
        }, error => {
          console.log(error);
      });
  
    });
  }

  goPromo(){
    this.appCtrl.getRootNav().push('BookstorePromoDetailPage');
  }

  goNext(){
    this.appCtrl.getRootNav().push('BookstoreVoucherDetailPage');
  }

  openMenuDetail(){
    this.appCtrl.getRootNav().push('BookstoreBooksDetailPage');
  }

  openSpecialDetail(prod_id){

    let loader = this.loadingCtrl.create({
      content: 'Loading Page ... ',
    });

    loader.present();

    setTimeout(() => {

      this.navCtrl.push('RestoSpecialDetailPage', 
      {prod_id: prod_id});

    }, 1000);

    setTimeout(() => {
      loader.dismiss();
    }, 3000);

  }

  openBookDetail(prod_id){

    let loader = this.loadingCtrl.create({
      content: 'Loading Page ... ',
    });

    loader.present();

    setTimeout(() => {

      this.navCtrl.push('BookstoreDetailPage', 
      {prod_id: prod_id});

    }, 1000);

    setTimeout(() => {
      loader.dismiss();
    }, 3000);

  }

  openPromoDetail(promo_id) {
    let loader = this.loadingCtrl.create({
      content: "Loading Page ... "
    });

    loader.present();

    setTimeout(() => {
      this.navCtrl.push("PromoModalPage", { promo_id: promo_id });
    }, 1000);

    setTimeout(() => {
      loader.dismiss();
    }, 3000);
  }

  add(pid: number, i){
    this.newcount = i
    console.log('add', pid, this.newcount)
    this.counter += 1;
    
  }

  remove(pid: number, i){
    console.log('remove', pid, i)
    if(this.counter > 1){
      this.counter -= 1;
    }
  }

  // addtoOrder(id: number, amt: number){
  //   this.storage.get('userID').then((val) => {
  //     this.userID = val;
    

  //     this.storage.get('merchantID').then((value) => {
  //       this.merchantID = value;

  //       this.cartProvider.cartCounter(this.merchantID).subscribe(
  //         res => {
  //           let resultset: any = res;
  //           this.count_order = resultset.data;
  //           console.log("first", this.count_order);

       
  //               this.cartProvider.postCart(this.userID, 1, 0, id, this.counter, amt, this.merchantID).subscribe(
  //                 res => {
  //                   let resultset: any = res;
  //                   this.orders = resultset.data;
  //                   console.log("orders", this.orders);
        
  //                 }, error => {
  //                   console.log(error);
  //               });
    
  //               this.cartProvider.cartMain(this.userID, this.merchantID, 0).subscribe(
  //                 res => {
  //                   let resultset: any = res;
  //                   this.main = resultset.data;
  //                   console.log("main", this.main);
  //                   console.log("cartid", this.main[0].cart_id);
  //                   console.log("mainid", this.main[0].ipayu_id);

  //                   this.storage.set('cartID', this.main[0].cart_id);
            
  //                 }, error => {
  //                   console.log(error);
  //               });
                
  //               const toast = this.toastCtrl.create({
  //                 message: 'Order saved in the cart.',
  //                 duration: 2000
  //               });
      
  //               toast.present();
      
  //               this.appCtrl.getRootNav().push("BookstorePage");

  //               //this.navCtrl.setRoot(this.navCtrl.getActive().component);            
    
  //         }, error => {
  //           console.log(error);
  //       });


      
  //     });

  //   });
  // }


  addtoOrder(id: number, amt: number){

    //this.table_num = localStorage.getItem('table_number')

    this.storage.get('userID').then((val) => {
      this.userID = val;
    

      this.storage.get('merchantID').then((value) => {
        this.merchantID = value;

        this.cartProvider.cartCounter(this.merchantID).subscribe(
          res => {
            let resultset: any = res;
            this.count_order = resultset.data;
            console.log("first", this.count_order);

       
                this.cartProvider.postCart(this.userID, 1, 0, id, this.counter, amt, this.merchantID).subscribe(
                  res => {
                    let resultset: any = res;
                    this.orders = resultset.data;
                    console.log("orders", this.orders);
        
                  }, error => {
                    console.log(error);
                });
    
                this.cartProvider.cartMain(this.userID, this.merchantID, 0).subscribe(
                  res => {
                    let resultset: any = res;
                    this.main = resultset.data;
                    console.log("main", this.main);
                    console.log("cartid", this.main[0].cart_id);
                    console.log("mainid", this.main[0].ipayu_id);

                    this.storage.set('cartID', this.main[0].cart_id);
            
                  }, error => {
                    console.log(error);
                });
                
                const toast = this.toastCtrl.create({
                  message: 'Order saved in the cart.',
                  duration: 2000
                });
      
                toast.present();
                
                //this.navCtrl.setRoot(this.navCtrl.getActive().component);   
                this.navCtrl.push('BookstorePage');         
    
          }, error => {
            console.log(error);
        });


      
      });

    });
  }


  navigateTo(page: string) {
    this.appCtrl.getRootNav().push(page);
  }

  navigateBack(){
    this.navCtrl.pop();
  }

  goBack() {
    this.navCtrl.pop();
  }

  goBackPrev(){
    this.navCtrl.pop();
  }

  goTo() {
    this.appCtrl.getRootNav().push("BooksFilterPage");
    console.log("sana");
  }

  goToFilter() {
    this.navCtrl.pop();
    console.log("sana");
  }

  getItems(ev: any) {
    // set val to the value of the searchbar
    const val = ev.target.value;

    if (val.length >= 1) {
      this.search_results = [];

      this.show = true;
      //Fetched the result from api
      // this.voucherProvider.getVoucherFilter(val).subscribe(
      //   res => {
      //     let resultset: any = res;
      //     let resultset2: any = res;

      //     let loader = this.loadingCtrl.create({
      //       content: "Loading Page ... "
      //     });

      //     loader.present();

      //     setTimeout(() => {
      //       this.productCounters = resultset2.product_counter;

      //       // if (this.productCounters.length > 0) {
      //       //   this.voucherBoolean = true;
      //       // }

      //       this.search_results = resultset.data;
      //     }, 1000);

      //     setTimeout(() => {
      //       loader.dismiss();
      //     }, 1000);

      //   },
      //   error => {
      //     console.log(error);
      //   }
      //);

      this.loadSearchResult(val);

      console.log("Thank you");
    }

    console.log("value", val);
    console.log("value lenth", val.length);

    // if the value is an empty string don't filter the items
    // if (val && val.trim() != "") {
    //   this.items = this.items.filter(item => {
    //     return item.toLowerCase().indexOf(val.toLowerCase()) > -1;
    //   });
    // }

  }

  openVoucherDetail(voucher_id) {
    let loader = this.loadingCtrl.create({
      content: "Loading Page ... "
    });

    loader.present();

    setTimeout(() => {
      this.navCtrl.push("VoucherModalPage", { voucher_id: voucher_id });
    }, 1000);

    setTimeout(() => {
      loader.dismiss();
    }, 3000);
  }

}
