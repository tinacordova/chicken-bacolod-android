import { Component, ViewChild, ElementRef} from '@angular/core';
import { IonicPage, NavController, NavParams, App} from 'ionic-angular';
import { SearchProvider } from '../../providers/search/search';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Geolocation } from '@ionic-native/geolocation'
import { ProductProvider } from '../../providers/product/product';

/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

declare var require 
var MarkerWithLabel = require('markerwithlabel')(google.maps);
declare var google
var geocoder;

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {
  private todo : FormGroup;
  show: boolean = true;
  private allParams ;
  public result: any = [];
  public locs: any = [];
  public products: any = [];
  public map_merchants: any = [];
  public locations: any = [];

  @ViewChild('map') mapRef: ElementRef;
  map: any;
  x: any;
  y: any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public appCtrl: App,
    public searchProvider:SearchProvider,
    private geolocation: Geolocation,
    private productProvider: ProductProvider,
    private formBuilder: FormBuilder) {

    this.allParams = this.navParams.data;
    console.log('test', this.navParams.data);

    this.todo = this.formBuilder.group({
      current_location: ['', Validators.required],
      item_description: ['']
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchPage');
    console.log('desc', this.navParams.data.desc);
    console.log('location', this.navParams.data.location);

    this.loadResult();    
    //this.initMap();

  }

  loadResult(){
    this.searchProvider.getResult(this.navParams.data.location, this.navParams.data.desc).subscribe(
      res => {
 
        this.result = res;

        //return data to html
        this.locs = this.result.data.location;
        this.products = this.result.data.item_desc;

      console.log('samsam', this.products);
        //  console.log("location-result", this.result.data.location);
        //  console.log("desc-result 1123", this.result.data.item_desc);
        //  console.log("position", this.result.data.merchant);

        //  console.log("position length", this.result.data.merchant.length);

         this.locations = this.result.data.merchant;

         //console.log('loc len: ', this.locations[1][3]);

         //console.log('yes', Object["values"](this.result.data.merchant));

        // Object.values(obj);
         
         

         console.log('map locations: ', this.locations);

        // let result = objArray.map(a => a.foo);

         //Start map plotting

         
         this.geolocation.watchPosition().subscribe(position => {
          console.log(position.coords.longitude + ' ' + position.coords.latitude);
          this.y = position.coords.longitude;
          this.x = position.coords.latitude;

          console.log('x', this.x);
          console.log('y', this.y);

          // for(i = 0; i < this.locations.length; i++){
          //   // console.log('sample', this.locations[i]);
          //   // //console.log('bal', this.locations[i].merchant_name);
          //   this.locations[i] = [this.locations[i].merchant_name, this.locations[i].latitude, this.locations[i].longitude, this.locations[i].image];
          // }
       
        console.log('loc loc', this.locations);

          //************************map start****************//
        
          var map = new google.maps.Map(document.getElementById('map'),{
            zoom: 15,
            mapTypeControl: false,
            zoomControl: false,
            streetViewControl: false,
            center: new google.maps.LatLng(this.x, this.y),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            styles: [
              {
                "featureType": "administrative.land_parcel",
                "elementType": "labels",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "poi",
                "elementType": "labels.text",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "poi.business",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "road",
                "elementType": "labels.icon",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "road.local",
                "elementType": "labels",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "transit",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
            ]
          });
       
        var infowindow = new google.maps.InfoWindow();
        
        var marker, i;

        for (i = 0; i < this.locations.length; i++) {
      
          var set_img =   {
            url: this.locations[i][3],
            scaledSize: new google.maps.Size(30, 30),
            // origin: new google.maps.Point(0, 0),
            // anchor: new google.maps.Point(32,65),
            labelOrigin: new google.maps.Point(40,33),
            // labelOrigin: new google.maps.Point(40,33)
        
          }
         
          var marker = new google.maps.Marker({
            
            position: new google.maps.LatLng(this.locations[i][1], this.locations[i][2]),
            // animation: google.maps.Animation.DROP,
            map: map,
            label: {
              text: this.locations[i][0],
              // color: 'white' 
              fontSize: "14px",
              labelClass: "map-marker-label",
            },
            icon: set_img
            
          });

          google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
              infowindow.setContent(this.locations[i][0]);
              infowindow.open(map, marker);
            }
          })(marker, i));
        } 

        
        marker = new google.maps.Marker({
          position: new google.maps.LatLng(this.x, this.y),
          map: map,
          icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png',
          // label: 'testing'
          // icon: {
          //   fillColor: 'green',
          // }
        });
        

        //************************map end****************//
   
        console.log("location", this.map_merchants);
      }, error => {
        console.log(error);
    });

         
         

         

         //End of map plott


      
      }, error => {
        console.log(error);
    });
  }

  searchForm(){
    console.log("test");
    console.log('form', this.todo.value);

    this.searchProvider.getResult(this.todo.value.current_location, this.todo.value.item_description).subscribe(
      res => {
 
        this.result = res;
        //console.log('data', this.result.data.item_desc);

        this.locations = this.result.data.location;
        this.products = this.result.data.item_desc;

         console.log("location-result", this.result.data.location);
         console.log("desc-result", this.result.data.item_desc);
      
      }, error => {
        console.log(error);
    });
  }

  initMap(loc: any, desc: any) {
    console.log("Location Loads!")
    this.productProvider.searchProduct(loc, desc).subscribe(
      res => {

        let resultset: any = res;
        this.map_merchants = resultset.data;
        let merchant_name = this.map_merchants.map(t=>t.merchant_name);
        let long = this.map_merchants.map(x=>x.longitude);
        let lat = this.map_merchants.map(y=>y.latitude);
        let img = this.map_merchants.map(z=>z.url)
        //let locations = long.length;

        console.log('name', merchant_name);
        console.log('long', long);
        console.log('lat', lat);
        console.log('img', img)

        this.geolocation.watchPosition().subscribe(position => {
          console.log(position.coords.longitude + ' ' + position.coords.latitude);
          this.y = position.coords.longitude;
          this.x = position.coords.latitude;
       

        for(i = 0; i < long.length; i++){
          this.locations[i] = [merchant_name[i], lat[i], long[i], img[i]];
        }

        console.log('loc loc', this.locations);

          //************************map start****************//
        
          var map = new google.maps.Map(document.getElementById('map'),{
            zoom: 15,
            mapTypeControl: false,
            zoomControl: false,
            streetViewControl: false,
            center: new google.maps.LatLng(this.x, this.y),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            styles: [
              {
                "featureType": "administrative.land_parcel",
                "elementType": "labels",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "poi",
                "elementType": "labels.text",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "poi.business",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "road",
                "elementType": "labels.icon",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "road.local",
                "elementType": "labels",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "transit",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
            ]
          });
       
        var infowindow = new google.maps.InfoWindow();
        
        var marker, i;

        for (i = 0; i < this.locations.length; i++) {
      
          var set_img =   {
            url: this.locations[i][3],
            scaledSize: new google.maps.Size(30, 30),
            // origin: new google.maps.Point(0, 0),
            // anchor: new google.maps.Point(32,65),
            labelOrigin: new google.maps.Point(40,33),
            // labelOrigin: new google.maps.Point(40,33)
        
          }
         
          var marker = new google.maps.Marker({
            
            position: new google.maps.LatLng(this.locations[i][1], this.locations[i][2]),
            // animation: google.maps.Animation.DROP,
            map: map,
            label: {
              text: this.locations[i][0],
              // color: 'white' 
              fontSize: "14px",
              labelClass: "map-marker-label",
            },
            icon: set_img
            
          });

          google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
              infowindow.setContent(this.locations[i][0]);
              infowindow.open(map, marker);
            }
          })(marker, i));
        } 

        
        marker = new google.maps.Marker({
          position: new google.maps.LatLng(this.x, this.y),
          map: map,
          icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png',
          // label: 'testing'
          // icon: {
          //   fillColor: 'green',
          // }
        });
        

        //************************map end****************//
   
        console.log("location", this.map_merchants);
      }, error => {
        console.log(error);
    });


  });
}

goBack(){
  this.appCtrl.getRootNav().push('LocationPage');
}

}
