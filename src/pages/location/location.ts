import { Component, ViewChild, ElementRef} from '@angular/core';
import { IonicPage, NavController, NavParams, App, Platform} from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { MerchantProvider } from '../../providers/merchant/merchant';
import { AlertController } from 'ionic-angular';
import { Diagnostic } from '@ionic-native/diagnostic';

/**
 * Generated class for the LocationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

declare var require 
var MarkerWithLabel = require('markerwithlabel')(google.maps);
declare var google
var geocoder;

@IonicPage()
@Component({
  selector: 'page-location',
  templateUrl: 'location.html',
})
export class LocationPage {
  private todo : FormGroup;
  merchants: any [];
  map_merchants: any [];
  locations: any = [];

  @ViewChild('map') mapRef: ElementRef;
  map: any;
  x: any;
  y: any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public appCtrl: App,
    private formBuilder: FormBuilder,
    private platform: Platform, 
    private geolocation: Geolocation, 
    private merchantProvider: MerchantProvider,
    private diagnostic: Diagnostic,
    private alertCtrl: AlertController) {

      this.todo = this.formBuilder.group({
        current_location: ['', Validators.required],
        item_description: ['']
      });

  }


  // ionViewDidLoad() {
  //   console.log('ionViewDidLoad LocationPage');
  //   this.initMap2();
  //   this.checkLocation();
  // }

  ionViewWillEnter() {
    this.initMap2();
    this.checkLocation();
    //this.navCtrl.setRoot(this.navCtrl.getActive().component);
    console.log('Runs when the page is about to enter and become the active page.');
}




  initMap2() {
    let map;
    let infoWindow;

    map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: 14.561372, lng: 121.015487},
      //center: {lat: null, lng: null},
      zoom: 15
    });

    console.log('map pos: ', map);

    //var image = 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png';

    var image = 'assets/icon/icons8-marker-40.png';

    var marker = new google.maps.Marker({
      position: map.center, 
      map: map,
      icon: image
    });

    infoWindow = new google.maps.InfoWindow;

    // Try HTML5 geolocation.
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(position) {
        var pos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };

        console.log('pos',pos);

        infoWindow.setPosition(pos);
        //infoWindow.setContent('Location found.');
        infoWindow.open(map);
        map.setCenter(pos);
      }, function() {
        this.handleLocationError(true, infoWindow, map.getCenter());
      });
    } else {
      // Browser doesn't support Geolocation
      this.handleLocationError(false, infoWindow, map.getCenter());
    }
  }
  
  handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
                          'Error: The Geolocation service failed.' :
                          'Error: Your browser doesn\'t support geolocation.');
    infoWindow.open(this.map);
  }


  checkLocation(){
    
    this.platform.ready().then((readySource) => {
        this.diagnostic.isLocationEnabled().then(
        (isAvailable) => {

            if(isAvailable === false){
              this.presentConfirm();
            }

            // console.log('Is available? ' + isAvailable);
            // //this.presentConfirm();
            // alert('Is available? ' + isAvailable);
        }).catch( (e) => {
            console.log(e);
            alert(JSON.stringify(e));
        });
    });
}

  presentConfirm() {
    let alert = this.alertCtrl.create({
      message: 'To continue, turn on device location, which uses Google location service.',
      buttons: [
        {
          text: 'No Thanks',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'OK',
          handler: () => {
            console.log('Buy clicked');
          }
        }
      ]
    });
    alert.present();
  }
  
  searchForm(){
    console.log('form', this.todo.value);
    //this.appCtrl.getRootNav().push('SearchPage', {location : this.todo.value.current_location , desc : this.todo.value.item_description});
    //this.navCtrl.setRoot('SearchPage', {location : this.todo.value.current_location , desc : this.todo.value.item_description});
    //this.navCtrl.push('SearchPage', {location : this.todo.value.current_location , desc : this.todo.value.item_description});
    this.navCtrl.push('SearchPage', {location : this.todo.value.current_location , desc : this.todo.value.item_description});
  }

  initMap() {
    console.log("Location Loads!")
    this.merchantProvider.newMerchant().subscribe(
      res => {


        let resultset: any = res;
        this.map_merchants = resultset.data;
        let merchant_name = this.map_merchants.map(t=>t.merchant_name);
        let long = this.map_merchants.map(x=>x.longitude);
        let lat = this.map_merchants.map(y=>y.latitude);
        let img = this.map_merchants.map(z=>z.url)
        //let locations = long.length;

        console.log('name', merchant_name);
        console.log('long', long);
        console.log('lat', lat);
        console.log('img', img);

        this.geolocation.watchPosition().subscribe(position => {
          console.log(position.coords.longitude + ' ' + position.coords.latitude);
          this.y = position.coords.longitude;
          this.x = position.coords.latitude;
       

        for(i = 0; i < long.length; i++){
          this.locations[i] = [merchant_name[i], lat[i], long[i], img[i]];
        }

        console.log('loc loc', this.locations);

          //************************map start****************//
        
          var map = new google.maps.Map(document.getElementById('map'),{
            zoom: 15,
            mapTypeControl: false,
            zoomControl: false,
            streetViewControl: false,
            center: new google.maps.LatLng(this.x, this.y),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            styles: [
              {
                "featureType": "administrative.land_parcel",
                "elementType": "labels",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "poi",
                "elementType": "labels.text",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "poi.business",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "road",
                "elementType": "labels.icon",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "road.local",
                "elementType": "labels",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "transit",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
            ]
          });
       
        var infowindow = new google.maps.InfoWindow();
        
        var marker, i;

        for (i = 0; i < this.locations.length; i++) {
      
          var set_img =   {
            url: this.locations[i][3],
            scaledSize: new google.maps.Size(30, 30),
            // origin: new google.maps.Point(0, 0),
            // anchor: new google.maps.Point(32,65),
            labelOrigin: new google.maps.Point(40,33),
            // labelOrigin: new google.maps.Point(40,33)
        
          }
         
          var marker = new google.maps.Marker({
            
            position: new google.maps.LatLng(this.locations[i][1], this.locations[i][2]),
            // animation: google.maps.Animation.DROP,
            map: map,
            label: {
              text: this.locations[i][0],
              // color: 'white' 
              fontSize: "14px",
              labelClass: "map-marker-label",
            },
            icon: set_img
            
          });

          google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
              infowindow.setContent(this.locations[i][0]);
              infowindow.open(map, marker);
            }
          })(marker, i));
        } 

        
        marker = new google.maps.Marker({
          position: new google.maps.LatLng(this.x, this.y),
          map: map,
          icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png',
          // label: 'testing'
          // icon: {
          //   fillColor: 'green',
          // }
        });
        

        //************************map end****************//
   
        console.log("location", this.map_merchants);
      }, error => {
        console.log(error);
    });


  });
}

goBack(){
  this.appCtrl.getRootNav().push('HomePage');
}

}
