import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Table2Page } from './table2';

@NgModule({
  declarations: [
    Table2Page,
  ],
  imports: [
    IonicPageModule.forChild(Table2Page),
  ],
})
export class Table2PageModule {}
