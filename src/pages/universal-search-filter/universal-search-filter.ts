import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App, LoadingController } from 'ionic-angular';
import { FormGroup, FormControl, Validators, FormBuilder, NgControl } from "@angular/forms";

/**
 * Generated class for the UniversalSearchFilterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-universal-search-filter',
  templateUrl: 'universal-search-filter.html',
})
export class UniversalSearchFilterPage {
  public saveUsername:boolean;
  formGroup: any;
  categories: any [];
  newmessage: any;
  tradeCategories: any = [];
  cat: any;
  selectedArray: any = [];

  constructor(
    public navCtrl: NavController,
    public appCtrl: App,  
    private fb: FormBuilder,
    private loadingCtrl: LoadingController,
    public navParams: NavParams) {

      this.formGroup = this.fb.group({
        location:[''],
        tc:['false']
      });

      console.log('form-val', this.formGroup.value);

  }


    public onSaveUsernameChanged(value:boolean){
        this.cat = value;
    }

    selectMember(data){
     
      if (data.checked == true) {
         this.selectedArray.push(data.id);
      } 
      if (data.checked == false) 
      {
        this.selectedArray = this.selectedArray.filter(item => item !== data.id);
      // let newArray = delete this.selectedArray [data.trade_name];
        // this.selectedArray = newArray;
      }
  
      let newLL = []
      newLL.push(this.selectedArray)
      console.log("category return", newLL)
      localStorage.setItem('categorize', this.selectedArray);
    }

    filterForm(event: any, category, data) {

    localStorage.getItem('cat');
    
    console.log("select array", this.selectedArray)
    let newLL2 = []
    newLL2.push(this.selectedArray)
    console.log("newLL", newLL2)

    localStorage.setItem('key', this.formGroup.value.location);

    console.log('sa', this.formGroup.value.location);

    
      // this.navCtrl.push('VoucherModalPage', 
      // {voucher_id: voucher_id});
  

    this.navCtrl.push('UniversalSearchPage',{
      origin: "location",
      test: this.formGroup.value.location
      });

    // this.appCtrl.getRootNav().push('UniversalSearchPage',{
    //     origin: "location",
    //     location: this.formGroup.value.location
    // });

    // this.newmessage = '';
    this.formGroup.value.location  = '';

    //this.navCtrl.push('VoucherFabPage');

    // let loader = this.loadingCtrl.create({
    //   content: 'Loading Page ... ',
    // });

    // loader.present();

    // setTimeout(() => {

    //   this.navCtrl.push('PromoSearchResultPage', 
    //   {location: this.formGroup.value.location});

    // }, 1000);

    // setTimeout(() => {
    //   loader.dismiss();
    // }, 3000);


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FilterPage');
  }

  onInput(ev: any) {
    // Reset items back to all of the items
    //this.initializeItems();

    // set val to the value of the searchbar
    let val = ev.target.value;

    console.log('val: ', val);

    // if the value is an empty string don't filter the items
    // if (val && val.trim() != '') {
    //   this.items = this.items.filter((item) => {
    //     return (item.storeName.toLowerCase().indexOf(val.toLowerCase()) > -1);
    //   })
    // }

  }

  // loadCategory(){
  //   console.log("Category Loads!");
  //   this.categoryProvider.getCategoryList().subscribe(
  //     res => {
  //       let resultset: any = res;
  //       this.categories = [];

  //       this.categories = resultset.data;
  //       console.log("categories", this.categories);
  //     }, error => {
  //       console.log(error);
  //   });
  // }


  goBack() {
    this.navCtrl.pop();
  }

  navigateTo(page:string){
    this.appCtrl.getRootNav().push(page);
  }

  openModal(){
    this.navCtrl.push('PromoPage');
  }

}