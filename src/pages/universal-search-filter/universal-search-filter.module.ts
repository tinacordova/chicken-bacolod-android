import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UniversalSearchFilterPage } from './universal-search-filter';

@NgModule({
  declarations: [
    UniversalSearchFilterPage,
  ],
  imports: [
    IonicPageModule.forChild(UniversalSearchFilterPage),
  ],
})
export class UniversalSearchFilterPageModule {}
