import { Component } from '@angular/core';
import { AlertController } from 'ionic-angular';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Storage } from '@ionic/storage';


import { UserProvider } from '../../providers/user/user';

import { ENV } from '../../environment/environment';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  private url = ENV.apiUrl;
  public user: any;
  private loginStatus: any;
  private loginMessage: any;

  localStorageUserId: string;
  localStorageFirstName: string;
  localStorageLastName: string;


  registrationForm = this.fb.group({
    mobile: ['', Validators.required],
    password: ['', Validators.required]
  });

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private userProvider: UserProvider,
    public alertCtrl: AlertController,
    private fb: FormBuilder,
    private storage: Storage,
    public appCtrl: App,
    ) {
  }

  ionViewDidLoad() {

    this.localStorageUserId = localStorage.getItem('user_id');
    this.localStorageFirstName = localStorage.getItem('user_firstname');
    this.localStorageLastName = localStorage.getItem('user_lastname');

    this.resetLocalStorage();
    console.log('ionViewDidLoad LoginPage');
  }

  resetLocalStorage(){
    this.storage.clear();
  }

  onLogin() {

    console.log('form :', this.registrationForm.value.mobile);

    let data = {
      "mobile": this.registrationForm.value.mobile,
      "password": this.registrationForm.value.password
    };

    this.userProvider.validateUser(data).subscribe
    (res => {
      let resultset: any = res;
      this.user = resultset.data;
      this.loginStatus = resultset.status;
      this.loginMessage = resultset.message;


      if(this.loginStatus == 'success'){  
        localStorage.setItem('user_id', this.user[0].id);
        localStorage.setItem('user_firstname', this.user[0].firstname);
        localStorage.setItem('user_lastname', this.user[0].lastname)

        this.storage.set('userID', this.user[0].id);
        this.storage.set('merchantID', 26);
        this.storage.set('firstName', this.user[0].firstname);
        this.storage.set('lastName', this.user[0].lastname)

        //This is for universal search product: false use endpoint 1, true use endpoint 2
        localStorage.setItem('uni_search', 'false');

        //this.navCtrl.push('HomePage');
        this.navCtrl.push('BookstorePage');
        

      }else{
        console.log('ERR');
        this.presentAlert(this.loginMessage);
      }

    }), error => {
      console.log(error);
    }
    
  }


  presentAlert(msg) {
    let alert = this.alertCtrl.create({
      title: 'Invalid Login',
      subTitle: msg,
      buttons: ['Dismiss']
    });
    alert.present();
  }

}
