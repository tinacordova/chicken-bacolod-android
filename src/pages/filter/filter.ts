import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App, LoadingController } from 'ionic-angular';
import { FormGroup, FormControl, Validators, FormBuilder, NgControl, FormArray } from "@angular/forms";
import { CategoryProvider } from '../../providers/category/category';
import { PromoSearchProvider } from '../../providers/promo-search/promo-search';

@IonicPage()
@Component({
  selector: 'page-filter',
  templateUrl: 'filter.html',
})
export class FilterPage {

  formGroup:FormGroup;
  categories: any [];
  newmessage: any;
  category: any;
  cat_name: any;
  selected: any;
  checkedItems: any = [];

  selectedArray: any = [];
  newArray: any = [];

  isChecked: boolean = true;

  data: any;

  constructor(
    public navCtrl: NavController,
    public appCtrl: App,  
    private fb: FormBuilder,
    private loadingCtrl: LoadingController,
    public categoryProvider: CategoryProvider,
    public navParams: NavParams,
    public promoSearchProvider: PromoSearchProvider) {

      this.formGroup = this.fb.group({
        location:[''],
        tc:['false']
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FilterPage');
    this.loadCategory();

    this.newmessage = '';
    this.formGroup.value.location = '';

    console.log(this.selectedArray)

  }

  selectMember(data){
    if (data.checked == true) {
       this.selectedArray.push(data.id);
    } 
    if (data.checked == false) 
    {
      this.selectedArray = this.selectedArray.filter(item => item !== data.id);
    }

    let newLL = []
    newLL.push(this.selectedArray)
    console.log("category return", newLL)
    localStorage.setItem('categorize', this.selectedArray);
  }

  filterForm(event: any, hehe, category, data) {

    console.log("first", this.formGroup.value.tc, hehe)
    console.log("second", this.formGroup.value.location)
    localStorage.getItem('cat');
    
    console.log("select array", this.selectedArray)
    let newLL2 = []
    newLL2.push(this.selectedArray)
    console.log("newLL", newLL2)

    localStorage.setItem('key', this.formGroup.value.location);

    this.navCtrl.push('PromoPage', 
      {categoryPush: newLL2,
      location: this.formGroup.value.location
      });

    this.newmessage = '';
    this.formGroup.value.location  = '';
  }

  onInput(ev: any) {
    let val = ev.target.value;

    console.log('val: ', val);
  }

  loadCategory(){
    console.log("Category Loads!");
    this.promoSearchProvider.getPromoCategoriesFilter().subscribe(
      res => {
        let resultset: any = res;
        this.categories = [];

        this.categories = resultset.data;
        console.log("categories", this.categories);
      }, error => {
        console.log(error);
    });
  }

  goBack() {
    this.navCtrl.pop();
  }

  navigateTo(page:string){
    this.appCtrl.getRootNav().push(page);
  }

  openModal(){
    this.navCtrl.push('PromoPage');
  }
}