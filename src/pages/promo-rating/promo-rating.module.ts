import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PromoRatingPage } from './promo-rating';

import { IonicRatingModule } from "ionic-rating";


@NgModule({
  declarations: [
    PromoRatingPage,
  ],
  imports: [
    IonicPageModule.forChild(PromoRatingPage),
    IonicRatingModule
  ],
})
export class PromoRatingPageModule {}
