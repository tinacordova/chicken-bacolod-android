import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

import { PromoRatingProvider } from '../../providers/promo-rating/promo-rating';

@IonicPage()
@Component({
  selector: 'page-promo-rating',
  templateUrl: 'promo-rating.html',
})
export class PromoRatingPage {

  starRating: number;
  promoId: number;

  promos_rating: any = {};

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private viewCtrl: ViewController,
              public promoRating: PromoRatingProvider) {

    this.promoId = this.navParams.get('promo_id');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PromoRatingPage');
  }

  dismiss(){
    this.viewCtrl.dismiss()
  }

  cancel(){
    this.viewCtrl.dismiss()
  }

  onModelChange($event){
    console.log($event)
    // this.rate = 5
    this.starRating = $event
  }

  submit(){
    let newDate = new Date().toISOString().slice(0, 19).replace('T', ' ');
    console.log("rating: ", this.starRating, "promo id: ", this.promoId, "date: ", newDate)

    this.promoRating.sendPromoRating(this.promoId, 1, this.starRating).subscribe(
      res => {
        let resultset: any = res;
        this.promos_rating = resultset.data;
        this.viewCtrl.dismiss()
      }), error => {
        console.log(error);
      }
  }

}
