import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App, LoadingController, UrlSerializer } from 'ionic-angular';
import { FormGroup, FormControl, Validators, FormBuilder, NgControl } from "@angular/forms";
import { VoucherProvider }  from '../../providers/voucher/voucher';
import { SpecialOfferProvider }  from '../../providers/special-offer/special-offer';

/**
 * Generated class for the SpecialOfferFilterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-special-offer-filter',
  templateUrl: 'special-offer-filter.html',
})
export class SpecialOfferFilterPage {

  public saveUsername:boolean;
  formGroup: any;
  categories: any [];
  newmessage: any;
  tradeCategories: any = [];
  cat: any;
  selectedArray: any = [];

  constructor(
    public navCtrl: NavController,
    public appCtrl: App,  
    private fb: FormBuilder,
    private loadingCtrl: LoadingController,
    public voucherProvider: VoucherProvider,
    public specialOfferProvider: SpecialOfferProvider,
    public navParams: NavParams) {

      this.formGroup = this.fb.group({
        location:[''],
        tc:['false']
      });

      console.log('form-val', this.formGroup.value);

  }


    public onSaveUsernameChanged(value:boolean){
        this.cat = value;
    }

    selectMember(data){
     
      if (data.checked == true) {
         this.selectedArray.push(data.id);
      } 
      if (data.checked == false) 
      {
        this.selectedArray = this.selectedArray.filter(item => item !== data.id);
      // let newArray = delete this.selectedArray [data.trade_name];
        // this.selectedArray = newArray;
      }
  
      let newLL = []
      newLL.push(this.selectedArray)
      console.log("category return", newLL)
      localStorage.setItem('categorize', this.selectedArray);
    }

    filterForm(event: any, category, data) {

    localStorage.getItem('cat');
    
    console.log("select array", this.selectedArray)
    let newLL2 = []
    newLL2.push(this.selectedArray)
    console.log("newLL", newLL2)

    localStorage.setItem('key', this.formGroup.value.location);

    this.navCtrl.push('SpecialOfferPage', 
      {categoryPush: newLL2,
      location: this.formGroup.value.location
      });

    this.newmessage = '';
    this.formGroup.value.location  = '';

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FilterPage');
    this.loadTradeCategory();
  }

  onInput(ev: any) {
    // Reset items back to all of the items
    //this.initializeItems();

    // set val to the value of the searchbar
    let val = ev.target.value;

    console.log('val: ', val);

    // if the value is an empty string don't filter the items
    // if (val && val.trim() != '') {
    //   this.items = this.items.filter((item) => {
    //     return (item.storeName.toLowerCase().indexOf(val.toLowerCase()) > -1);
    //   })
    // }

  }

  // loadCategory(){
  //   console.log("Category Loads!");
  //   this.categoryProvider.getCategoryList().subscribe(
  //     res => {
  //       let resultset: any = res;
  //       this.categories = [];

  //       this.categories = resultset.data;
  //       console.log("categories", this.categories);
  //     }, error => {
  //       console.log(error);
  //   });
  // }

  loadTradeCategory(){
    this.specialOfferProvider.getTradeCategoryItems().subscribe(
      res => {
        let resultset: any = res;

        //display all the category with product inside
        this.tradeCategories = resultset.data;

        console.log('trade categories', this.tradeCategories);

      }, error => {
        console.log(error);
    });
  }

  goBack() {
    this.navCtrl.pop();
  }

  navigateTo(page:string){
    this.appCtrl.getRootNav().push(page);
  }

  openModal(){
    this.navCtrl.push('PromoPage');
  }

}
