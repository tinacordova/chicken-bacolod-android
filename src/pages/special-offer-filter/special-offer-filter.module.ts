import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SpecialOfferFilterPage } from './special-offer-filter';

@NgModule({
  declarations: [
    SpecialOfferFilterPage,
  ],
  imports: [
    IonicPageModule.forChild(SpecialOfferFilterPage),
  ],
})
export class SpecialOfferFilterPageModule {}
