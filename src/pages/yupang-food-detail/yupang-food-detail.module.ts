import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { YupangFoodDetailPage } from './yupang-food-detail';

@NgModule({
  declarations: [
    YupangFoodDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(YupangFoodDetailPage),
  ],
})
export class YupangFoodDetailPageModule {}
