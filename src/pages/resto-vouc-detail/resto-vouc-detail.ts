import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';

/**
 * Generated class for the RestoVoucDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-resto-vouc-detail',
  templateUrl: 'resto-vouc-detail.html',
})
export class RestoVoucDetailPage {

  constructor(
    public navCtrl: NavController, 
    public appCtrl: App,
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RestoVoucDetailPage');
  }

  goBack() {
    this.navCtrl.pop();
  }

}
