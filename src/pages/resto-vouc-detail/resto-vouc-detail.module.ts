import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RestoVoucDetailPage } from './resto-vouc-detail';

@NgModule({
  declarations: [
    RestoVoucDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(RestoVoucDetailPage),
  ],
})
export class RestoVoucDetailPageModule {}
