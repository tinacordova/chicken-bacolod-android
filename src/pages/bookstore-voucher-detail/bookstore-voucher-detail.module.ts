import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BookstoreVoucherDetailPage } from './bookstore-voucher-detail';

@NgModule({
  declarations: [
    BookstoreVoucherDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(BookstoreVoucherDetailPage),
  ],
})
export class BookstoreVoucherDetailPageModule {}
