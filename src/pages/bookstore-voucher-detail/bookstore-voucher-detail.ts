import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the BookstoreVoucherDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-bookstore-voucher-detail',
  templateUrl: 'bookstore-voucher-detail.html',
})
export class BookstoreVoucherDetailPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BookstoreVoucherDetailPage');
  }

  goBack() {
    this.navCtrl.pop();
  }

}
