import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RestoPromoDetailPage } from './resto-promo-detail';

@NgModule({
  declarations: [
    RestoPromoDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(RestoPromoDetailPage),
  ],
})
export class RestoPromoDetailPageModule {}
