import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';

/**
 * Generated class for the RestoPromoDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-resto-promo-detail',
  templateUrl: 'resto-promo-detail.html',
})
export class RestoPromoDetailPage {

  constructor(
    public navCtrl: NavController, 
    public appCtrl: App,
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RestoPromoDetailPage');
  }

  goBack() {
    this.navCtrl.pop();
  }

}
