import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BookstorePage } from './bookstore';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    BookstorePage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(BookstorePage),
  ],
})
export class BookstorePageModule {}
