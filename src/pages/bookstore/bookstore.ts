import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, App, LoadingController, ModalController, ToastController, MenuController, Content } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { MerchantProvider } from '../../providers/merchant/merchant';
import { PromoProvider } from '../../providers/promo/promo';
import { VoucherProvider } from '../../providers/voucher/voucher';
import { ProductProvider } from '../../providers/product/product';
import { CartProvider } from '../../providers/cart/cart';

//import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

import { Observable } from "rxjs";
import { debounceTime, distinctUntilChanged } from "rxjs/operators";

/**
 * Generated class for the BookstorePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-bookstore',
  templateUrl: 'bookstore.html',
})
export class BookstorePage {
  @ViewChild(Content) content: Content;

  merchants: any = [];
  promos: any = [];
  vouchers: any = [];
  products: any = [];
  merchant_ads: any = [];
  specialOffers: any = [];
  prodCategories: any = [];
  orders: any[];
  cartcounter: any [];
  counterFront: number;
  newcount: number;
  counter : number = 1;
  count_order: any [];
  userID: number;
  cartID: number;
  merchantID: number;
  main: any;
  cart_id: number;
  tableNumber: number;
  cartQty: number;

  loopStatus: Boolean = false;

  loopStatusVoucher: Boolean = false;

  localStorageUserId: string;
  firstName: string;
  lastName: string;

  my_id: number;
  my_amt: number;
  table_num: any;

  resCount: number;
  clearStatus: Boolean = false;
  data: any;

  searchChangeObserver;

  constructor(
    public navCtrl: NavController, 
    public appCtrl: App,
    public menuCtrl: MenuController,
    public merchantProvider: MerchantProvider,
    public promoProvider: PromoProvider,
    public voucherProvider: VoucherProvider,
    public productProvider: ProductProvider,
    private loadingCtrl: LoadingController,
    public cartProvider: CartProvider,
    public toastCtrl: ToastController, 
    public modalCtrl: ModalController,
    private storage: Storage,
    //private sqlite: SQLite,
    public navParams: NavParams) {

      this.storage.set('merchantID', 38);

      // menuCtrl.enable(true);

      // localStorage.setItem('key', "");

      this.storage.get('firstName').then((val)=>{
        this.firstName = val;
        console.log('fname', this.firstName);
      });

      this.storage.get('lastName').then((val)=>{
        this.lastName = val;
        console.log('lname', this.lastName);
      });

  }


  // loadCreateTable(){
  //   this.sqlite.create({
  //     name: 'sample.db',
  //     location: 'default'
  //   }).then((db: SQLiteObject) => {
  //     db.executeSql('CREATE TABLE IF NOT EXISTS cart_detail('+
  //                     'cart_detail_id INTEGER PRIMARY KEY,' +
  //                     'cart_id INTEGER,' + 
  //                     'product_id INTEGER,' +
  //                     'prod_addin_datetime TEXT,' +
  //                     'prod_modify_datetime TEXT,' +
  //                     'qty INTEGER,' +
  //                     'unit_price INTEGER,' + 
  //                     'sub_total INTEGER)', [])
  //     .then(res => {
  //       console.log('Executed SQL')
  //       let toast = this.toastCtrl.create({
  //         message: ("OK" + res),
  //         duration: 3000, 
  //         position: 'top'
  //       });
      
  //       toast.onDidDismiss(() => {
  //         console.log('Dismissed toast');
  //       });
      
  //       toast.present();
  
  //     })
  //     .catch(e => {
  //       console.log(e)
  
  //       let toast = this.toastCtrl.create({
  //         message: ("not ok" + e),
  //         duration: 3000,
  //         position: 'top'
  //       });
      
  //       toast.onDidDismiss(() => {
  //         console.log('Dismissed toast');
  //       });
      
  //       toast.present();
  //       //this.loadInsert()
  //     });
  //     })
  
  //   }

  openFirst() {
    this.menuCtrl.enable(true, 'first');
    this.menuCtrl.open('first');
  }


  ionViewDidEnter(){
    this.getCartID();
  }

  ionViewDidLoad() {
    this.getCartID();
    this.loadShopInfo();
    this.loadMerchantAds();
    this.loadProdSpecialOffer();
    this.loadMerchantPromo();
    this.loadMerchantVoucher();
    this.loadProductMerchantCategory();
  }

  getCartID(){

    this.storage.get('userID').then((value)=>{
      this.userID = value;

      console.log('user_id', this.userID);
  
      this.storage.get('merchantID').then((value)=>{
        this.merchantID = value;

        console.log('merchant_id', this.merchantID);

        this.storage.get('tableNumber').then((value)=>{
          this.tableNumber = value;

          this.cartProvider.cartMain(this.userID, this.merchantID, 0).subscribe(
            res => {
              let resultset: any = res;
              this.main = resultset.data;
              console.log("main", this.main);
              console.log("cartid", this.main[0].cart_id);
              console.log("mainid", this.main[0].ipayu_id);

              this.loadCartDetailCounter(this.main[0].cart_id);
      
              this.cart_id = this.main[0].cart_id      
              this.storage.set('cartID', this.cart_id);
      
            }, error => {
              console.log(error);
          });
        
        });
      
      });
    
    });
  }

  loadShopInfo(){

    this.storage.get('merchantID').then((val)=>{
      this.merchantID = val;

      this.merchantProvider.merchantInfo(this.merchantID).subscribe(
        res => {
          let resultset: any = res;
          this.merchants = resultset.data;
  
          console.log("merchants", this.merchants);
        }, error => {
          console.log(error);
      });
    
    });
  }

  loadMerchantPromo(){
    this.storage.get('merchantID').then((val)=>{
      this.merchantID = val;

      this.promoProvider.getMerchantPromo(this.merchantID).subscribe(
        res => {
          let resultset: any = res;
          this.promos = resultset.data;
  
          console.log("merchant promos", this.promos);     
        }, error => {
          console.log(error);
      });
  
    });

  }

  loadMerchantVoucher(){

    // this.storage.get('merchantID').then((value)=>{
    //   this.merchantID = value;

    //   this.voucherProvider.getMerchantVoucher(this.merchantID).subscribe(
    //     res => {
    //       let resultset: any = res;
    //       this.vouchers = resultset.data;

    //       if(this.vouchers.length >= 3){
    //         this.loopStatusVoucher = true;
    //       }else{
    //         this.loopStatusVoucher = false;
    //       }
          
    //       console.log("merchant vouchers", this.vouchers);     
    //     }, error => {
    //       console.log(error);
    //   });
    
    // });

    // this.storage.get('merchantID').then((value)=>{
    //   this.merchantID = value;

    //   this.voucherProvider.getMerchantVoucher(this.merchantID).subscribe(
    //     res => {
    //       let resultset: any = res;
    //       this.vouchers = resultset.data;
          
    //       console.log("merchant vouchers", this.vouchers);     
    //     }, error => {
    //       console.log(error);
    //   });
    
    // });

    this.storage.get('merchantID').then((value) => {
      this.merchantID = value;
    
      
    let data = {
      "merchant_id":this.merchantID
    };

    this.voucherProvider.getVoucherMerchantCategoryItems(data).subscribe(
      res => {
        let resultset: any = res;
        this.vouchers = resultset.data;

          if(this.vouchers.length >= 3){
            this.loopStatusVoucher = true;
          }else{
            this.loopStatusVoucher = false;
          }
        
      },
      error => {
        console.log(error);
      }
    );
    
    });

  }

  loadProductMerchantCategory(){
    this.storage.get('merchantID').then((value)=>{
      this.merchantID = value;

      this.productProvider.getProductMerchantCategory(this.merchantID).subscribe(
        res => {
          let resultset: any = res;
          this.prodCategories = resultset.data;
  
        }, error => {
          console.log(error);
      });
    });
  }

  loadCartDetailCounter(cartID){

        this.storage.get('merchantID').then((value)=>{
          this.merchantID = value;

            this.cartProvider.getCartDetailCounter(cartID, this.merchantID).subscribe(
              res => {
                let resultset: any = res;
                this.cartcounter = resultset.data;
                
                console.log("counter", this.cartcounter[0].detail_counter);
                this.counterFront = this.cartcounter[0].detail_counter
        
                if (this.cartcounter[0].detail_counter == 0)
                this.counterFront + 1

                this.storage.set('itemQuantity', this.counterFront);

                this.storage.get('itemQuantity').then((val) => {

                  if(val != 0){
                    this.cartQty = val;
                  }else{
                    this.cartQty = 0;
                  }
                
                });

                
              }, error => {
                console.log(error);
            });

        });
  }


  openCart(){
    this.appCtrl.getRootNav().push('MenuOrderingPage');
  }
  
  loadProdSpecialOffer(){
    this.storage.get('merchantID').then((val) => {
      this.merchantID = val;

        this.productProvider.getSpecialOffer(this.merchantID).subscribe(
          res => {
            let resultset: any = res;
            this.specialOffers = [];
            this.specialOffers = resultset.data;

            if(this.specialOffers.length >= 3){
              this.loopStatus = true;
            }else{
              this.loopStatus = false;
            }
    
            console.log("Special Offer 123", this.specialOffers);  
            console.log("Special Offer length", this.specialOffers.length);     
          }, error => {
            console.log(error);
        });

    });

  }

  loadMerchantAds(){

    this.storage.get('merchantID').then((val) => {
      this.merchantID = val;

      let data = {
        "merchant_id": this.merchantID
      };
  
      this.merchantProvider.getMerchantAds(data).subscribe(
        res => {
          let resultset: any = res;
          this.merchant_ads = resultset.data;
  
          console.log("merchant ads", this.merchant_ads);     
        }, error => {
          console.log(error);
      });
  
    });
  }

  doRefresh(refresher) {
    console.log("Begin async operation", refresher);
    this.getCartID();
    this.loadShopInfo();
    this.loadMerchantAds();
    this.loadProdSpecialOffer();
    this.loadMerchantPromo();
    this.loadMerchantVoucher();
    this.loadProductMerchantCategory();

    setTimeout(() => {
      console.log("Async operation has ended");
      refresher.complete();
    }, 2000);
  }

  goPromo(){
    this.appCtrl.getRootNav().push('BookstorePromoDetailPage');
  }

  goNext(){
    this.appCtrl.getRootNav().push('BookstoreVoucherDetailPage');
  }

  openMenuDetail(){
    this.appCtrl.getRootNav().push('BookstoreBooksDetailPage');
  }

  openSpecialDetail(prod_id){

    let loader = this.loadingCtrl.create({
      content: 'Loading Page ... ',
    });

    loader.present();

    setTimeout(() => {

      this.navCtrl.push('BookstoreBooksDetailPage', 
      {prod_id: prod_id});

    }, 1000);

    setTimeout(() => {
      loader.dismiss();
    }, 3000);

  }

  openBookDetail(prod_id){

    let loader = this.loadingCtrl.create({
      content: 'Loading Page ... ',
    });

    loader.present();

    setTimeout(() => {

      this.navCtrl.push('BookstoreDetailPage', 
      {prod_id: prod_id});

    }, 1000);

    setTimeout(() => {
      loader.dismiss();
    }, 3000);

  }

  openPromoDetail(promo_id) {
    let loader = this.loadingCtrl.create({
      content: "Loading Page ... "
    });

    loader.present();

    setTimeout(() => {
      this.navCtrl.push("PromoModalPage", { promo_id: promo_id });
    }, 1000);

    setTimeout(() => {
      loader.dismiss();
    }, 3000);
  }

  add(pid: number, i){
    this.newcount = i
    console.log('add', pid, this.newcount)
    this.counter += 1;
    
  }

  remove(pid: number, i){
    console.log('remove', pid, i)
    if(this.counter > 1){
      this.counter -= 1;
    }
  }

  addtoOrder(id: number, amt: number){

    //this.table_num = localStorage.getItem('table_number')

    this.storage.get('userID').then((val) => {
      this.userID = val;
    

      this.storage.get('merchantID').then((value) => {
        this.merchantID = value;

        this.cartProvider.cartCounter(this.merchantID).subscribe(
          res => {
            let resultset: any = res;
            this.count_order = resultset.data;
            console.log("first", this.count_order);

       
                this.cartProvider.postCart(this.userID, 1, 0, id, this.counter, amt, this.merchantID).subscribe(
                  res => {
                    let resultset: any = res;
                    this.orders = resultset.data;
                    console.log("orders", this.orders);
        
                  }, error => {
                    console.log(error);
                });
    
                this.cartProvider.cartMain(this.userID, this.merchantID, 0).subscribe(
                  res => {
                    let resultset: any = res;
                    this.main = resultset.data;
                    console.log("main", this.main);
                    console.log("cartid", this.main[0].cart_id);
                    console.log("mainid", this.main[0].ipayu_id);

                    this.storage.set('cartID', this.main[0].cart_id);
            
                  }, error => {
                    console.log(error);
                });
                
                const toast = this.toastCtrl.create({
                  message: 'Order saved in the cart.',
                  duration: 2000
                });
      
                toast.present();
      
                //this.navCtrl.setRoot(this.navCtrl.getActive().component);       
                this.navCtrl.push('BookstorePage');     
    
          }, error => {
            console.log(error);
        });


      
      });

    });
  }

  navigateToLogin(){
    this.storage.clear();
    this.appCtrl.getRootNav().push('LoginPage');
  }

  navigateTo(page: string) {
    this.appCtrl.getRootNav().push(page);
  }

  onSearchChange(searchValue: string) {
    if (!this.searchChangeObserver) {
      Observable.create(observer => {
        this.searchChangeObserver = observer;
      })
        .pipe(debounceTime(1500)) // wait 300ms after the last event before emitting last event
        .pipe(distinctUntilChanged()) // only emit if value is different from previous value
        .subscribe(res => {
          this.resCount = res.length;

          if (
            this.resCount > 0 &&
            res.length > 0 &&
            this.clearStatus === false
          ) {
            this.navCtrl.push("SearchResultPage", {
              origin: "bookstore",
              keyword: res
            });
          }

          this.clearStatus = false;
        });
    }

    this.searchChangeObserver.next(searchValue);
  }

  onClear(event: any) {
    //This is a marker that the clear search was clicked (the x button)
    this.clearStatus = true;
  }

  onChange(event: any) {
    //Get the string length of the search bar and assign to resCount
    this.resCount = event.target.value.length;
  }

  toWallet() {
    this.appCtrl.getRootNav().push("WalletFabPage");
  }
  
  toProduct() {
    this.appCtrl.getRootNav().push("SpecialOfferPage");
  }

  toPromo() {
    this.appCtrl.getRootNav().push("PromoPage");
  }

  toResto() {
    this.appCtrl.getRootNav().push("VoucherFabPage");
  }

  navScreen(page:string) {
    this.appCtrl.getRootNav().push(page);
  }

  openVoucherDetail(voucher_id) {
    let loader = this.loadingCtrl.create({
      content: "Loading Page ... "
    });

    loader.present();

    setTimeout(() => {
      this.navCtrl.push("VoucherModalPage", { voucher_id: voucher_id });
    }, 1000);

    setTimeout(() => {
      loader.dismiss();
    }, 3000);
  }

}
