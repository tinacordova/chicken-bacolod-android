import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { AlertController } from 'ionic-angular';

import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';

import { DatePicker } from '@ionic-native/date-picker';

import { MerchantProvider } from '../../providers/merchant/merchant';
import { TransactionProvider } from '../../providers/transaction/transaction';
import { ReservationProvider } from '../../providers/reservation/reservation';
import { BranchProvider } from '../../providers/branch/branch';
import { ProductProvider } from '../../providers/product/product';
import { WalletProvider } from '../../providers/wallet/wallet';

import { HttpClient } from '@angular/common/http/'; 
import { HttpHeaders } from '@angular/common/http';

@IonicPage()
@Component({
  selector: 'page-reservation',
  templateUrl: 'reservation.html',
})
export class ReservationPage {
  private reservation : FormGroup;
  theCategories: string = "categories";
  merchants: any = [];
  merchant_ads: any = [];
  merchant_id = parseInt(localStorage.getItem('new_merchant_id'));
  userID = localStorage.getItem('user_id');
  public counter : number = 1;
  newDate: any;
  newTime: any;
  merchantStatus: any;
  merchantMessage: any;
  buttonStatus = true;
  currDate: any;
  fcm_num: any [];
  merchantID: number;
  branchList: any = [];
  productMerchantList: any = [];
  lastInsertedID: number;
  transactions: any;

  constructor(
    public navCtrl: NavController, 
    public appCtrl: App,
    public branchProvider: BranchProvider,
    public navParams: NavParams, 
    private merchantProvider: MerchantProvider,
    private reservationProvider: ReservationProvider,
    private transactionProvider: TransactionProvider,
    private http: HttpClient,
    private datePicker: DatePicker,
    public walletProvider: WalletProvider,
    public productProvider: ProductProvider,
    private storage: Storage,
    private formBuilder: FormBuilder,
    private alertCtrl: AlertController) {

      // this.reservation = this.formBuilder.group({
      //   quantity: ['', Validators.required],
      //   reservation_date: ['', Validators.required],
      //   reservation_time: ['', Validators.required]
      // });

      this.reservation = this.formBuilder.group({
        firstname: ['', Validators.required],
        lastname: ['', Validators.required],
        product_id: ['', Validators.required],
        branch_id: ['', Validators.required]
      });

      this.currDate = new Date();

  }


  public pad(n){
    return n<10 ? '0'+n : n
  }

  optionsFn(){
    console.log('prod_cat_id', )
  }

  transactionAppointment(txn_type:string){

    this.storage.get('merchantID').then((value)=>{
      this.merchantID = value;
    
        let data = {
          "amount": "0",
          "merchant_id": this.merchantID,
          "user_id": 2,
          "txn_type":txn_type
        };

        this.transactionProvider.addTransactionAppointment(data).subscribe(
          res => {
            let resultset: any = res;  
            this.transactions = resultset.status;
        });

    });

  }

  reservationForm(){    

    console.log(this.reservation.value)

    let year = this.newDate.getFullYear();
    let month = this.pad((this.newDate.getMonth() + 1));
    let day = this.pad(this.newDate.getDate());

    let hour = this.newTime.getHours();
    let min = this.newTime.getMinutes();
    let sec = this.newTime.getSeconds();    

    let dateVal = year + '-' + month + '-' + day + ' ' + hour + ':' + min + ':' + sec;
  
    let data = {
      "appointment_date": dateVal,
      //"appointment_date": '2019-07-16 00:00:00',
      "product_id":this.reservation.value.product_id,
      "branch_id":this.reservation.value.branch_id,
      "firstname":this.reservation.value.firstname,
      "lastname":this.reservation.value.lastname
      }
      
      //Calling the api to insert transaction record with txn_type "PENDING"
      this.transactionAppointment('pending');
      
      this.reservationProvider.createAppointment(data).subscribe(
        res => {

          let resultset: any = res;         
          
          this.merchants = [];
          this.merchantStatus = '';

          this.merchantStatus = resultset.status;
          this.merchantMessage = resultset.message;
          this.lastInsertedID = resultset.data.last_inserted_id;

          console.log('meme', this.lastInsertedID);

          if(this.merchantStatus == 'success'){
            this.sendNotification(this.lastInsertedID);

            console.log('last inserted id', this.lastInsertedID);
            
            //Clear the input fields
            this.clearInput();

            this.successAlert();
          }else{
            this.errorAlert(this.merchantMessage);
          }
  
          //console.log("merchants", this.merchants);
        }, error => {
          this.errorAlert(error);
          console.log(error);
      });
    
  }

  sendNotification(appointmentID){
    
    this.storage.get('merchantID').then((value) => {
    this.merchantID = value;

        this.walletProvider.sendNotif(this.merchantID).subscribe(
          res => {
            let resultset: any = res;
            this.fcm_num = resultset.data;
            console.log("fcm", this.fcm_num[0].fcm_token);
            console.log("merchant id", this.merchantID)

            let body = {
              "notification":{
                "title":"IPAYU Philippines",
                "body":"A customer just redeemed their voucher. Please check it out.",
                "sound":"default",
                "click_action":"FCM_PLUGIN_ACTIVITY",
                "icon":"icon"
              },
              "data":{
                "param1":"appointment",
                "param2": appointmentID
              },
              "to": this.fcm_num[0].fcm_token,
              "priority":"high",
              "restricted_package_name":""
            }

            let options = new HttpHeaders().set('Content-Type','application/json');
            this.http.post("https://fcm.googleapis.com/fcm/send",body,{
              headers: options.set('Authorization', 'key=AAAAPi0wxFY:APA91bHZhmnNW0Axfc5JaEr72jjDu80IFtrOozXao-uUp_qHKs5OnMxo_BIE2z84lL0MShow1aC8XYA-w6XRytTEAWHk-KOFNI-VE3JNmBDktA4IsRQ82VEERMGz6FpR7dfPEB4-Uqga'),
            }).subscribe();
            
            // const alert = this.alertCtrl.create({
            //   title: "Congratulations!",
            //   subTitle: "Voucher is ready to use.",
            //   buttons: [
            //     {
            //       text: 'OK',
            //       handler: () => {
            //       }
            //     }
            //   ],
            //   cssClass: 'alertCustomCss'
            // });
            // alert.present()
          
          }), error => {
            console.log(error);
          }

        });

  }

  ionViewDidLoad() {
    this.loadMerchantAds();
    this.loadShopInfo();
    this.loadBranchList();
    this.loadProductMerchantList();
    console.log('ionViewDidLoad ReservationPage');
  }

  loadBranchList(){
    this.storage.get('merchantID').then((val)=>{
      this.merchantID = val;

      this.branchProvider.getBranchList(this.merchantID).subscribe(
        res => {
          let resultset: any = res;
          this.branchList = resultset.data;
  
          console.log("merchant ads", this.merchant_ads);     
        }, error => {
          console.log(error);
      });
      
    });
  }

  loadProductMerchantList(){
    this.storage.get('merchantID').then((val)=>{
      this.merchantID = val;

      let data = {
        "merchant_id":this.merchantID
      };

      this.productProvider.getProductMerchantList(data).subscribe(
        res => {
          let resultset: any = res;
          this.productMerchantList = resultset.data;

        }, error => {
          console.log(error);
      });
      
    });
  }

  successAlert() {
    let alert = this.alertCtrl.create({
      title: 'Success',
      subTitle: 'Appointment has been successfully added.',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            alert.dismiss();            
            this.appCtrl.getRootNav().push('WalletFabPage');
          }
        }
      ]
    });
    alert.present();
  }

  errorAlert(message:any) {
    let alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }

  goBack() {
    this.navCtrl.pop();
  }

  loadMerchantAds(){

    this.storage.get('merchantID').then((val)=>{
      this.merchantID = val;
   
      let data = {
        "merchant_id":this.merchantID
      };
  
      this.merchantProvider.getMerchantAds(data).subscribe(
        res => {
          let resultset: any = res;
          this.merchant_ads = resultset.data;
  
          console.log("merchant ads", this.merchant_ads);     
        }, error => {
          console.log(error);
      });
      
   
    });

  }

  increment(){
    this.counter += 1;
  }
  
  decrement(){

    if(this.counter > 1){
      this.counter -= 1;
    }
    
  }

  loadShopInfo(){

    this.storage.get('merchantID').then((val)=>{
      this.merchantID = val;
    
      this.merchantProvider.merchantInfo(this.merchantID).subscribe(
        res => {
          let resultset: any = res;
          this.merchants = resultset.data;
  
          console.log("merchant info", this.merchants);
        }, error => {
          console.log(error);
      });
    
    });
  }

  selectTime(){
    this.datePicker.show({
      date: new Date(),
      mode: 'time',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    }).then(
      date => {
        this.newTime = date;
        console.log('Got date: ', date)

        //Enable the button if the date and time is already filled out
        if( (typeof this.newDate.getFullYear() != 'undefined' && this.newDate.getFullYear()) &&  (typeof this.newTime.getHours() != 'undefined' && this.newTime.getHours())) {
          this.buttonStatus = false;
        } 

      },
      err => console.log('Error occurred while getting date: ', err)
    );
  }

  clearInput(){
      //Clear the date end time selection before alert 
      this.newDate = "";
      this.newTime = ""

      //Disable again the book a table button
      this.buttonStatus = true;
  }

  selectDate(){
    this.datePicker.show({
      date: new Date(),
      mode: 'date',
      allowOldDates: false,
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    }).then(
      date => {
        let currentDate = new Date();
        currentDate.setHours(0,0,0,0);
        this.newDate = date;
        console.log('Got date: ', date)

        if(this.newDate.getTime() < currentDate.getTime() ){
          
          //Clear the input Fields
          this.newDate = "";
          this.buttonStatus = true;

          this.errorAlert('Pick a valid Date');
        }

        //Enable the button if the date and time is already filled out
        if( (typeof this.newDate.getFullYear() != 'undefined' && this.newDate.getFullYear()) &&  (typeof this.newTime.getHours() != 'undefined' && this.newTime.getHours())) {
          this.buttonStatus = false;
        } 

      },
      err => console.log('Error occurred while getting date: ', err)
    );
  }


}
