import { Component, ViewChild, ElementRef, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, IonicApp, ToastController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import {Http} from '@angular/http';

import { NativeGeocoder, 
  NativeGeocoderReverseResult, 
  NativeGeocoderForwardResult, 
  NativeGeocoderOptions } from '@ionic-native/native-geocoder';

  let options: NativeGeocoderOptions = {
    useLocale: true,
    maxResults: 1
  };
  
  let map: any;
  let infowindow: any;
  let optionss = {
    enableHighAccuracy: true,
    timeout: 5000,
    maximumAge: 0
  };

declare var require 
var MarkerWithLabel = require('markerwithlabel')(google.maps);
declare var google
var geocoder;

@IonicPage()
@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
})
export class MapPage {

  @ViewChild("search")
  public searchElementRef: ElementRef;
  @ViewChild('map') mapRef: ElementRef;
  map: any;
  x: any;
  y: any;

  merchants: any [];
  map_merchants: any [];
  loc: any = {};
  locations: any = [];
  sample: any = [];
  sample2: any = [];
  address: any = [];
  convert_add: any = [];

  output1: any = [];
  output2: any = [];
  real_merchant: any = [];
  real_product: any = [];
  new_locations: any = [];

  my_search: any [];

  theCategories: string = "delight";

  onLoad: boolean = true;
  onSearch: boolean = false;

  autocompleteService: any;
  placesService: any;
  query: string = '';
  places: any = [];

  Open: boolean = true; 
  
  autocomplete = { input: '' };
  autocompleteItems = [];
  GoogleAutocomplete;

  address_name: string;
  search_loc: string;
  search_item: string

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public geolocation: Geolocation,
    public platform: Platform,
    public ionicApp: IonicApp,
    //public merchantProvider: MerchantProvider,
    public nativeGeocoder: NativeGeocoder,
    public toaster: ToastController,
    public http: Http,
    private ngZone: NgZone
    ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MapPage');
    this.initMap();
  }

  initMap() {


//****************** STATIC CODE ******************/


    this.geolocation.watchPosition().subscribe(position => {
      console.log(position.coords.longitude + ' ' + position.coords.latitude);
      this.y = position.coords.longitude;
      this.x = position.coords.latitude;


      //************************map start****************//

      var map = new google.maps.Map(document.getElementById('map'),{
        zoom: 15,
        mapTypeControl: false,
        zoomControl: false,
        streetViewControl: false,
        center: new google.maps.LatLng(this.x, this.y),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        styles: [
          {
            "featureType": "administrative.land_parcel",
            "elementType": "labels",
            "stylers": [
              {
                "visibility": "off"
              }
            ]
          },
          {
            "featureType": "poi",
            "elementType": "labels.text",
            "stylers": [
              {
                "visibility": "off"
              }
            ]
          },
          {
            "featureType": "poi.business",
            "stylers": [
              {
                "visibility": "off"
              }
            ]
          },
          {
            "featureType": "road",
            "elementType": "labels.icon",
            "stylers": [
              {
                "visibility": "off"
              }
            ]
          },
          {
            "featureType": "road.local",
            "elementType": "labels",
            "stylers": [
              {
                "visibility": "off"
              }
            ]
          },
          {
            "featureType": "transit",
            "stylers": [
              {
                "visibility": "off"
              }
            ]
          },
        ]
      });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

      var set_img =   {
        url: 'http://206.189.32.108/images_CWswDdGUmsUEoQKx/merchants/xmav2xfq2f7jnpxypek.jpeg',
        scaledSize: new google.maps.Size(30, 30),
        // origin: new google.maps.Point(0, 0),
        // anchor: new google.maps.Point(32,65),
        labelOrigin: new google.maps.Point(40,33),
        // labelOrigin: new google.maps.Point(40,33)

      }
    
      var marker = new google.maps.Marker({
        
        position: new google.maps.LatLng(100, 100),
        // animation: google.maps.Animation.DROP,
        map: map,
        label: {
          text: 'ABC',
          fontSize: "14px",
          labelClass: "map-marker-label",
        },
        icon: set_img
        
      });

        google.maps.event.addListener(marker, 'click', (function(marker, i) {
          return function() {
            infowindow.setContent(this.locations[i][0]);
            infowindow.open(map, marker);
          }
        })(marker, i));

        marker = new google.maps.Marker({
          position: new google.maps.LatLng(this.x, this.y),
          map: map,
          icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png'
        });
    });

  }


}
