import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppSearchPage } from './app-search';

@NgModule({
  declarations: [
    AppSearchPage,
  ],
  imports: [
    IonicPageModule.forChild(AppSearchPage),
  ],
})
export class AppSearchPageModule {}
