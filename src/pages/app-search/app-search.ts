import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  AlertController
} from "ionic-angular";
import { MerchantProvider } from "../../providers/merchant/merchant";

/**
 * Generated class for the AppSearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-app-search",
  templateUrl: "app-search.html"
})
export class AppSearchPage {
  merchants: any = [];
  status: any;
  statusMessage: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private merchantProvider: MerchantProvider
  ) {}

  ionViewDidLoad() {
    console.log("ionViewDidLoad AppSearchPage");
  }

  goBack() {
    this.navCtrl.pop();
  }

  getItems(ev: any) {
    // set val to the value of the searchbar
    const val = ev.target.value;

    if (val.length >= 3) {
      let data = {
        merchant_name: ev.target.value
      };

      //Fetched the result from api
      this.merchantProvider.searchApp(data).subscribe(
        res => {
          let resultset: any = res;

          let loader = this.loadingCtrl.create({
            content: "Loading Page ... "
          });

          loader.present();

          setTimeout(() => {
            this.merchants = resultset.data;
          }, 1000);

          setTimeout(() => {
            loader.dismiss();
          }, 1000);

          console.log("merchant app result", this.merchants);
        },
        error => {
          console.log(error);
        }
      );
    }
  }

  searchApp(ev: any) {
    // set val to the value of the searchbar
    const val = ev.target.value;

    let data = {
      merchant_name: ev.target.value
    };

    //Fetched the result from api
    this.merchantProvider.searchApp(data).subscribe(
      res => {
        let resultset: any = res;

        let loader = this.loadingCtrl.create({
          content: "Loading Page ... "
        });

        loader.present();

        setTimeout(() => {
          this.merchants = resultset.data;
        }, 1000);

        setTimeout(() => {
          loader.dismiss();
        }, 1000);

        console.log("merchant app result", this.merchants);
      },
      error => {
        console.log(error);
      }
    );
  }

  addAppToFav(merchantID: any) {
    let data = {
      user_id: localStorage.getItem("user_id"),
      merchant_id: merchantID
    };

    this.merchantProvider.addApp(data).subscribe(
      res => {
        let resultset: any = res;
        this.status = resultset.status;
        this.statusMessage = resultset.message;

        if (this.status == "success") {
          this.alertStatus("Success", "Successfully Added");
        } else {
          this.alertStatus("Error", this.statusMessage);
        }
      },
      error => {
        console.log(error);
      }
    );

    console.log("data", data);

    console.log("test", merchantID);
  }

  alertStatus(status: any, msg: string) {
    let alert = this.alertCtrl.create({
      title: status,
      message: msg,
      cssClass: "alert-title",
      buttons: [
        {
          text: "OK",
          role: "cancel",
          handler: () => {
            console.log("Yes clicked");
          }
        }
      ]
    });
    alert.present();
  }
}
