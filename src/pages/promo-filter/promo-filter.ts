import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';
import { PromoProvider } from '../../providers/promo/promo';

/**
 * Generated class for the PromoFilterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-promo-filter',
  templateUrl: 'promo-filter.html',
})
export class PromoFilterPage {
  promos: any = [];
  locationString: any;

  constructor(
    public navCtrl: NavController, 
    public appCtrl: App, 
    public navParams: NavParams,
    public promoProvider: PromoProvider) {

    this.locationString = this.navParams.get('location');
    console.log('location', this.locationString);

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PromoFilterPage');
    this.loadPromo();
  }

  loadPromo(){
    console.log("Promo Loads!")
    this.promoProvider.getPromoFilterLocation(this.locationString).subscribe(
      res => {
        let resultset: any = res;
        this.promos = resultset.data;
        console.log("promo list", this.promos); 
      }, error => {
        console.log(error);
    });
  }

  goTo(){
    this.appCtrl.getRootNav().push('FilterPage');
    console.log("sana");
  }

  goBack() {
    this.navCtrl.pop();
  }

  navigateTo(page:string){
    this.appCtrl.getRootNav().push(page);
  }

  openPromoDetail(promo_id){
    this.navCtrl.push('PromoModalPage', 
    {promo_id: promo_id});

  }


  openModal(){
    this.navCtrl.push('FilterPage');
  }

}
