import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PromoFilterPage } from './promo-filter';

@NgModule({
  declarations: [
    PromoFilterPage,
  ],
  imports: [
    IonicPageModule.forChild(PromoFilterPage),
  ],
})
export class PromoFilterPageModule {}
