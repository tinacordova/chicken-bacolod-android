import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BookstorePromoDetailPage } from './bookstore-promo-detail';

@NgModule({
  declarations: [
    BookstorePromoDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(BookstorePromoDetailPage),
  ],
})
export class BookstorePromoDetailPageModule {}
