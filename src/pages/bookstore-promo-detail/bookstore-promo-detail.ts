import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the BookstorePromoDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-bookstore-promo-detail',
  templateUrl: 'bookstore-promo-detail.html',
})
export class BookstorePromoDetailPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BookstorePromoDetailPage');
  }

  goBack() {
    this.navCtrl.pop();
  }

}
