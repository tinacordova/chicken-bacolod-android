import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  App,
  LoadingController
} from "ionic-angular";

import { VoucherProvider } from "../../providers/voucher/voucher";
import { CartProvider } from '../../providers/cart/cart';
import { MerchantProvider } from '../../providers/merchant/merchant';
import { Storage } from '@ionic/storage';

import { Observable } from "rxjs";
import { debounceTime, distinctUntilChanged } from "rxjs/operators";

/**
 * Generated class for the VoucherFabPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-voucher-fab",
  templateUrl: "voucher-fab.html"
})
export class VoucherFabPage {
  vouchers: any = [];
  voucherCounters: any = [];
  search_results: any = [];
  voucherCategories: any = [];
  merchants: any = [];
  show: boolean = false;
  showFilter: boolean = true;
  voucherBoolean: boolean = false;
  newCate: any;
  merchantID: number;

  userID: number;
  tableNumber: number;
  main: any;
  cart_id: number;
  cartcounter: any [];
  counterFront: number;

  firstFilter: boolean = true;
  secondFilter: boolean = false;

  searchQuery: string = "";
  items: string[];
  locationPass: any;

  resCount: number;
  clearStatus: Boolean = false;

  searchChangeObserver;

  constructor(
    public navCtrl: NavController,
    public appCtrl: App,
    public navParams: NavParams,
    public voucherProvider: VoucherProvider,
    private storage: Storage,
    public loadingCtrl: LoadingController,
    public merchantProvider: MerchantProvider,
    public cartProvider: CartProvider
  ) {
    this.initializeItems();
    this.locationPass = this.navParams.get("location");
    this.newCate = localStorage.getItem("categorize");

    console.log("bam", this.locationPass);
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad VoucherFabPage");
    this.getCartID();
    this.loadShopInfo();
    //this.loadVoucher();
    this.loadVoucherCategory();

    if (this.locationPass == null) {
      //this.loadVoucher();
      this.loadVoucherCategory();
      console.log("location is null", this.locationPass);
    } else {
      // Parameter is not null, pass the param as a query
      console.log("not null location", this.locationPass);

      this.firstFilter = false;
      this.secondFilter = true;
      this.loadVoucherFilter();
    }
  }

  loadShopInfo(){

    this.storage.get('merchantID').then((val)=>{
      this.merchantID = val;

      this.merchantProvider.merchantInfo(this.merchantID).subscribe(
        res => {
          let resultset: any = res;
          this.merchants = resultset.data;
  
          console.log("merchants", this.merchants);
        }, error => {
          console.log(error);
      });
    
    });
  }

  initializeItems() {
    this.items = ["Amsterdam", "Bogota"];
  }

  getCartID(){

    this.storage.get('userID').then((value)=>{
      this.userID = value;

      console.log('user_id', this.userID);
  
      this.storage.get('merchantID').then((value)=>{
        this.merchantID = value;

        console.log('merchant_id', this.merchantID);

        this.storage.get('tableNumber').then((value)=>{
          this.tableNumber = value;

          this.cartProvider.cartMain(this.userID, this.merchantID, 0).subscribe(
            res => {
              let resultset: any = res;
              this.main = resultset.data;
              console.log("main", this.main);
              console.log("cartid", this.main[0].cart_id);
              console.log("mainid", this.main[0].ipayu_id);

              this.loadCartDetailCounter(this.main[0].cart_id);
      
              this.cart_id = this.main[0].cart_id      
              this.storage.set('cartID', this.cart_id);
      
            }, error => {
              console.log(error);
          });
        
        });
      
      });
    
    });
  }

  loadCartDetailCounter(cartID){

    this.storage.get('merchantID').then((value)=>{
      this.merchantID = value;

        this.cartProvider.getCartDetailCounter(cartID, this.merchantID).subscribe(
          res => {
            let resultset: any = res;
            this.cartcounter = resultset.data;
            
            console.log("counter 123", this.cartcounter[0].detail_counter);
            this.counterFront = this.cartcounter[0].detail_counter
    
            if (this.cartcounter[0].detail_counter == 0)
            this.counterFront + 1
            
          }, error => {
            console.log(error);
        });

    });
}

  loadVoucherCategory() {

    this.storage.get('merchantID').then((value) => {
      this.merchantID = value;
    
      
    let data = {
      "merchant_id":this.merchantID
    };

    this.voucherProvider.getVoucherMerchantCategoryItems(data).subscribe(
      res => {
        let resultset: any = res;


        this.voucherCategories = resultset.data;

        console.log("voucher 123", this.voucherCategories);
      },
      error => {
        console.log(error);
      }
    );
    
    });
    
  }

  loadVoucher() {
    console.log("Voucher Loads!");
    this.voucherProvider.getVoucherCategoryItems().subscribe(
      res => {
        let resultset: any = res;
        this.vouchers = resultset.data;
        console.log("voucher home", this.vouchers);
      },
      error => {
        console.log(error);
      }
    );
  }

  goBack() {
    this.navCtrl.pop();
    //this.appCtrl.getRootNav().push('HomePage');
  }

  navigateTo(page: string) {
    this.appCtrl.getRootNav().push(page);
  }

  goTo() {
    this.appCtrl.getRootNav().push("VoucherFilterPage");
    console.log("sana");
  }

  goToFilter() {
    this.navCtrl.pop();
    console.log("sana");
  }

  goBackTo() {
    this.appCtrl.getRootNav().push("VoucherModalPage");
  }

  openModal() {
    this.navCtrl.push("VoucherFilterPage");
  }

  openVoucherDetail(voucher_id) {
    this.navCtrl.push("VoucherModalPage", { voucher_id: voucher_id });
  }

  openSearchTo() {
    this.navCtrl.push("VoucherSearchResultPage");
  }

  openCart(){
    this.appCtrl.getRootNav().push('MenuOrderingPage');
  }

  loadVoucherFilter() {
    this.show = true;

    this.voucherProvider
      .getVoucherFilterLocation(localStorage.getItem("key"), this.newCate)
      .subscribe(
        res => {
          let resultset: any = res;
          let resultset2: any = res;

          let loader = this.loadingCtrl.create({
            content: "Loading Page ... "
          });

          loader.present();

          setTimeout(() => {
            this.voucherCounters = resultset2.voucher_counter;

            if (this.voucherCounters.length > 0) {
              this.voucherBoolean = true;
            }

            this.search_results = resultset.data;
          }, 1000);

          setTimeout(() => {
            loader.dismiss();
          }, 3000);

          console.log("voucher counter", this.voucherCounters);

          console.log("voucher search result", this.search_results);
        },
        error => {
          console.log(error);
        }
      );
  }

  getItems(ev: any) {
    let sample = 0;
    //Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the searchbar
    const val = ev.target.value;

    if (val.length >= 3) {
      this.search_results = [];

      this.show = true;
      //Fetched the result from api
      this.voucherProvider.getVoucherFilter(val).subscribe(
        res => {
          let resultset: any = res;
          let resultset2: any = res;

          let loader = this.loadingCtrl.create({
            content: "Loading Page ... "
          });

          loader.present();

          setTimeout(() => {
            this.voucherCounters = resultset2.voucher_counter;

            if (this.voucherCounters.length > 0) {
              this.voucherBoolean = true;
            }

            this.search_results = resultset.data;
          }, 1000);

          setTimeout(() => {
            loader.dismiss();
          }, 1000);

          console.log("voucher counter", this.voucherCounters);

          console.log("voucher search result", this.search_results);
        },
        error => {
          console.log(error);
        }
      );

      console.log("Thank you");
    }

    console.log("value", val);
    console.log("value lenth", val.length);

    // if the value is an empty string don't filter the items
    if (val && val.trim() != "") {
      this.items = this.items.filter(item => {
        return item.toLowerCase().indexOf(val.toLowerCase()) > -1;
      });
    }
  }

  toWallet() {
    this.appCtrl.getRootNav().push("WalletFabPage");
  }
  
  toProduct() {
    this.appCtrl.getRootNav().push("SpecialOfferPage");
  }

  toPromo() {
    this.appCtrl.getRootNav().push("PromoPage");
  }

  toResto() {
    this.appCtrl.getRootNav().push("VoucherFabPage");
  }

  onSearchChange(searchValue: string) {
    if (!this.searchChangeObserver) {
      Observable.create(observer => {
        this.searchChangeObserver = observer;
      })
        .pipe(debounceTime(1500)) // wait 300ms after the last event before emitting last event
        .pipe(distinctUntilChanged()) // only emit if value is different from previous value
        .subscribe(res => {
          this.resCount = res.length;

          if (
            this.resCount > 0 &&
            res.length > 0 &&
            this.clearStatus === false
          ) {

            this.storage.get('merchantID').then((value)=>{
              this.merchantID = value;

              let data = {
                "merchant_id":this.merchantID,
                "keyword":res
              };
  
                this.voucherProvider.getMerchantVoucherSearch(data).subscribe(
                  res => {
                    let resultset: any = res;
                    this.voucherCategories = [];
            
                    this.voucherCategories = resultset.data;
                    console.log("Voucher merchant Search", this.voucherCategories);
                  },
                  error => {
                    console.log(error);
                  }
                );
            });
          }

          this.clearStatus = false;
        });
    }

    this.searchChangeObserver.next(searchValue);
  }

  onClear(event: any) {
    //This is a marker that the clear search was clicked (the x button)
    this.clearStatus = true;
  }

  onChange(event: any) {
    //Get the string length of the search bar and assign to resCount
    this.resCount = event.target.value.length;
  }

  
  
}
