import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VoucherFabPage } from './voucher-fab';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    VoucherFabPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(VoucherFabPage),
  ],
})
export class VoucherFabPageModule {}
