import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';

/**
 * Generated class for the YupangcoBranchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-yupangco-branch',
  templateUrl: 'yupangco-branch.html',
})
export class YupangcoBranchPage {

  constructor(
    public navCtrl: NavController, 
    public appCtrl: App,
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad YupangcoBranchPage');
  }

  goBack() {
    this.navCtrl.pop();
  }

}
