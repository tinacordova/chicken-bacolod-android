import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { YupangcoBranchPage } from './yupangco-branch';

@NgModule({
  declarations: [
    YupangcoBranchPage,
  ],
  imports: [
    IonicPageModule.forChild(YupangcoBranchPage),
  ],
})
export class YupangcoBranchPageModule {}
