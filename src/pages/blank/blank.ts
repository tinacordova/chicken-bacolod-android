import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController,LoadingController, Loading } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-blank',
  templateUrl: 'blank.html',
})
export class BlankPage {

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public viewCtrl: ViewController,
              public loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BlankPage');
    // this.viewCtrl.dismiss()  
    // this.dismiss()

    let loader = this.loadingCtrl.create({
      content: 'Loading Page ... ',
    });
  
    loader.present();
  
    setTimeout(() => {

  
    }, 1000);
  
    setTimeout(() => {
      loader.dismiss();
    }, 1000);
    
  }

  dismiss(){
    // setTimeout(() => {
    //   this.viewCtrl.dismiss();
    // }, 1000);
    this.viewCtrl.dismiss()
  }

}
