import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';

/**
 * Generated class for the YupangSpecialDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-yupang-special-detail',
  templateUrl: 'yupang-special-detail.html',
})
export class YupangSpecialDetailPage {

  constructor(
    public navCtrl: NavController,
    public appCtrl: App,  
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad YupangSpecialDetailPage');
  }

  goBack() {
    this.navCtrl.pop();
  }

}
