import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { YupangSpecialDetailPage } from './yupang-special-detail';

@NgModule({
  declarations: [
    YupangSpecialDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(YupangSpecialDetailPage),
  ],
})
export class YupangSpecialDetailPageModule {}
