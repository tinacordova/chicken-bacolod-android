import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';

/**
 * Generated class for the YupangVoucDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-yupang-vouc-detail',
  templateUrl: 'yupang-vouc-detail.html',
})
export class YupangVoucDetailPage {

  constructor(
    public navCtrl: NavController,
    public appCtrl: App, 
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad YupangVoucDetailPage');
  }

  goBack() {
    this.navCtrl.pop();
  }

}
