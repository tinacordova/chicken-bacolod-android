import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { YupangVoucDetailPage } from './yupang-vouc-detail';

@NgModule({
  declarations: [
    YupangVoucDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(YupangVoucDetailPage),
  ],
})
export class YupangVoucDetailPageModule {}
