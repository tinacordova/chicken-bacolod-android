import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SpecialOfferPage } from './special-offer';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    SpecialOfferPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(SpecialOfferPage),
  ],
})
export class SpecialOfferPageModule {}
