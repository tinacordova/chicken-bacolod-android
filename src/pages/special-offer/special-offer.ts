import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  App,
  LoadingController
} from "ionic-angular";
import { CartProvider } from '../../providers/cart/cart';
import { MerchantProvider } from '../../providers/merchant/merchant';
import { VoucherProvider } from "../../providers/voucher/voucher";
import { ProductProvider } from "../../providers/product/product";

import { Storage } from '@ionic/storage';

import { Observable } from "rxjs";
import { debounceTime, distinctUntilChanged } from "rxjs/operators";

@IonicPage()
@Component({
  selector: 'page-special-offer',
  templateUrl: 'special-offer.html',
})
export class SpecialOfferPage {

  vouchers: any = [];
  voucherCounters: any = [];

  specialOffers: any = [];
  specialOfferCounters: any = [];

  search_results: any = [];
  voucherCategories: any = [];
  merchants: any = [];

  specialOfferCategories: any = [];
  prodCategories: any = [];

  show: boolean = false;
  showFilter: boolean = true;
  voucherBoolean: boolean = false;
  newCate: any;
  merchantID: number;
  userID: number;
  tableNumber: number;
  main: any;
  cart_id: number;
  cartcounter: any [];
  counterFront: number;

  firstFilter: boolean = true;
  secondFilter: boolean = false;

  searchQuery: string = "";
  items: string[];
  locationPass: any;

  localStorageUserId: string;
  localStorageFirstName: string;
  localStorageLastName: string;

  resCount: number;
  clearStatus: Boolean = false;

  searchChangeObserver;

  constructor(
    public navCtrl: NavController,
    public appCtrl: App,
    public navParams: NavParams,
    private storage: Storage,
    public cartProvider: CartProvider,
    public merchantProvider: MerchantProvider,
    public voucherProvider: VoucherProvider,
    public productProvider: ProductProvider,
    public loadingCtrl: LoadingController
  ) {
    this.initializeItems();
    this.locationPass = this.navParams.get("location");
    this.newCate = localStorage.getItem("categorize");
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad VoucherFabPage");

    this.getCartID();
    this.loadShopInfo();

    this.localStorageUserId = localStorage.getItem('user_id');
    this.localStorageFirstName = localStorage.getItem('user_firstname');
    this.localStorageLastName = localStorage.getItem('user_lastname');

    this.loadSpecialOffer();
    this.loadSpecialOfferCategory();

    if (this.locationPass == null) {
      this.loadSpecialOffer();
      console.log("location is null", this.locationPass);
    } else {
      // Parameter is not null, pass the param as a query
      console.log("not null location", this.locationPass);

      this.firstFilter = false;
      this.secondFilter = true;
      //this.loadVoucherFilter();
      this.loadSpecialOfferFilter();
    }
  }

  initializeItems() {
    this.items = ["Amsterdam", "Bogota"];
  }

  getCartID(){

    this.storage.get('userID').then((value)=>{
      this.userID = value;

      console.log('user_id', this.userID);
  
      this.storage.get('merchantID').then((value)=>{
        this.merchantID = value;

        console.log('merchant_id', this.merchantID);

        this.storage.get('tableNumber').then((value)=>{
          this.tableNumber = value;

          this.cartProvider.cartMain(this.userID, this.merchantID, 0).subscribe(
            res => {
              let resultset: any = res;
              this.main = resultset.data;
              console.log("main", this.main);
              console.log("cartid", this.main[0].cart_id);
              console.log("mainid", this.main[0].ipayu_id);

              this.loadCartDetailCounter(this.main[0].cart_id);
      
              this.cart_id = this.main[0].cart_id      
              this.storage.set('cartID', this.cart_id);
      
            }, error => {
              console.log(error);
          });
        
        });
      
      });
    
    });
  }

  loadCartDetailCounter(cartID){

    this.storage.get('merchantID').then((value)=>{
      this.merchantID = value;

        this.cartProvider.getCartDetailCounter(cartID, this.merchantID).subscribe(
          res => {
            let resultset: any = res;
            this.cartcounter = resultset.data;
            
            console.log("counter 123", this.cartcounter[0].detail_counter);
            this.counterFront = this.cartcounter[0].detail_counter
    
            if (this.cartcounter[0].detail_counter == 0)
            this.counterFront + 1
            
          }, error => {
            console.log(error);
        });

    });
}

loadShopInfo(){

  this.storage.get('merchantID').then((val)=>{
    this.merchantID = val;

    this.merchantProvider.merchantInfo(this.merchantID).subscribe(
      res => {
        let resultset: any = res;
        this.merchants = resultset.data;

        console.log("merchants", this.merchants);
      }, error => {
        console.log(error);
    });
  
  });
}


  loadSpecialOfferCategory() {
    let data = {};
    this.productProvider.getSpecialOfferCategoryItems(data).subscribe(
      res => {
        let resultset: any = res;

        //display all the category with product inside
        this.specialOfferCategories = resultset.data;

        console.log("product 123", this.specialOfferCategories);
      },
      error => {
        console.log(error);
      }
    );
  }


  loadSpecialOffer() {
    console.log("Voucher Loads!");

    this.storage.get('merchantID').then((value) => {
      this.merchantID = value;

      let data = {
        "merchant_id":this.merchantID
      };
  
      this.productProvider.getMerchantSpecialOfferCategoryItemsWallet(data).subscribe(
        res => {
          let resultset: any = res;
          this.prodCategories = resultset.data;
          console.log("sp home", this.prodCategories);
        },
        error => {
          console.log(error);
        });
    });
  }

  goBack() {
    this.navCtrl.pop();
  }

  navigateTo(page: string) {
    this.appCtrl.getRootNav().push(page);
  }

  goTo() {
    this.appCtrl.getRootNav().push("SpecialOfferFilterPage");
    console.log("sana");
  }

  goToFilter() {
    this.navCtrl.pop();
    console.log("sana");
  }

  goBackTo() {
    this.appCtrl.getRootNav().push("VoucherModalPage");
  }

  openModal() {
    this.navCtrl.push("VoucherFilterPage");
  }

  openProductDetail(prod_id, mer_id){

    let loader = this.loadingCtrl.create({
      content: 'Loading Page ... ',
    });

    loader.present();

    setTimeout(() => {

      this.navCtrl.push('BookstoreDetailPage', 
      {prod_id: prod_id,
      mer_id: mer_id});

    }, 1000);

    setTimeout(() => {
      loader.dismiss();
    }, 3000);

  }

  openSearchTo() {
    this.navCtrl.push("VoucherSearchResultPage");
  }


  loadSpecialOfferFilter() {
    this.show = true;

    this.productProvider
      .getSpecialOfferFilterLocation(localStorage.getItem("key"), this.newCate)
      .subscribe(
        res => {
          let resultset: any = res;
          let resultset2: any = res;

          let loader = this.loadingCtrl.create({
            content: "Loading Page ... "
          });

          loader.present();

          setTimeout(() => {
            this.specialOfferCounters = resultset2.special_offer_counter;

            if (this.specialOfferCounters.length > 0) {
              this.voucherBoolean = true;
            }

            this.search_results = resultset.data;
          }, 1000);

          setTimeout(() => {
            loader.dismiss();
          }, 3000);

          console.log("sp counter", this.voucherCounters);

          console.log("sp search result", this.search_results);
        },
        error => {
          console.log(error);
        }
      );
  }

  getItems(ev: any) {
    let sample = 0;
    //Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the searchbar
    const val = ev.target.value;

    if (val.length >= 3) {
      this.search_results = [];

      this.show = true;
      //Fetched the result from api
      this.productProvider.getSpecialOfferFilter(val).subscribe(
        res => {
          let resultset: any = res;
          let resultset2: any = res;

          let loader = this.loadingCtrl.create({
            content: "Loading Page ... "
          });

          loader.present();

          setTimeout(() => {
            this.specialOfferCounters = resultset2.special_offer_counter;

            if (this.specialOfferCounters.length > 0) {
              this.voucherBoolean = true;
            }

            this.search_results = resultset.data;
          }, 1000);

          setTimeout(() => {
            loader.dismiss();
          }, 1000);

          console.log("sp counter", this.specialOfferCounters);

          console.log("sp search result", this.search_results);
        },
        error => {
          console.log(error);
        }
      );

      console.log("Thank you");
    }

    console.log("value", val);
    console.log("value lenth", val.length);

    // if the value is an empty string don't filter the items
    if (val && val.trim() != "") {
      this.items = this.items.filter(item => {
        return item.toLowerCase().indexOf(val.toLowerCase()) > -1;
      });
    }
  }

  openBookDetail(prod_id){

    let loader = this.loadingCtrl.create({
      content: 'Loading Page ... ',
    });

    loader.present();

    setTimeout(() => {

      this.navCtrl.push('BookstoreDetailPage', 
      {prod_id: prod_id});

    }, 1000);

    setTimeout(() => {
      loader.dismiss();
    }, 3000);

  }

  onSearchChange(searchValue: string) {
    if (!this.searchChangeObserver) {
      Observable.create(observer => {
        this.searchChangeObserver = observer;
      })
        .pipe(debounceTime(1500)) // wait 300ms after the last event before emitting last event
        .pipe(distinctUntilChanged()) // only emit if value is different from previous value
        .subscribe(res => {
          this.resCount = res.length;

          if (
            this.resCount > 0 &&
            res.length > 0 &&
            this.clearStatus === false
          ) {

            this.storage.get('merchantID').then((value) => {
              this.merchantID = value;
        
              let data = {
                "merchant_id":this.merchantID,
                "keyword":res
              };
          
              this.productProvider.getMerchantSpecialOfferSearch(data).subscribe(
                res => {
                  let resultset: any = res;
                  this.prodCategories = resultset.data;
                  console.log("sp home", this.specialOffers);
                },
                error => {
                  console.log(error);
                });
            });

          }

          this.clearStatus = false;
        });
    }

    this.searchChangeObserver.next(searchValue);
  }

  onClear(event: any) {
    //This is a marker that the clear search was clicked (the x button)
    this.clearStatus = true;
  }

  onChange(event: any) {
    //Get the string length of the search bar and assign to resCount
    this.resCount = event.target.value.length;
  }

  openCart(){
    this.appCtrl.getRootNav().push('MenuOrderingPage');
  }

  toWallet() {
    this.appCtrl.getRootNav().push("WalletFabPage");
  }
  
  toProduct() {
    this.appCtrl.getRootNav().push("SpecialOfferPage");
  }

  toPromo() {
    this.appCtrl.getRootNav().push("PromoPage");
  }

  toResto() {
    this.appCtrl.getRootNav().push("VoucherFabPage");
  }
  
  navScreen(page:string) {
    this.appCtrl.getRootNav().push(page);
  }

}
