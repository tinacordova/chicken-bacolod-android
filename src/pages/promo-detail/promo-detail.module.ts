import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PromoDetailPage } from './promo-detail';

@NgModule({
  declarations: [
    PromoDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(PromoDetailPage),
  ],
})
export class PromoDetailPageModule {}
