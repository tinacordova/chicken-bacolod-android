import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { ComponentsModule } from '../components/components.module';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
// import { ReservationPage } from '../pages/reservation/reservation';

import { MyApp } from './app.component';

import { Geolocation } from '@ionic-native/geolocation';
import { Diagnostic } from '@ionic-native/diagnostic';
import { DatePicker } from '@ionic-native/date-picker';
import { IonicStorageModule } from '@ionic/storage';
import { SQLite} from '@ionic-native/sqlite';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { SearchProvider } from '../providers/search/search';
import { MerchantProvider } from '../providers/merchant/merchant';
import { BannerProvider } from '../providers/banner/banner';
import { ProductProvider } from '../providers/product/product';
import { VoucherProvider } from '../providers/voucher/voucher';
import { PromoProvider } from '../providers/promo/promo';
import { NativeGeocoder } from '@ionic-native/native-geocoder';
import { CategoryProvider } from '../providers/category/category';
import { PromoSearchProvider } from '../providers/promo-search/promo-search';

import { SocialSharing } from '@ionic-native/social-sharing';
import { Calendar } from '@ionic-native/calendar';
import { PromoRatingProvider } from '../providers/promo-rating/promo-rating';
import { IonicRatingModule } from 'ionic-rating';
import { WalletProvider } from '../providers/wallet/wallet';
import { ReservationProvider } from '../providers/reservation/reservation';
import { CartProvider } from '../providers/cart/cart';
import { UserProvider } from '../providers/user/user';
import { TransactionProvider } from '../providers/transaction/transaction';
import { UniversalSearchProvider } from '../providers/universal-search/universal-search';
import { SpecialOfferProvider } from '../providers/special-offer/special-offer';
import { RestoCategoryProvider } from '../providers/resto-category/resto-category';
import { ProductDetailProvider } from '../providers/product-detail/product-detail';
import { BranchProvider } from '../providers/branch/branch';


@NgModule({
  declarations: [
    MyApp,
    // ReservationPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    ComponentsModule,
    HttpClientModule,
    IonicRatingModule,
    IonicModule.forRoot(MyApp, { scrollAssist: false, autoFocusAssist: false }),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    // ReservationPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    SearchProvider,
    MerchantProvider,
    BannerProvider,
    ProductProvider,
    VoucherProvider,
    PromoProvider,
    Geolocation,
    Diagnostic,
    DatePicker,
    NativeGeocoder,
    CategoryProvider,
    PromoSearchProvider,
    SocialSharing,
    Calendar,
    PromoRatingProvider,
    WalletProvider,
    ReservationProvider, 
    CartProvider,
    UserProvider, 
    TransactionProvider,
    UniversalSearchProvider,
    SpecialOfferProvider, 
    RestoCategoryProvider,
    ProductDetailProvider,
    BranchProvider 
  ]
})
export class AppModule {}
