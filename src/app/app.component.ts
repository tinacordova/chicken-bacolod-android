import { Component, ViewChild  } from '@angular/core';
import { Nav, Platform, MenuController, App } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';

import { BookstorePage } from '../pages/bookstore/bookstore';
import { LoginPage } from '../pages/login/login';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage:string;

  // rootPage:string = 'RestaurantPage';


  firstName: string;
  lastName: string;
  localStorageFirstName: string;
  localStorageLastName: string;

  constructor(
    platform: Platform, 
    statusBar: StatusBar, 
    public appCtrl: App,
    private storage: Storage,
    public menuCtrl: MenuController,
    splashScreen: SplashScreen) {

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleLightContent(); // Make the status bar visible
      //statusBar.styleDefault();

      this.rootPage = 'BookstorePage';

      this.storage.get('firstName').then((val)=>{
        this.firstName = val;
        console.log('fname', this.firstName);
      });
  
      this.storage.get('lastName').then((val)=>{
        this.lastName = val;
        console.log('lname', this.lastName);
      });

      if(localStorage.getItem('user_id') == null){
        this.rootPage = 'LoginPage';
      }else{
        this.rootPage = 'BookstorePage';
      }

      splashScreen.hide();  

    });
  }

  navScreen(page:string) {
    this.appCtrl.getRootNav().push(page);
  }

  navigateToLogin(){
    this.menuCtrl.close();
    this.storage.clear();
    this.appCtrl.getRootNav().push('LoginPage');
  }

  menuOpened(){
    this.storage.get('firstName').then((val)=>{
      this.firstName = val;
      console.log('fname', this.firstName);
    });

    this.storage.get('lastName').then((val)=>{
      this.lastName = val;
      console.log('lname', this.lastName);
    });
  }

}

