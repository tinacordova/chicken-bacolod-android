import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ENV } from '../../environment/environment';
import 'rxjs/add/operator/map'

/*
  Generated class for the SearchProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SearchProvider {

  private url = ENV.apiUrl;

  constructor(public http: HttpClient) {
    console.log('Hello BannerProvider Provider');
  }

  getResult(loc:string, desc:string){
    console.log('url', this.url + '/v1/search?loc='+ loc +'&desc='+ desc);
    return this.http.get(this.url + '/v1/search?loc='+ loc +'&desc='+ desc ).map(res => res );
  }

  getBannerDetail(id:number){
    return this.http.get(this.url + '/v1/banner/' + id).map(res => res );
  }

}
