import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';

import { ENV } from '../../environment/environment';


/*
  Generated class for the ProductProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ProductProvider {

  private url = ENV.apiUrl;

  constructor(public http: HttpClient) {
    console.log('Hello ProductProvider Provider');
  }

  // NEW INTEGATION FOR FRONTEND

  searchProduct(loc: string, desc:string){
    return this.http.get(this.url + '/search?loc' + loc + '&desc' + desc).map(res => res );
  }

  getProducts(){
    return this.http.get(this.url + '/front/product').map(res => res );
  }

  getProductsHome(){
    return this.http.get(this.url + '/v8/product/market').map(res => res );
  }

  getProductDetailV8(id: number){
    let status = localStorage.getItem('uni_search');
    
    console.log('status', status);
    
    if(status === 'true'){
      localStorage.setItem('uni_search', 'false');
      return this.http.get(this.url + '/v8/product_uni/' + id).map(res => res );
    }else{
      return this.http.get(this.url + '/v8/product/' + id).map(res => res );
    }
  }

  getProductBookDetailV8(id: number){    
      return this.http.get(this.url + '/v8/product_book/' + id).map(res => res );
  }

  getProductSubImages(id: number){
    return this.http.get(this.url + '/v8/product_sub_images/' + id).map(res => res );
  }

  getMarketplace(){
    return this.http.get(this.url + '/front/product/filter/marketplace').map(res => res );
  }

  sortMarketplace(sort: string){
    return this.http.get(this.url + '/front/product/filter/marketplace/sort/' + sort).map(res => res );
  }

  getSales(){
    return this.http.get(this.url + '/front/product/filter/sales').map(res => res );
  }

  sortSales(sort: string){
    return this.http.get(this.url + '/front/product/filter/sales/sort/' + sort).map(res => res );
  }

  /* ENDS HERE */



  getProductsDetail(id: number){
    return this.http.get(this.url + '/v1/product/' + id).map(res => res );
  }

  sortProducts(sort: string){
    return this.http.get(this.url + '/v1/product/sort/' + sort).map(res => res );
  }

  firstBatchProducts(){
    return this.http.get(this.url + '/v1/product/batch/1').map(res => res );
  }

  secondBatchProducts(){
    return this.http.get(this.url + '/v1/product/batch/2').map(res => res );
  }

  getFilter(filter: string){
    return this.http.get(this.url + '/v1/product/filter/' + filter).map(res => res );
  }

  getProductMerchantCategory(id: number){
    return this.http.get(this.url + '/v8/merchant/category/' + id).map(res => res );
  }

  getBookstoreMerchantCategory(data:any){
    return this.http.post(this.url + '/v8/bookstore/category', data).map(res => res );
  }

  getBookstoreTextSearch(data:any){
    return this.http.post(this.url + '/v8/bookstore/text_search', data).map(res => res );
  }

  getSpecialOffer(merchant_id: number){
    return this.http.get(this.url + '/v8/product/special_offer/merchant/' + merchant_id).map(res => res );
  }

  getProductMerchantCategoryItems(merchant_id: number, cat_id: number){
    return this.http.get(this.url + '/v8/merchant/'+ merchant_id + '/category/' + cat_id).map(res => res );
  }

  getSpecialOfferCategoryItems(data:any){
    return this.http.post(this.url + '/v8/categorized_special_offer_list', data).map(res => res );
  }

  getMerchantSpecialOfferCategoryItems(data:any){
    return this.http.post(this.url + '/v8/merchant_categorized_special_offer_list', data).map(res => res );
  }

  getProductMerchantList(data:any){
    return this.http.post(this.url + '/v8/product_merchant_list', data).map(res => res );
  }

  getMerchantSpecialOfferCategoryItemsWallet(data:any){
    return this.http.post(this.url + '/v8/merchant_categorized_special_offer_list_wallet', data).map(res => res );
  }

  getMerchantSpecialOfferSearch(data:any){
    return this.http.post(this.url + '/v8/merchant_special_offer_search', data).map(res => res );
  }

  getProductCategories(data:any){
    return this.http.post(this.url + '/v8/product_category_list', data).map(res => res );
  }

  getVoucherFilterLocation(query: string, arr = []){

    let data = {
      "query": "",
      "location": query,
      "trade_category_id": [arr]
    };

  const httpOptions = {
    headers: new HttpHeaders({
          'Accept':  'application/json',
          'Content-Type': 'application/json'
        })
    };

  let body = JSON.stringify(data);
  console.log('body',body);

  return this.http.post(this.url + '/search/voucher_search_category',body,httpOptions);        
  }

  getSpecialOfferFilterLocation(query: string, arr = []){

    let data = {
      "query": "",
      "location": query,
      "trade_category_id": [arr]
    };

  const httpOptions = {
    headers: new HttpHeaders({
          'Accept':  'application/json',
          'Content-Type': 'application/json'
        })
    };

  let body = JSON.stringify(data);
  console.log('body',body);

  return this.http.post(this.url + '/search/special_offer_search_category',body,httpOptions);        
  }

  getSpecialOfferFilter(query: string){

    let data = {
      "query": query,
      "location": ""
    };

    const httpOptions = {
      headers: new HttpHeaders({
            'Accept':  'application/json',
            'Content-Type': 'application/json'
          })
      };
 
    let body = JSON.stringify(data);
    console.log('body',body);
  
    return this.http.post(this.url + '/v8/special_offer_search',body,httpOptions);        
    }
      
}
