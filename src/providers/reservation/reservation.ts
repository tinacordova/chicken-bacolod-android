import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';

import { ENV } from '../../environment/environment';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the ReservationProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ReservationProvider {
  private url = ENV.apiUrl;

  constructor(public http: HttpClient) {
    console.log('Hello ReservationProvider Provider');
  }

  getReservationList(data:any){
    return this.http.get(this.url + '/v8/reservation/list', data).map(res => res);
  }

  getMerchantAppointmentList(data:any){
    return this.http.post(this.url + '/v8/appointment_list', data).map(res => res); 
  }

  createReservation(data:any){
    return this.http.post(this.url + '/v8/reservation/user', data).map(res => res);      
  }

  createAppointment(data:any){
    return this.http.post(this.url + '/v8/appointment', data).map(res => res);      
  }

  cancelReservation(data:any){
    return this.http.post(this.url + '/merchant/cancel_reservation', data).map(res => res);      
  }

  cancelAppointment(data:any){
    return this.http.post(this.url + '/merchant/cancel_appointment', data).map(res => res);      
  }

}
