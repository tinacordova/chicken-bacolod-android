import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { ENV } from '../../environment/environment';
import 'rxjs/add/operator/map';

/*
  Generated class for the PromoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PromoProvider {

  private url = ENV.apiUrl;

  constructor(public http: HttpClient) {
    console.log('Hello PromoProvider Provider');
  }

  /* NEW INTEGRATION FOR FRONTEND */

  getPromo(){
    return this.http.get(this.url + '/front/promo').map(res => res );
  }

  getPromoCategoryItems(){
    return this.http.get(this.url + '/v8/categorized_promo_list').map(res => res );
  }

  getMerchantPromoCategoryItems(data: any){
    return this.http.post(this.url + '/v8/merchant_categorized_promo_list', data).map(res => res); 
  }

  getMerchantPromoSearch(data: any){
    return this.http.post(this.url + '/v8/merchant_promo_search', data).map(res => res); 
  }
  
  getPromoHome(){
    return this.http.get(this.url + '/v8/promo').map(res => res );
  }

  getPromoFilterLocation(location: any){
    return this.http.get(this.url + '/v8/search/location/' + location).map(res => res );
  }

  getPromoHomeOne(){
    return this.http.get(this.url + '/v8/promo/one').map(res => res );
  }

  getPromoHomeLimit(limit: number){
    return this.http.get(this.url + '/v8/promo/' + limit).map(res => res );
  }

  getMerchantPromo(id:number){
    return this.http.get(this.url + '/v8/promo/merchant/' + id).map(res => res );
  }

  sortPromo(sort: string){
    return this.http.get(this.url + '/front/promo/sort/' + sort).map(res => res );
  }

  getPromoDetail(id:number){
    return this.http.get(this.url + '/v8/promo_detail/' + id).map(res => res );
  }

  promoLocation(id:number){
    return this.http.get(this.url + '/front/promo/location/' + id).map(res => res );
  }
  
  sortPromoLocation(id:number, sort: string){
    return this.http.get(this.url + '/front/promo/location/' + id + '/sort/' + sort).map(res => res );
  }

  promoMerchant(id:number){
    return this.http.get(this.url + '/front/promo/merchant/' + id).map(res => res );
  }

  promoMerchantSort(id:number, sort: string){
    return this.http.get(this.url + '/front/promo/merchant/' + id + '/sort/' + sort).map(res => res );
  }

  promoMerchantSortLoc(id:number, sort: string, loc: string){
    return this.http.get(this.url + '/front/promo/merchant/' + id + '/sort/' + sort + '/location/' + loc).map(res => res );
  }

  /* ENDS HERE */
  


}
