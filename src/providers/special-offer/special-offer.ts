import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { ENV } from '../../environment/environment';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the SpecialOfferProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SpecialOfferProvider {
  private url = ENV.apiUrl;

  constructor(public http: HttpClient) {
    console.log('Hello SpecialOfferProvider Provider');
  }

  getTradeCategoryItems(){
    return this.http.get(this.url + '/v8/trade_category').map(res => res );
  }

}
