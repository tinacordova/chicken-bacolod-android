import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { ENV } from '../../environment/environment';

/*
  Generated class for the BranchProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class BranchProvider {

  private url = ENV.apiUrl;

  constructor(public http: HttpClient) {
    console.log('Hello BranchProvider Provider');
  }

  getBranchList(id: number){
    return this.http.get(this.url + '/v8/merchant/branch/' + id).map(res => res );
  }

}
