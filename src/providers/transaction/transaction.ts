import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { ENV } from '../../environment/environment';
import 'rxjs/add/operator/map';

@Injectable()
export class TransactionProvider {

  private url = ENV.apiUrl;

  constructor(public http: HttpClient) {
    console.log('Hello TransactionProvider Provider');
  }

  // LIVE
  postTransactionGet(vid: number, mid: number, uid: number,
      amt: number, type: string, bal: number,
      r_num: string, r_img: string, vuid: number,
      muid: string){
  
        return this.http.post(this.url + '/transaction/transaction_post_get', {
        "voucher_id": vid,
        "merchant_id" : mid,
        "user_id" : uid,
        "amount" : amt,
        "type" : ("'" + type + "'"),
        "balance": bal,
        "receipt_number" : ("'" + r_num + "'"),
        "receipt_img" : ("'" + r_img + "'"),
        "voucher_user_id": vuid,
        "merchant_user_id" : ("'" + muid + "'")
        } ).map(res => res);
        }

  voucherDetail(vid: number){

      return this.http.post(this.url + '/transaction/voucher_detail', {
      "voucher_id": vid,
      } ).map(res => res);
    }

  addTransactionAppointment(data:any){
    return this.http.post(this.url + '/transaction/transaction_appointment', data).map(res => res );
  }

  // LOCAL
  // postTransactionGet(vid: number, mid: number, uid: number,
  //   amt: number, type: string, bal: number,
  //   r_num: string, r_img: string, vuid: number,
  //   muid: string){

  //     return this.http.post('http://127.0.0.1:8000/api' + '/transaction/transaction_post_get', {
  //     "voucher_id": vid,
  //     "merchant_id" : mid,
  //     "user_id" : uid,
  //     "amount" : amt,
  //     "type" : ("'" + type + "'"),
  //     "balance": bal,
  //     "receipt_number" : ("'" + r_num + "'"),
  //     "receipt_img" : ("'" + r_img + "'"),
  //     "voucher_user_id": vuid,
  //     "merchant_user_id" : ("'" + muid + "'")
  //     } ).map(res => res);
  //     }

  // voucherDetail(vid: number){

  //     return this.http.post('http://127.0.0.1:8000/api' + '/transaction/voucher_detail', {
  //     "voucher_id": vid,
  //     } ).map(res => res);
  //   }

}
