import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { ENV } from '../../environment/environment';

@Injectable()

export class BannerProvider {

  private url = ENV.apiUrl;

  constructor(public http: HttpClient) {
    console.log('Hello BannerProvider Provider');
  }

  getBanner(){
    return this.http.get(this.url + '/v1/banner').map(res => res );
  }

  getBannerDetail(id:number){
    return this.http.get(this.url + '/v1/banner/' + id).map(res => res );
  }

}
