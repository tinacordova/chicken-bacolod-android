import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ENV } from '../../environment/environment';

/*
  Generated class for the CategoryProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CategoryProvider {

  private url = ENV.apiUrl;

  constructor(public http: HttpClient) {
    console.log('Hello CategoryProvider Provider');
  }

  getCategoryList(){
    return this.http.get(this.url + '/v8/category_list').map(res => res );
  }

}

