import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { ENV } from '../../environment/environment';
import 'rxjs/add/operator/map';

@Injectable()
export class PromoRatingProvider {

  private url = ENV.apiUrl;

  constructor(public http: HttpClient) {
    console.log('Hello PromoRatingProvider Provider');
  }

  // LIVE

  sendPromoRating(promoid: number, userid: number, rating: number){
    // return this.http.post(this.url + '/search/promo_search').map(res => res );
    return this.http.post(this.url + '/promo/rating', {
      "promo_id": promoid,
      "ipayu_user_id": userid,
      // "promo_rating_date": nowdate,
      "rating": rating
    } ).map(res => res);
  }

  getPromoRating(promoid: number){
    // return this.http.post(this.url + '/search/promo_search').map(res => res );
    return this.http.post(this.url + '/promo/rating_result', {
      "promo_id": promoid
    } ).map(res => res);
  }

  // LOCAL
  // sendPromoRating(promoid: number, userid: number, rating: number){
  //   // return this.http.post(this.url + '/search/promo_search').map(res => res );
  //   return this.http.post('http://127.0.0.1:8000/api' + '/promo/rating', {
  //     "promo_id": promoid,
  //     "ipayu_user_id": userid,
  //     // "promo_rating_date": nowdate,
  //     "rating": rating
  //   } ).map(res => res);
  // }

  // getPromoRating(promoid: number){
  //   // return this.http.post(this.url + '/search/promo_search').map(res => res );
  //   return this.http.post('http://127.0.0.1:8000/api' + '/promo/rating_result', {
  //     "promo_id": promoid
  //   } ).map(res => res);
  // }

}
