import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { ENV } from '../../environment/environment';
import 'rxjs/add/operator/map';

@Injectable()
export class RestoCategoryProvider {

  private url = ENV.apiUrl;

  constructor(public http: HttpClient) {
    console.log('Hello RestoCategoryProvider Provider');
  }

  // LIVE
  getProducts(mid: number, cit: number){
    // return this.http.post(this.url + '/search/promo_search').map(res => res );
    return this.http.post(this.url + '/resto/get_products', {
      "merchant_id": mid,
      "cat_id": cit
    } ).map(res => res);
  }

  getCategory(cit: number){
    // return this.http.post(this.url + '/search/promo_search').map(res => res );
    return this.http.post(this.url + '/resto/get_category', {
      "cat_id": cit
    } ).map(res => res);
  }

  // LOCAL
  // getProducts(mid: number, cit: number){
  //   // return this.http.post(this.url + '/search/promo_search').map(res => res );
  //   return this.http.post('http://127.0.0.1:8000/api' + '/resto/get_products', {
  //     "merchant_id": mid,
  //     "cat_id": cit
  //   } ).map(res => res);
  // }

  // getCategory(cit: number){
  //   // return this.http.post(this.url + '/search/promo_search').map(res => res );
  //   return this.http.post('http://127.0.0.1:8000/api' + '/resto/get_category', {
  //     "cat_id": cit
  //   } ).map(res => res);
  // }

}
