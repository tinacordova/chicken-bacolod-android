import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { ENV } from '../../environment/environment';

@Injectable()
export class CartProvider {

  private url = ENV.apiUrl;

  constructor(public http: HttpClient) {
    console.log('Hello CartProvider Provider');
  }

  // LIVE
  postCart(ipi: number, sid: number, table: number, pid: number, 
            qty: number, price: number, mid: any){
    return this.http.post(this.url + '/cart/cart_post',{
      "ipayu_id" : ipi,
      "session_id" : sid,
      "table_number": table,
      "product_id" : pid,
      "qty" : qty,
      "unit_price" : price,
      "merchant_id" : mid
    }).map(res => res );
  }

  postCartDetail(ipi: number){
    return this.http.post(this.url + '/cart/cart_detail',{
      "cart_id" : ipi
    }).map(res => res );
  }

  // postCartDetail(ipi: number, mid: number){
  //   return this.http.post(this.url + '/cart/cart_detail',{
  //     "cart_id" : ipi,
  //     "merchant_id": mid
  //   }).map(res => res );
  // }

  getCart(ipi: number, mid: number){
    return this.http.post(this.url + '/cart/cart_get',{
      "cart_id" : ipi,
      "merchant_id": mid
    }).map(res => res );
  }

  getCartDetailCounter(ipi: number, mid: number){

    console.log('pid', ipi);
    console.log('mid', mid);
    
    return this.http.post(this.url + '/cart/cart_detail_counter',{
      "cart_id" : ipi,
      "merchant_id" : mid,
    }).map(res => res );
  }

  getTableNumber(ipi: number, mid: number){
    
    return this.http.post(this.url + '/cart/get_table_number',{
      "ipayu_id" : ipi,
      "merchant_id" : mid,
    }).map(res => res );
  }

  getCartPayment(ipi: number, cit: number, disc: number, cash: number, card, change: number){
    return this.http.post(this.url + '/cart/cart_payment',{
      "ipayu_id": ipi,
      "cart_id" : cit,
      "total_discount" : disc,
      "payment_by_cash" : cash,
      "payment_by_card" : card,
      "cash_change" : change
    }).map(res => res );
  }

  updateCart(pid: number, cid: number, qty: number){
    return this.http.post(this.url + '/cart/cart_update',{
      "product_id": pid,
      "cart_id": cid,
      "qty": qty
    }).map(res => res );
  }

  cartCounter(mid: number){
    return this.http.get(this.url + '/cart/cart_counter/' + mid).map(res => res );
  }

  saveTransaction(userID: number,
        amt : number,
        mercID : number,
        r_img : any,
        mercUserID : number,
        cart_id : number,
        payment_cash : number,
        payment_by_card : number,
        cash_change : number){
    return this.http.post(this.url + '/cart/cart_transaction',{
      'user_id' : userID,
      'amount' : amt,
      'merchant_id' : mercID,
      'receipt_img' : r_img,
      'merchant_user_id' : mercUserID,
      'cart_id' : cart_id, 
      'payment_by_cash' : payment_cash,
      'payment_by_card' : payment_by_card,
      'cash_change' : cash_change
    }).map(res => res );
  }

  cancelOrder(pid: number, cdid : number, cit: number){
    return this.http.post(this.url + '/cart/cart_remove',{
      'product_id' : pid,
      'cart_detail_id' : cdid,
      'cart_id': cit
    }).map(res => res );
  }

  updateQty(qty: number, pid : number, cdid: number){
    return this.http.post(this.url + '/cart/cart_update_qty',{
      'qty' : qty,
      'product_id' : pid,
      'cart_detail_id': cdid
    }).map(res => res );
  }

  orderWalletList(ipi: number){
    return this.http.post(this.url + '/cart/cart_wallet_list',{
      'ipayu_id' : ipi
    }).map(res => res );
  }

  orderMerchantWalletList(data: any){
    return this.http.post(this.url + '/cart/merchant_cart_wallet_list', data).map(res => res);
  }

  cartMain(ipi: number, mid: number, table: number){
    return this.http.post(this.url + '/cart/cart_main',{
      'ipayu_id' : ipi,
      'table_number': table,
      'merchant_id': mid
    }).map(res => res );
  }

  // // LOCAL
  // postCart(ipi: number, sid: number, table: number, pid: number, 
  //           qty: number, price: number, mid: any){
  //   return this.http.post('http://127.0.0.1:8000/api' + '/cart/cart_post',{
  //     "ipayu_id" : ipi,
  //     "session_id" : sid,
  //     "table_number": table,
  //     "product_id" : pid,
  //     "qty" : qty,
  //     "unit_price" : price,
  //     "merchant_id" : mid
  //   }).map(res => res );
  // }

  // postCartDetail(ipi: number, mid: number){
  //   return this.http.post('http://127.0.0.1:8000/api' + '/cart/cart_detail',{
  //     "cart_id" : ipi,
  //     "merchant_id": mid
  //   }).map(res => res );
  // }

  // getCart(ipi: number, mid: number){
  //   return this.http.post('http://127.0.0.1:8000/api' + '/cart/cart_get',{
  //     "cart_id" : ipi,
  //     "merchant_id": mid
  //   }).map(res => res );
  // }

  // getCartDetailCounter(ipi: number){
  //   return this.http.post('http://127.0.0.1:8000/api' + '/cart/cart_detail_counter',{
  //     "cart_id" : ipi
  //   }).map(res => res );
  // }

  // getCartPayment(ipi: number, cit: number, disc: number, cash: number, card, change: number){
  //   return this.http.post('http://127.0.0.1:8000/api' + '/cart/cart_payment',{
  //     "ipayu_id": ipi,
  //     "cart_id" : cit,
  //     "total_discount" : disc,
  //     "payment_by_cash" : cash,
  //     "payment_by_card" : card,
  //     "cash_change" : change
  //   }).map(res => res );
  // }

  // updateCart(pid: number, cid: number, qty: number){
  //   return this.http.post('http://127.0.0.1:8000/api' + '/cart/cart_update',{
  //     "product_id": pid,
  //     "cart_id": cid,
  //     "qty": qty
  //   }).map(res => res );
  // }

  // cartCounter(){
  //   return this.http.get('http://127.0.0.1:8000/api' + '/cart/cart_counter').map(res => res );
  // }

  // saveTransaction(userID: number,
  //       amt : number,
  //       mercID : number,
  //       r_img : any,
  //       mercUserID : number,
  //       cart_id : number,
  //       payment_cash : number,
  //       payment_by_card : number,
  //       cash_change : number){
  //   return this.http.post('http://127.0.0.1:8000/api' + '/cart/cart_transaction',{
  //     'user_id' : userID,
  //     'amount' : amt,
  //     'merchant_id' : mercID,
  //     'receipt_img' : r_img,
  //     'merchant_user_id' : mercUserID,
  //     'cart_id' : cart_id, 
  //     'payment_by_cash' : payment_cash,
  //     'payment_by_card' : payment_by_card,
  //     'cash_change' : cash_change
  //   }).map(res => res );
  // }

  // cancelOrder(pid: number, cdid : number, cit: number){
  //   return this.http.post('http://127.0.0.1:8000/api' + '/cart/cart_remove',{
  //     'product_id' : pid,
  //     'cart_detail_id' : cdid,
  //     'cart_id': cit
  //   }).map(res => res );
  // }

  // updateQty(qty: number, pid : number, cdid: number){
  //   return this.http.post('http://127.0.0.1:8000/api' + '/cart/cart_update_qty',{
  //     'qty' : qty,
  //     'product_id' : pid,
  //     'cart_detail_id': cdid
  //   }).map(res => res );
  // }

  // orderWalletList(ipi: number){
  //   return this.http.post('http://127.0.0.1:8000/api' + '/cart/cart_wallet_list',{
  //     'ipayu_id' : ipi
  //   }).map(res => res );
  // }

  // cartMain(ipi: number, mid: number){
  //   return this.http.post('http://127.0.0.1:8000/api' + '/cart/cart_main',{
  //     'ipayu_id' : ipi,
  //     'merchant_id': mid
  //   }).map(res => res );
  // }

}
