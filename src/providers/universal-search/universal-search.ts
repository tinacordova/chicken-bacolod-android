import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';

import { ENV } from '../../environment/environment';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the UniversalSearchProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UniversalSearchProvider {
  private url = ENV.apiUrl;

  constructor(public http: HttpClient) {
    console.log('Hello UniversalSearchProvider Provider');
  }


  universalSearch(data:any){
    return this.http.post(this.url + '/search/universal_search', data).map(res => res);      
  }

  universalSearchLocation(data:any){
    return this.http.post(this.url + '/search/search_location', data).map(res => res);      
  }

}
