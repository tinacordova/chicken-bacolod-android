import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { ENV } from '../../environment/environment';
import 'rxjs/add/operator/map';


/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserProvider {

  private url = ENV.apiUrl;

  constructor(public http: HttpClient) {
    console.log('Hello UserProvider Provider');
  }

  validateUser(data:any){
    return this.http.post(this.url + '/user/validate', data).map(res => res);      
  }

}
