import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { ENV } from '../../environment/environment';
import 'rxjs/add/operator/map';

@Injectable()
export class WalletProvider {

  private url = ENV.apiUrl;

  constructor(public http: HttpClient) {
    console.log('Hello WalletProvider Provider');
  }

  // LIVE
  postWalletVoucher(vid: number, uid: any, fname: string, lname: string){
    return this.http.post(this.url + '/wallet/wallet_voucher_post', {
    "voucher_id": vid,  
    "user_id": uid,
    "user_fname": "'" + fname + "'",
    "user_lname": "'" + lname + "'"
    } ).map(res => res);
  }

  getWalletVoucher(uid: number){
    return this.http.post(this.url + '/wallet/wallet_voucher', {
      "user_id":  uid
      } ).map(res => res);
  }

  getMerchantWalletVoucher(data: any){
    return this.http.post(this.url + '/wallet/merchant_wallet_voucher', data).map(res => res); 
  }

  getWalletUse(iuid: number, vid: number){
    return this.http.post(this.url + '/wallet/wallet_voucher_use', {
      "ipayu_user_id": iuid,
      "voucher_id": vid
      } ).map(res => res);
  }

  sendNotif(mid: number){
    return this.http.post(this.url + '/merchant/device_get', {
      "merchant_id": mid
      } ).map(res => res);
  }

  getVoucherStatus(user_id:number, voucher_id:number){
    return this.http.post(this.url + '/wallet/wallet_voucher_status', {
      "user_id": user_id,
      "voucher_id": voucher_id 
      } ).map(res => res);
  }

  checkVoucher(vid){
    return this.http.post(this.url + '/wallet/wallet_voucher_checker', {
      "voucher_id": vid 
      } ).map(res => res);
  }

  // LOCAL
  // postWalletVoucher(vid: number, uid: any, fname: string, lname: string){
  //   return this.http.post('http://127.0.0.1:8000/api' + '/wallet/wallet_voucher_post', {
  //   "voucher_id": vid,  
  //   "user_id": uid,
  //   "user_fname": "'" + fname + "'",
  //   "user_lname": "'" + lname + "'"
  //   } ).map(res => res);
  // }

  // getWalletVoucher(uid: number){
  //   return this.http.post('http://127.0.0.1:8000/api' + '/wallet/wallet_voucher', {
  //     "user_id":  uid
  //     } ).map(res => res);
  // }

  // getWalletUse(iuid: number, vid: number){
  //   return this.http.post('http://127.0.0.1:8000/api' + '/wallet/wallet_voucher_use', {
  //     "ipayu_user_id": iuid,
  //     "voucher_id": vid
  //     } ).map(res => res);
  // }

  // sendNotif(mid: number){
  //   return this.http.post('http://127.0.0.1:8000/api' + '/merchant/device_get', {
  //     "merchant_id": mid
  //     } ).map(res => res);
  // }

  // getVoucherStatus(user_id:number, voucher_id:number){
  //   return this.http.post('http://127.0.0.1:8000/api' + '/wallet/wallet_voucher_status', {
  //     "user_id": user_id,
  //     "voucher_id": voucher_id 
  //     } ).map(res => res);
  //   }
}
