import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';

import { ENV } from '../../environment/environment';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';


/*
  Generated class for the VoucherProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class VoucherProvider {

  private url = ENV.apiUrl;

  constructor(public http: HttpClient) {
    console.log('Hello VoucherProvider Provider');
  }

  /* NEW INTEGRATION FOR FRONTEND */

  getVoucher(){
    return this.http.get(this.url + '/front/voucher').map(res => res );
  }

  getVoucherHome(){
    return this.http.get(this.url + '/v8/voucher').map(res => res );
  }

  sortVoucher(sort: string){
    return this.http.get(this.url + '/front/voucher/sort/' + sort).map(res => res );
  }

  sortVoucherMerchant(id: number){
    return this.http.get(this.url + '/front/voucher/merchant/' + id).map(res => res );
  }

  getVoucherLocation(id: number){
    return this.http.get(this.url + '/front/voucher/location/' + id).map(res => res );
  }

  getVoucherDetail(id:number){
    return this.http.get(this.url + '/v8/voucher/' + id).map(res => res );
  }

  getMerchantVoucher(id:number){
    return this.http.get(this.url + '/v8/voucher/merchant/' + id).map(res => res );
  }

  sortVoucherLocation(id: number, sort: string){
    return this.http.get(this.url + '/front/voucher/location/' + id + '/sort/' + sort).map(res => res );
  }

  voucherMerchant(id:number){
    return this.http.get(this.url + '/front/voucher/merchant/' + id).map(res => res );
  }

  vouchMercSort(id: number, sort: string){
    return this.http.get(this.url + '/front/voucher/merchant/' + id + '/sort/' + sort).map(res => res );
  }

  vouchMercSortLoc(id: number, sort: string, loc: string){
    return this.http.get(this.url + '/front/voucher/merchant/' + id + '/sort/' + sort + '/location/' + loc).map(res => res );
  }

  getVoucherCategoryItems(){
    return this.http.get(this.url + '/v8/categorized_voucher_list').map(res => res );
  }

  getVoucherMerchantCategoryItems(data: any){
    return this.http.post(this.url + '/v8/merchant_categorized_voucher_list', data).map(res => res);
  }

  getMerchantVoucherSearch(data: any){
    return this.http.post(this.url + '/v8/merchant_voucher_search', data).map(res => res);
  }

  getTradeCategoryItems(){
    return this.http.get(this.url + '/v8/trade_category').map(res => res );
  }


  getPromoFilter(data:any){
    return this.http.post(this.url + '/search/promo_search', data).map(res => res);

  }

  getVoucherFilter(query: string){

    let data = {
      "query": query,
      "location": ""
    };

    const httpOptions = {
      headers: new HttpHeaders({
            'Accept':  'application/json',
            'Content-Type': 'application/json'
          })
      };
 
    let body = JSON.stringify(data);
    console.log('body',body);
  
    return this.http.post(this.url + '/search/voucher_search',body,httpOptions);        
    }

    getVoucherFilterLocation(query: string, arr = []){

        let data = {
          "query": "",
          "location": query,
          "trade_category_id": [arr]
        };
  
      const httpOptions = {
        headers: new HttpHeaders({
              'Accept':  'application/json',
              'Content-Type': 'application/json'
            })
        };
   
      let body = JSON.stringify(data);
      console.log('body',body);
    
      return this.http.post(this.url + '/search/voucher_search_category',body,httpOptions);        
      }

  /* ENDS HERE */
  


}
