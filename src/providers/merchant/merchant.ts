import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ENV } from '../../environment/environment';

@Injectable()
export class MerchantProvider {

  private url = ENV.apiUrl;

  constructor(public http: HttpClient) {
    console.log('Hello MerchantProvider Provider');
  }

  /* NEW INTEGRATION FOR FRONT END */

  newMerchant(){
    return this.http.get(this.url + '/front/merchant').map(res => res );
  }

  merchantDetail(id: number){
    return this.http.get(this.url + '/front/product/merchant/' + id).map(res => res );
  }

  getFirstProduct(id: number){
    return this.http.get(this.url + '/front/product/merchant/' + id + '/batch/1').map(res => res );
  }

  getSecondProduct(id: number){
    return this.http.get(this.url + '/front/product/merchant/' + id + '/batch/2').map(res => res );
  }

  sortProductMerchant(id: number, sort: string){
    return this.http.get(this.url + '/front/product/merchant/' + id + '/sort/' + sort).map(res => res );
  }

  search(address: string, name: string){
    return this.http.get(this.url + '/front/merchant/add/' + address + '/prod/' + name).map(res => res );
  }

  /* ENDS HERE */

  merchant10(){
    return this.http.get(this.url + '/v1/merchant/batch/1').map(res => res );
  }

  getAppList(data:any){
    return this.http.post(this.url + '/user/app_list', data).map(res => res);      
  }

  merchant20(){
    return this.http.get(this.url + '/v1/merchant/batch/2').map(res => res );
  }

  getMerchant(){
    return this.http.get(this.url + '/v1/merchant').map(res => res );
  }
  

  sortMerchantProducts(id: number, sort: string){
    return this.http.get(this.url + '/v1/merchant/product/' + id + '/sort/' + sort).map(res => res );
  }

  merchantInfo(id: number){
    return this.http.get(this.url + '/v8/shop_info/' + id).map(res => res );
  }

  getMerchantAds(data:any){
    //return this.http.get(this.url + '/v8/merchant_ads/' + id).map(res => res );
    return this.http.post(this.url + '/v8/merchant_ads', data).map(res => res);
  }

  // MERCHANT VOUCHER

  // LIVE
  postMerchantVoucher(vid: number, mid: number, uid: any){
    // return this.http.post(this.url + '/search/promo_search').map(res => res );
    return this.http.post(this.url + '/merchant/voucher_post', {
    "voucher_id": vid,
    "merchant_id": mid,  
    "user_id": uid
    } ).map(res => res);
  }

  // LOCAL
  // postMerchantVoucher(vid: number, mid: number, uid: any){
  //   // return this.http.post(this.url + '/search/promo_search').map(res => res );
  //   return this.http.post('http://127.0.0.1:8000/api' + '/merchant/voucher_post', {
  //   "voucher_id": vid,
  //   "merchant_id": mid,  
  //   "user_id": uid
  //   } ).map(res => res);
  // }

  searchApp(data:any){
    return this.http.post(this.url + '/user/app_search', data).map(res => res);      
  }

  addApp(data:any){
    return this.http.post(this.url + '/user/add_app', data).map(res => res);      
  }

}
