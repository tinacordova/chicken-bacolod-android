import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { ENV } from '../../environment/environment';

@Injectable()
export class ProductDetailProvider {

  private url = ENV.apiUrl;

  constructor(public http: HttpClient) {
    console.log('Hello ProductDetailProvider Provider');
  }

  getProductDetail(pid: number){
    return this.http.post('http://127.0.0.1:8000/api' + '/product/product_detail', {
      "product_id": pid
      } ).map(res => res);
  }

}
