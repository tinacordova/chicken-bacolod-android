import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { ENV } from '../../environment/environment';
import 'rxjs/add/operator/map';

@Injectable()
export class PromoSearchProvider {

  private url = ENV.apiUrl;

  constructor(public http: HttpClient) {
    console.log('Hello PromoSearchProvider Provider');
  }
  
  // getPromoSearch(item1: string){
  //   // return this.http.post(this.url + '/search/promo_search').map(res => res );
  //   return this.http.post(this.url + '/search/promo_search', {
  //     "abc": item1,
  //     "description": item1,
  //     "location": ""
  //   } ).map(res => res);
  // }

  // LIVE
  getPromoSearchCat(item1: string){
    // return this.http.post(this.url + '/search/promo_search').map(res => res );
    return this.http.post(this.url + '/search/promo_search', {
      "query": item1,
      "location": ""
      } ).map(res => res);
  }

  getPromoFilterCat(item: string, arr: any = []){
    // return this.http.post(this.url + '/search/promo_search').map(res => res );
    return this.http.post(this.url + '/search/promo_search_category', {
      "query": "",
      "description": "",
      "location": item,
      "trade_category_id": [arr]
    } ).map(res => res);
  }

  // LOCAL
  // getPromoSearchCat(item1: string){
  //   // return this.http.post(this.url + '/search/promo_search').map(res => res );
  //   return this.http.post('http://127.0.0.1:8000/api' + '/search/promo_search', {
  //     "query": item1,
  //     "description": item1,
  //     "location": ""
  //   } ).map(res => res);
  // }

  // getPromoFilterCat(item: string, arr: any = []){
  //   // return this.http.post(this.url + '/search/promo_search').map(res => res );
  //   return this.http.post('http://127.0.0.1:8000/api' + '/search/promo_search_category', {
  //     "abc": "",
  //     "description": "",
  //     "location": item,
  //     "trade_category_id": [arr]
  //   } ).map(res => res);
  // }

  

  getPromoCategoriesFilter(){
    return this.http.get(this.url + '/v8/trade_category').map(res => res );
  }


}
