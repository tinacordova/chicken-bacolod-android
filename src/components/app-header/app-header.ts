import { Component } from '@angular/core';
import { NavController, App } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { CartProvider } from '../../providers/cart/cart';
import { MerchantProvider } from '../../providers/merchant/merchant';

/**
 * Generated class for the AppHeaderComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'app-header',
  templateUrl: 'app-header.html'
})
export class AppHeaderComponent {

  text: string;
  userID: number;
  merchantID: number;
  tableNumber: number;
  counterFront: number;
  merchants: any = [];
  cartcounter: any [];
  cartQty: number;

  main: any;
  cart_id: number;

  constructor(
    private storage: Storage,
    public navCtrl: NavController, 
    public cartProvider: CartProvider,
    public appCtrl: App,
    public merchantProvider: MerchantProvider
    ) {
    console.log('Hello AppHeaderComponent Component');
    this.text = 'Hello World';

    this.storage.get('itemQuantity').then((val) => {
      if(val != 0){
        this.cartQty = val;
      }else{
        this.cartQty = 0;
      }
      
    });

    this.getCartID();
    this.loadShopInfo();

  }

  ionViewDidLoad() {
    this.getCartID();
    this.loadShopInfo();
  }

  getCartID(){

    this.storage.get('userID').then((value)=>{
      this.userID = value;

      console.log('user_id', this.userID);
  
      this.storage.get('merchantID').then((value)=>{
        this.merchantID = value;

        console.log('merchant_id', this.merchantID);

        this.storage.get('tableNumber').then((value)=>{
          this.tableNumber = value;

          this.cartProvider.cartMain(this.userID, this.merchantID, 0).subscribe(
            res => {
              let resultset: any = res;
              this.main = resultset.data;
              console.log("main", this.main);
              console.log("cartid", this.main[0].cart_id);
              console.log("mainid", this.main[0].ipayu_id);

              this.loadCartDetailCounter(this.main[0].cart_id);
      
              this.cart_id = this.main[0].cart_id      
              this.storage.set('cartID', this.cart_id);
      
            }, error => {
              console.log(error);
          });
        
        });
      
      });
    
    });
  }

  loadCartDetailCounter(cartID){

    this.storage.get('merchantID').then((value)=>{
      this.merchantID = value;

        this.cartProvider.getCartDetailCounter(cartID, this.merchantID).subscribe(
          res => {
            let resultset: any = res;
            this.cartcounter = resultset.data;
            
            console.log("counter", this.cartcounter[0].detail_counter);
            this.counterFront = this.cartcounter[0].detail_counter
    
            if (this.cartcounter[0].detail_counter == 0)
            this.counterFront + 1

            this.storage.set('itemQuantity', this.counterFront);

            this.storage.get('itemQuantity').then((val) => {
              this.cartQty = val;
            });

            
          }, error => {
            console.log(error);
        });

    });
}

loadShopInfo(){

  this.storage.get('merchantID').then((val)=>{
    this.merchantID = val;

    this.merchantProvider.merchantInfo(this.merchantID).subscribe(
      res => {
        let resultset: any = res;
        this.merchants = resultset.data;

        console.log("merchants", this.merchants);
      }, error => {
        console.log(error);
    });
  
  });
}

goToReservation(){
  this.navCtrl.push('ReservationPage');
}

openCart(){
  this.appCtrl.getRootNav().push('MenuOrderingPage');
}

}
