import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { FabComponent } from './fab/fab';
import { AppHeaderComponent } from './app-header/app-header';
import { AppHeaderHomeComponent } from './app-header-home/app-header-home';
@NgModule({
	declarations: [FabComponent,
    AppHeaderComponent,
    AppHeaderHomeComponent],
	imports: [IonicModule],
	exports: [FabComponent,
    AppHeaderComponent,
    AppHeaderHomeComponent]
})
export class ComponentsModule {}
