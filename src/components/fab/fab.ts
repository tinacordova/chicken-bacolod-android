import { Component } from '@angular/core';
import { NavController, App } from 'ionic-angular';



/**
 * Generated class for the FabComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'fab',
  templateUrl: 'fab.html'
})
export class FabComponent {

  text: string;

  constructor(
    public appCtrl: App,     
    public navCtrl: NavController) {
    
      console.log('Hello FabComponent Component');
    this.text = 'Hello World';
  }

  toWallet() {
    //this.appCtrl.getRootNav().push("WalletFabPage");
    this.navCtrl.push("WalletFabPage");
  }
  
  toProduct() {
    //this.appCtrl.getRootNav().push("SpecialOfferPage");
    this.navCtrl.push("SpecialOfferPage");
  }

  toPromo() {
    //this.appCtrl.getRootNav().push("PromoPage");
    this.navCtrl.push("PromoPage");
  }

  toResto() {
    //this.appCtrl.getRootNav().push("VoucherFabPage");
    this.navCtrl.push("VoucherFabPage");
  }

}
